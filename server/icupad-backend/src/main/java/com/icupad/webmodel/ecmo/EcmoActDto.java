package com.icupad.webmodel.ecmo;

import com.icupad.domain.User;
import com.icupad.entity.ecmo.EcmoAct;
import com.icupad.entity.ecmo.EcmoProcedure;
import com.icupad.function.utils.Utils;

import java.sql.Timestamp;

/**
 * Created by Marcin on 04.03.2017.
 */
public class EcmoActDto {
    private Long id;
    private Long procedureId;
    private Long createdBy;
    private Long date;
    private Double act;
//    private Boolean actual;

    public EcmoActDto(){}

    public EcmoActDto(EcmoAct ecmoAct){
        this.id = ecmoAct.getId();
        this.procedureId = ecmoAct.getProcedureId().getId();
        this.createdBy = ecmoAct.getCreatedBy().getId();
        this.date = Utils.getTimeOrNull(ecmoAct.getDate());
        this.act = ecmoAct.getAct();
//        this.actual = ecmoAct.getActual();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProcedureId() {
        return procedureId;
    }

    public void setProcedureId(Long procedureId) {
        this.procedureId = procedureId;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Double getAct() {
        return act;
    }

    public void setAct(Double act) {
        this.act = act;
    }

//    public Boolean getActual() {
//        return actual;
//    }
//
//    public void setActual(Boolean actual) {
//        this.actual = actual;
//    }
}

package com.icupad.webmodel.stethoscope;

public class AuscultateSuiteDto {
    private Long id;
    private Long patientId;
    private String description;
    private String executorHl7Id;
    private String executorName;
    private String executorSurname;
    private Long auscultateSuiteSchemaId;
    private Long position;
    private Integer temperature;
    private Boolean isRespirated;
    private Boolean passiveOxygenTherapy;
    private Long examinationDateTime;

    private Long createdById;
    private Long createdDateTime;
    private Long lastModifiedById;
    private Long lastModifiedDateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExecutorHl7Id() {
        return executorHl7Id;
    }

    public void setExecutorHl7Id(String executorHl7Id) {
        this.executorHl7Id = executorHl7Id;
    }

    public String getExecutorName() {
        return executorName;
    }

    public void setExecutorName(String executorName) {
        this.executorName = executorName;
    }

    public String getExecutorSurname() {
        return executorSurname;
    }

    public void setExecutorSurname(String executorSurname) {
        this.executorSurname = executorSurname;
    }

    public Long getAuscultateSuiteSchemaId() {
        return auscultateSuiteSchemaId;
    }

    public void setAuscultateSuiteSchemaId(Long auscultateSuiteSchemaId) {
        this.auscultateSuiteSchemaId = auscultateSuiteSchemaId;
    }

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public Boolean getIsRespirated() {
        return isRespirated;
    }

    public void setIsRespirated(Boolean respirated) {
        isRespirated = respirated;
    }

    public Boolean getPassiveOxygenTherapy() {
        return passiveOxygenTherapy;
    }

    public void setPassiveOxygenTherapy(Boolean passiveOxygenTherapy) {
        this.passiveOxygenTherapy = passiveOxygenTherapy;
    }

    public Long getExaminationDateTime() {
        return examinationDateTime;
    }

    public void setExaminationDateTime(Long examinationDateTime) {
        this.examinationDateTime = examinationDateTime;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public Long getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Long createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Long getLastModifiedById() {
        return lastModifiedById;
    }

    public void setLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
    }

    public Long getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(Long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }
}

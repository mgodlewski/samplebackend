package com.icupad.webmodel.ecmo;

import com.icupad.domain.User;
import com.icupad.entity.ecmo.EcmoParameter;
import com.icupad.entity.ecmo.EcmoProcedure;
import com.icupad.function.utils.Utils;

import java.sql.Timestamp;

/**
 * Created by Marcin on 04.03.2017.
 */
public class EcmoParameterDto {
    private Long id;
    private Long procedureId;
    private Long createdBy;
    private Long date;
    private Double rpm;
    private Double bloodFlow;
    private Double fiO2;
    private Double gasFlow;
    private Double p1;
    private Double p2;
    private Double p3;
    private Double delta;
    private Double heparinSuply;
    private String description;
//    private Boolean actual;

    public EcmoParameterDto(){}

    public EcmoParameterDto(EcmoParameter ecmoParameter) {
        this.id = ecmoParameter.getId();
        this.procedureId = ecmoParameter.getProcedureId().getId();
        this.createdBy = ecmoParameter.getCreatedBy().getId();
        this.date = Utils.getTimeOrNull(ecmoParameter.getDate());
        this.rpm = ecmoParameter.getRpm();
        this.bloodFlow = ecmoParameter.getBloodFlow();
        this.fiO2 = ecmoParameter.getFiO2();
        this.gasFlow = ecmoParameter.getGasFlow();
        this.p1 = ecmoParameter.getP1();
        this.p2 = ecmoParameter.getP2();
        this.p3 = ecmoParameter.getP3();
        this.delta = ecmoParameter.getDelta();
        this.heparinSuply = ecmoParameter.getHeparinSuply();
        this.description = ecmoParameter.getDescription();
//        this.actual = ecmoParameter.getActual();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProcedureId() {
        return procedureId;
    }

    public void setProcedureId(Long procedureId) {
        this.procedureId = procedureId;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Double getRpm() {
        return rpm;
    }

    public void setRpm(Double rpm) {
        this.rpm = rpm;
    }

    public Double getBloodFlow() {
        return bloodFlow;
    }

    public void setBloodFlow(Double bloodFlow) {
        this.bloodFlow = bloodFlow;
    }

    public Double getFiO2() {
        return fiO2;
    }

    public void setFiO2(Double fiO2) {
        this.fiO2 = fiO2;
    }

    public Double getGasFlow() {
        return gasFlow;
    }

    public void setGasFlow(Double gasFlow) {
        this.gasFlow = gasFlow;
    }

    public Double getP1() {
        return p1;
    }

    public void setP1(Double p1) {
        this.p1 = p1;
    }

    public Double getP2() {
        return p2;
    }

    public void setP2(Double p2) {
        this.p2 = p2;
    }

    public Double getP3() {
        return p3;
    }

    public void setP3(Double p3) {
        this.p3 = p3;
    }

    public Double getDelta() {
        return delta;
    }

    public void setDelta(Double delta) {
        this.delta = delta;
    }

    public Double getHeparinSuply() {
        return heparinSuply;
    }

    public void setHeparinSuply(Double heparinSuply) {
        this.heparinSuply = heparinSuply;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

//    public Boolean getActual() {
//        return actual;
//    }
//
//    public void setActual(Boolean actual) {
//        this.actual = actual;
//    }
}

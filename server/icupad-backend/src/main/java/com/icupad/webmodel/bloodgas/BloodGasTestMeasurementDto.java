package com.icupad.webmodel.bloodgas;

import com.icupad.entity.enums.Abnormality;
import com.icupad.entity.enums.BloodSource;
import com.icupad.function.utils.Utils;

import java.util.Date;

/**
 * Created by Marcin on 09.06.2017.
 */
public class BloodGasTestMeasurementDto {
    private String name;
    private String unit;
    private String bloodSource;
    private double value;
    private String abnormality;
    private Date resultDate;
    private Double bottomDefaultNorm;
    private Double topDefaultNorm;
    private long stayId;
    private long testId;
    private Date birthDate;

    public BloodGasTestMeasurementDto(String name, String unit, BloodSource bloodSource, double value,
                                      Abnormality abnormality, Date resultDate, long stayId, long testId, Date birthDate) {
        this.name = name;
        this.unit = unit;
        this.bloodSource = Utils.toStringOrNull(bloodSource);
        this.value = value;
        this.abnormality = Utils.toStringOrNull(abnormality);
        this.resultDate = resultDate;
        this.stayId = stayId;
        this.testId = testId;
        this.birthDate = birthDate;
    }

    public long getTestId() {
        return testId;
    }

    public void setTestId(long testId) {
        this.testId = testId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getBloodSource() {
        return bloodSource;
    }

    public void setBloodSource(String bloodSource) {
        this.bloodSource = bloodSource;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getAbnormality() {
        return abnormality;
    }

    public void setAbnormality(String abnormality) {
        this.abnormality = abnormality;
    }

    public Date getResultDate() {
        return resultDate;
    }

    public void setResultDate(Date resultDate) {
        this.resultDate = resultDate;
    }

    public Double getBottomDefaultNorm() {
        return bottomDefaultNorm;
    }

    public void setBottomDefaultNorm(Double bottomDefaultNorm) {
        this.bottomDefaultNorm = bottomDefaultNorm;
    }

    public Double getTopDefaultNorm() {
        return topDefaultNorm;
    }

    public void setTopDefaultNorm(Double topDefaultNorm) {
        this.topDefaultNorm = topDefaultNorm;
    }

    public long getStayId() {
        return stayId;
    }

    public void setStayId(long stayId) {
        this.stayId = stayId;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
}

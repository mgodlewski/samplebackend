package com.icupad.webmodel.ecmo;

import com.icupad.entity.ecmo.VeinCannula;

/**
 * Created by Marcin on 04.03.2017.
 */
public class VeinCannulaDto {
    private String type;
    private Double size;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }

    public VeinCannulaDto(){}

    public VeinCannulaDto(VeinCannula veinCannula) {
        this.type = veinCannula.getType();
        this.size = veinCannula.getSize();
    }
}

package com.icupad.webmodel.ecmo;

import com.icupad.entity.ecmo.EcmoProcedure;
import com.icupad.function.utils.Utils;

import java.sql.Timestamp;

/**
 * Created by Marcin on 04.03.2017.
 */
public class EcmoProcedureDto {
        private Long id;
        private Long stayId;
        private Long createdById;
        private Long endById;
        private String startCause;
        private Long startDate;
        private String endCause;
        private Long endDate;

    public EcmoProcedureDto(){}

    public EcmoProcedureDto(EcmoProcedure aEcmoProcedure) {
        this.id = aEcmoProcedure.getId();
        this.stayId = aEcmoProcedure.getStay().getId();
        this.createdById = aEcmoProcedure.getCreatedBy().getId();
        if(aEcmoProcedure.getEndBy()!=null){
            this.endById = aEcmoProcedure.getEndBy().getId();
        }else{
            this.endById = null;
        }
        this.startCause = aEcmoProcedure.getStartCause();
        this.startDate = Utils.getTimeOrNull(aEcmoProcedure.getStartDate());
        this.endCause = aEcmoProcedure.getEndCause();
        this.endDate = Utils.getTimeOrNull(aEcmoProcedure.getEndDate());
    }

    public java.lang.Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStay() {
        return stayId;
    }

    public void setStay(java.lang.Long stay) {
        this.stayId = stay;
    }

    public Long getCreatedBy() {
        return createdById;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdById= createdBy;
    }

    public String getStartCause() {
        return startCause;
    }

    public void setStartCause(String startCause) {
        this.startCause = startCause;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public String getEndCause() {
        return endCause;
    }

    public void setEndCause(String endCause) {
        this.endCause = endCause;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public Long getEndBy() {
        return endById;
    }

    public void setEndBy(Long endBy) {
        this.endById = endBy;
    }
}

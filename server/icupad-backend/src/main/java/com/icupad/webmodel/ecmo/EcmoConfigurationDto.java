package com.icupad.webmodel.ecmo;

import com.icupad.domain.User;
import com.icupad.entity.ecmo.EcmoConfiguration;
import com.icupad.entity.ecmo.EcmoProcedure;
import com.icupad.entity.ecmo.UsedStructures;
import com.icupad.entity.ecmo.VeinCannula;
import com.icupad.function.utils.Utils;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Marcin on 04.03.2017.
 */
public class EcmoConfigurationDto {
    private Long id;
    private Long procedureId;
    private Long createdBy;
    private String mode;
    private String caniulationType;
    private String wentowanieLeftVentricle;
    private String arteryCannulaType;
    private Double arteryCannulaSize;
    private String oxygeneratorType;
    private Boolean actual;
    private Long date;
    private List<String> cannulationStructures;
    private List<VeinCannulaDto> veinCannulaList;

    public EcmoConfigurationDto(){}

    public EcmoConfigurationDto(EcmoConfiguration ecmoConfiguration, List<UsedStructures> usedStructuresList, List<VeinCannula> veinCannulaList) {
        this.id = ecmoConfiguration.getId();
        this.procedureId = ecmoConfiguration.getProcedureId().getId();
        this.createdBy = ecmoConfiguration.getCreatedBy().getId();
        this.mode = ecmoConfiguration.getMode();
        this.caniulationType = ecmoConfiguration.getCaniulationType();
        this.wentowanieLeftVentricle = ecmoConfiguration.getWentowanieLeftVentricle();
        this.arteryCannulaType = ecmoConfiguration.getArteryCannulaType();
        this.arteryCannulaSize = ecmoConfiguration.getArteryCannulaSize();
        this.oxygeneratorType = ecmoConfiguration.getOxygeneratorType();
        this.actual = ecmoConfiguration.getActual();
        this.date = Utils.getTimeOrNull(ecmoConfiguration.getDate());
        this.cannulationStructures = usedStructuresList.stream().map(s->s.getName()).collect(Collectors.toList());
        this.veinCannulaList = IntStream.range(0, veinCannulaList.size())
                .mapToObj(s -> new VeinCannulaDto(veinCannulaList.get(s)))
                .collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProcedureId() {
        return procedureId;
    }

    public void setProcedureId(Long procedureId) {
        this.procedureId = procedureId;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getCaniulationType() {
        return caniulationType;
    }

    public void setCaniulationType(String caniulationType) {
        this.caniulationType = caniulationType;
    }

    public String getWentowanieLeftVentricle() {
        return wentowanieLeftVentricle;
    }

    public void setWentowanieLeftVentricle(String wentowanieLeftVentricle) {
        this.wentowanieLeftVentricle = wentowanieLeftVentricle;
    }

    public String getArteryCannulaType() {
        return arteryCannulaType;
    }

    public void setArteryCannulaType(String arteryCannulaType) {
        this.arteryCannulaType = arteryCannulaType;
    }

    public Double getArteryCannulaSize() {
        return arteryCannulaSize;
    }

    public void setArteryCannulaSize(Double arteryCannulaSize) {
        this.arteryCannulaSize = arteryCannulaSize;
    }

    public String getOxygeneratorType() {
        return oxygeneratorType;
    }

    public void setOxygeneratorType(String oxygeneratorType) {
        this.oxygeneratorType = oxygeneratorType;
    }

    public Boolean getActual() {
        return actual;
    }

    public void setActual(Boolean actual) {
        this.actual = actual;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public List<String> getCannulationStructures() {
        return cannulationStructures;
    }

    public void setCannulationStructures(List<String> cannulationStructures) {
        this.cannulationStructures = cannulationStructures;
    }

    public List<VeinCannulaDto> getVeinCannulaList() {
        return veinCannulaList;
    }

    public void setVeinCannulaList(List<VeinCannulaDto> veinCannulaList) {
        this.veinCannulaList = veinCannulaList;
    }
}

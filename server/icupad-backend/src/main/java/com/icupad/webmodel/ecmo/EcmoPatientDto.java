package com.icupad.webmodel.ecmo;

import com.icupad.domain.User;
import com.icupad.entity.ecmo.EcmoPatient;
import com.icupad.entity.ecmo.EcmoProcedure;
import com.icupad.function.utils.Utils;

import java.sql.Timestamp;

/**
 * Created by Marcin on 04.03.2017.
 */
public class EcmoPatientDto {
    private Long id;
    private Long procedureId;
    private Long createdBy;
    private Long date;
    private Double sat;
    private Double abp;
    private Double vcp;
    private Double hr;
    private Double tempSurface;
    private Double tempCore;
    private Double diuresis;
    private Double ultrafiltraction;
//    private Boolean actual;

    public EcmoPatientDto(){}

    public EcmoPatientDto(EcmoPatient ecmoPatient){
        this.id = ecmoPatient.getId();
        this.procedureId = ecmoPatient.getProcedureId().getId();
        this.createdBy = ecmoPatient.getCreatedBy().getId();
        this.date = Utils.getTimeOrNull(ecmoPatient.getDate());
        this.sat = ecmoPatient.getSat();
        this.abp = ecmoPatient.getAbp();
        this.vcp = ecmoPatient.getVcp();
        this.hr = ecmoPatient.getHr();
        this.tempSurface = ecmoPatient.getTempSurface();
        this.tempCore = ecmoPatient.getTempCore();
        this.diuresis = ecmoPatient.getDiuresis();
        this.ultrafiltraction = ecmoPatient.getUltrafiltraction();
//        this.actual = ecmoPatient.getActual();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProcedureId() {
        return procedureId;
    }

    public void setProcedureId(Long procedureId) {
        this.procedureId = procedureId;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Double getSat() {
        return sat;
    }

    public void setSat(Double sat) {
        this.sat = sat;
    }

    public Double getAbp() {
        return abp;
    }

    public void setAbp(Double abp) {
        this.abp = abp;
    }

    public Double getVcp() {
        return vcp;
    }

    public void setVcp(Double vcp) {
        this.vcp = vcp;
    }

    public Double getHr() {
        return hr;
    }

    public void setHr(Double hr) {
        this.hr = hr;
    }

    public Double getTempSurface() {
        return tempSurface;
    }

    public void setTempSurface(Double tempSurface) {
        this.tempSurface = tempSurface;
    }

    public Double getTempCore() {
        return tempCore;
    }

    public void setTempCore(Double tempCore) {
        this.tempCore = tempCore;
    }

    public Double getDiuresis() {
        return diuresis;
    }

    public void setDiuresis(Double diuresis) {
        this.diuresis = diuresis;
    }

    public Double getUltrafiltraction() {
        return ultrafiltraction;
    }

    public void setUltrafiltraction(Double ultrafiltraction) {
        this.ultrafiltraction = ultrafiltraction;
    }

//    public Boolean getActual() {
//        return actual;
//    }
//
//    public void setActual(Boolean actual) {
//        this.actual = actual;
//    }
}

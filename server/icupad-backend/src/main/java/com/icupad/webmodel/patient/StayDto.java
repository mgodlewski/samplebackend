package com.icupad.webmodel.patient;

public class StayDto {
    private Long id;
    private Boolean active;
    private Long admitDate;
    private String admittingDoctorHl7Id;
    private String admittingDoctorName;
    private String admittingDoctorSurname;
    private String admittingDoctorNpwz;
    private String assignedPatientLocationName;
    private Long dischargeDate;
    private String hl7Id;
    private String stayType;
    private Long patientId;

    private Long createdDateTime;
    private Long lastModifiedDateTime;
    private Long createdById;
    private Long lastModifiedById;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Long getAdmitDate() {
        return admitDate;
    }

    public void setAdmitDate(Long admitDate) {
        this.admitDate = admitDate;
    }

    public String getAdmittingDoctorHl7Id() {
        return admittingDoctorHl7Id;
    }

    public void setAdmittingDoctorHl7Id(String admittingDoctorHl7Id) {
        this.admittingDoctorHl7Id = admittingDoctorHl7Id;
    }

    public String getAdmittingDoctorName() {
        return admittingDoctorName;
    }

    public void setAdmittingDoctorName(String admittingDoctorName) {
        this.admittingDoctorName = admittingDoctorName;
    }

    public String getAdmittingDoctorSurname() {
        return admittingDoctorSurname;
    }

    public void setAdmittingDoctorSurname(String admittingDoctorSurname) {
        this.admittingDoctorSurname = admittingDoctorSurname;
    }

    public String getAdmittingDoctorNpwz() {
        return admittingDoctorNpwz;
    }

    public void setAdmittingDoctorNpwz(String admittingDoctorNpwz) {
        this.admittingDoctorNpwz = admittingDoctorNpwz;
    }

    public String getAssignedPatientLocationName() {
        return assignedPatientLocationName;
    }

    public void setAssignedPatientLocationName(String assignedPatientLocationName) {
        this.assignedPatientLocationName = assignedPatientLocationName;
    }

    public Long getDischargeDate() {
        return dischargeDate;
    }

    public void setDischargeDate(Long dischargeDate) {
        this.dischargeDate = dischargeDate;
    }

    public String getHl7Id() {
        return hl7Id;
    }

    public void setHl7Id(String hl7Id) {
        this.hl7Id = hl7Id;
    }

    public String getStayType() {
        return stayType;
    }

    public void setStayType(String stayType) {
        this.stayType = stayType;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public Long getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Long createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Long getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(Long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public Long getLastModifiedById() {
        return lastModifiedById;
    }

    public void setLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
    }
}

package com.icupad.webmodel.general;

import java.util.List;

public class UserDto {
    private Long id;
    private String login;
    private String password;
    private List<String> roles;
    private String name;
    private String surname;
    private String hl7Id;
    private boolean passwordAttached;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getHl7Id() {
        return hl7Id;
    }

    public void setHl7Id(String hl7Id) {
        this.hl7Id = hl7Id;
    }

    public boolean isPasswordAttached() {
        return passwordAttached;
    }

    public void setPasswordAttached(boolean passwordAttached) {
        this.passwordAttached = passwordAttached;
    }
}

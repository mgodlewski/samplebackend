package com.icupad.webmodel.patient;

public class OperationDto {
    private Long id;
    private Long stayId;
    private Long operatingDate;
    private String description;
    private boolean archived;

    private Long createdById;
    private Long lastModifiedById;
    private Long createdDateTime;
    private Long lastModifiedDateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStayId() {
        return stayId;
    }

    public void setStayId(Long stayId) {
        this.stayId = stayId;
    }

    public Long getOperatingDate() {
        return operatingDate;
    }

    public void setOperatingDate(Long operatingDate) {
        this.operatingDate = operatingDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public Long getLastModifiedById() {
        return lastModifiedById;
    }

    public void setLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
    }

    public Long getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Long createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Long getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(Long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }
}

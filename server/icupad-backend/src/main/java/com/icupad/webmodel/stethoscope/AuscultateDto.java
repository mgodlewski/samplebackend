package com.icupad.webmodel.stethoscope;

public class AuscultateDto {
    private Long id;
    private String description;
    private String wavName;
    private Long auscultatePointId;
    private Long auscultateSuiteId;
    private Long pollResult;

    private Long createdById;
    private Long createdDateTime;
    private Long lastModifiedById;
    private Long lastModifiedDateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWavName() {
        return wavName;
    }

    public void setWavName(String wavName) {
        this.wavName = wavName;
    }

    public Long getAuscultatePointId() {
        return auscultatePointId;
    }

    public void setAuscultatePointId(Long auscultatePointId) {
        this.auscultatePointId = auscultatePointId;
    }

    public Long getAuscultateSuiteId() {
        return auscultateSuiteId;
    }

    public void setAuscultateSuiteId(Long auscultateSuiteId) {
        this.auscultateSuiteId = auscultateSuiteId;
    }

    public Long getPollResult() {
        return pollResult;
    }

    public void setPollResult(Long pollResult) {
        this.pollResult = pollResult;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public Long getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Long createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Long getLastModifiedById() {
        return lastModifiedById;
    }

    public void setLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
    }

    public Long getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(Long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }
}

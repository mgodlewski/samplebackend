package com.icupad.synchronization.webmodel.patient;

import com.icupad.webmodel.patient.PatientDto;

public class PatientSyncResponseDto {
    private Boolean success;
    private PatientDto patientDto;

    public PatientSyncResponseDto(Boolean success, PatientDto patientDto) {
        this.success = success;
        this.patientDto = patientDto;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public PatientDto getPatientDto() {
        return patientDto;
    }

    public void setPatientDto(PatientDto patientDto) {
        this.patientDto = patientDto;
    }
}

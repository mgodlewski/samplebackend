package com.icupad.synchronization.synchronizer.stethoscope;

import com.icupad.domain.User;
import com.icupad.entity.stethoscope.Auscultate;
import com.icupad.function.dtotoentity.stethoscope.AuscultateDtoToEntityFunction;
import com.icupad.function.entitytodto.stethoscope.AuscultateEntityToDtoFunction;
import com.icupad.repository.UserRepository;
import com.icupad.repository.stethoscope.AuscultateRepository;
import com.icupad.synchronization.webmodel.stethoscope.AuscultateSyncResponseDto;
import com.icupad.webmodel.stethoscope.AuscultateDto;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.sql.Timestamp;

@Service
public class AuscultateSynchronizer {
    @Inject
    private AuscultateRepository auscultateRepository;
    @Inject
    private AuscultateEntityToDtoFunction auscultateEntityToDtoFunction;
    @Inject
    private AuscultateDtoToEntityFunction auscultateDtoToEntityFunction;
    @Inject
    private UserRepository userRepository;

    private User user;
    private Timestamp timestamp;

    public AuscultateSyncResponseDto prepareResponseAndSaveValidIfNoConflict(AuscultateDto auscultateDto, Auscultate auscultate) {
        user = userRepository.findOne(1L);
        timestamp = new Timestamp(System.currentTimeMillis());

        if(auscultate == null) {
            return insertAuscultateBasedOnAuscultateDto(auscultateDto);
        }
        if(ifAuscultateDtoHasCurrentLastModifiedDateAndId(auscultateDto, auscultate)) {
            return updateAuscultateBasedOnAuscultateDto(auscultateDto);
        }
        if(ifAuscultateDtoHasNoNewData(auscultateDto, auscultate)) {
            return updateLastModifiedDate(auscultate);
        }
        return returnConflict(auscultate);
    }

    private AuscultateSyncResponseDto insertAuscultateBasedOnAuscultateDto(AuscultateDto auscultateDto) {
        Auscultate auscultate = auscultateDtoToEntityFunction.apply(auscultateDto);
        auscultate.setId(null);
        auscultate.setCreatedBy(user);
        auscultate.setCreatedDate(timestamp);
        auscultate.setLastModifiedBy(user);
        auscultate.setLastModifiedDate(timestamp);
        auscultate = auscultateRepository.save(auscultate);
        return new AuscultateSyncResponseDto(true, auscultateEntityToDtoFunction.apply(auscultate));
    }

    private AuscultateSyncResponseDto updateAuscultateBasedOnAuscultateDto(AuscultateDto auscultateDto) {
        Auscultate auscultate = auscultateDtoToEntityFunction.apply(auscultateDto);
        auscultate.setLastModifiedBy(user);
        auscultate.setLastModifiedDate(timestamp);
        auscultate = auscultateRepository.save(auscultate);
        return new AuscultateSyncResponseDto(true, auscultateEntityToDtoFunction.apply(auscultate));
    }

    private boolean ifAuscultateDtoHasCurrentLastModifiedDateAndId(AuscultateDto auscultateDto, Auscultate auscultate) {
        return auscultate.getLastModifiedDate().equals(new Timestamp(auscultateDto.getLastModifiedDateTime()))
                && auscultate.getId().equals(auscultateDto.getId());
    }

    private boolean ifAuscultateDtoHasNoNewData(AuscultateDto auscultateDto, Auscultate auscultate) {
        return false;
    }

    private AuscultateSyncResponseDto updateLastModifiedDate(Auscultate auscultate) {
        auscultate.setLastModifiedDate(timestamp);
        auscultate.setLastModifiedBy(user);
        auscultate = auscultateRepository.save(auscultate);
        return new AuscultateSyncResponseDto(true, auscultateEntityToDtoFunction.apply(auscultate));
    }

    private AuscultateSyncResponseDto returnConflict(Auscultate auscultate) {
        return new AuscultateSyncResponseDto(false, auscultateEntityToDtoFunction.apply(auscultate));
    }
}

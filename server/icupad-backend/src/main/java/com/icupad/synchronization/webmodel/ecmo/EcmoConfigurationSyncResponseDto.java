package com.icupad.synchronization.webmodel.ecmo;

import com.icupad.webmodel.ecmo.EcmoConfigurationDto;

/**
 * Created by Marcin on 02.04.2017.
 */
public class EcmoConfigurationSyncResponseDto {
    private Boolean success;
    private EcmoConfigurationDto ecmoConfigurationDto;

    public EcmoConfigurationSyncResponseDto(Boolean success, EcmoConfigurationDto ecmoConfigurationDto) {
        this.success = success;
        this.ecmoConfigurationDto = ecmoConfigurationDto;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public EcmoConfigurationDto getEcmoConfigurationDto() {
        return ecmoConfigurationDto;
    }

    public void setEcmoConfigurationDto(EcmoConfigurationDto ecmoConfigurationDto) {
        this.ecmoConfigurationDto = ecmoConfigurationDto;
    }
}

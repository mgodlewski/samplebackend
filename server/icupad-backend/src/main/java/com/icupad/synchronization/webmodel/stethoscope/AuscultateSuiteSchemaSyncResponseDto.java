package com.icupad.synchronization.webmodel.stethoscope;

import com.icupad.webmodel.stethoscope.AuscultateSuiteSchemaDto;

public class AuscultateSuiteSchemaSyncResponseDto {
    private Boolean success;
    private AuscultateSuiteSchemaDto auscultateSuiteSchemaDto;

    public AuscultateSuiteSchemaSyncResponseDto(Boolean success, AuscultateSuiteSchemaDto auscultateSuiteSchemaDto) {
        this.success = success;
        this.auscultateSuiteSchemaDto = auscultateSuiteSchemaDto;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public AuscultateSuiteSchemaDto getAuscultateSuiteSchemaDto() {
        return auscultateSuiteSchemaDto;
    }

    public void setAuscultateSuiteSchemaDto(AuscultateSuiteSchemaDto auscultateSuiteSchemaDto) {
        this.auscultateSuiteSchemaDto = auscultateSuiteSchemaDto;
    }
}

package com.icupad.synchronization.synchronizer.patient;

import com.icupad.domain.User;
import com.icupad.entity.patient.Operation;
import com.icupad.function.dtotoentity.patient.OperationDtoToEntityFunction;
import com.icupad.function.entitytodto.patient.OperationEntityToDtoFunction;
import com.icupad.repository.UserRepository;
import com.icupad.repository.patient.OperationRepository;
import com.icupad.webmodel.patient.OperationDto;
import com.icupad.synchronization.webmodel.patient.OperationSyncResponseDto;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.sql.Timestamp;

@Service
public class OperationSynchronizer {

    @Inject
    private OperationRepository operationRepository;
    @Inject
    private OperationDtoToEntityFunction operationDtoToEntityFunction;
    @Inject
    private OperationEntityToDtoFunction operationEntityToDtoFunction;
    @Inject
    private UserRepository userRepository;

    private User user;
    private Timestamp timestamp;

    public OperationSyncResponseDto prepareResponseAndSaveValidIfNoConflict(
            OperationDto operationDto, Operation operation) {
        user = userRepository.findOne(1L);
        timestamp = new Timestamp(System.currentTimeMillis());

        if(operation == null) {
            return insertOperationBasedOnOperationDto(operationDto);
        }
        if(ifOperationDtoHasCurrentLastModifiedDateAndId(operationDto, operation)) {
            return updateOperationBasedOnOperationDto(operationDto);
        }
        return returnConflict(operation);
    }

    private OperationSyncResponseDto insertOperationBasedOnOperationDto(OperationDto operationDto) {
        Operation operation = operationDtoToEntityFunction.apply(operationDto);
        operation.setCreatedBy(user);
        operation.setCreatedDate(timestamp);
        operation.setLastModifiedBy(user);
        operation.setLastModifiedDate(timestamp);
        operation = operationRepository.save(operation);

        return new OperationSyncResponseDto(true, operationEntityToDtoFunction.apply(operation));
    }

    private OperationSyncResponseDto updateOperationBasedOnOperationDto(OperationDto operationDto) {
        Operation operation = operationDtoToEntityFunction.apply(operationDto);
        operation.setLastModifiedBy(user);
        operation.setLastModifiedDate(timestamp);
        operation = operationRepository.save(operation);
        return new OperationSyncResponseDto(true, operationEntityToDtoFunction.apply(operation));
    }

    private boolean ifOperationDtoHasCurrentLastModifiedDateAndId(OperationDto operationDto, Operation operation) {
        return operation.getLastModifiedDate().equals(new Timestamp(operationDto.getLastModifiedDateTime()))
                && operation.getId().equals(operationDto.getId());
    }

    private OperationSyncResponseDto returnConflict(Operation operation) {
        return new OperationSyncResponseDto(false, operationEntityToDtoFunction.apply(operation));
    }

}

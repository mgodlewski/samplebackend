package com.icupad.synchronization.webmodel.patient;

import com.icupad.webmodel.patient.OperationDto;

public class OperationSyncResponseDto {
    private Boolean success;
    private OperationDto operationDto;

    public OperationSyncResponseDto(Boolean success, OperationDto operationDto) {
        this.success = success;
        this.operationDto = operationDto;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public OperationDto getOperationDto() {
        return operationDto;
    }

    public void setOperationDto(OperationDto operationDto) {
        this.operationDto = operationDto;
    }
}


package com.icupad.synchronization.synchronizer.stethoscope;

import com.icupad.domain.User;
import com.icupad.entity.stethoscope.AuscultateSuiteSchema;
import com.icupad.function.dtotoentity.stethoscope.AuscultateSuiteSchemaDtoToEntityFunction;
import com.icupad.function.entitytodto.stethoscope.AuscultateSuiteSchemaEntityToDtoFunction;
import com.icupad.repository.UserRepository;
import com.icupad.repository.stethoscope.AuscultateSuiteSchemaRepository;
import com.icupad.synchronization.webmodel.stethoscope.AuscultateSuiteSchemaSyncResponseDto;
import com.icupad.webmodel.stethoscope.AuscultateSuiteSchemaDto;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.sql.Timestamp;

@Service
public class AuscultateSuiteSchemaSynchronizer {
    @Inject
    private AuscultateSuiteSchemaRepository auscultateSuiteSchemaRepository;
    @Inject
    private AuscultateSuiteSchemaEntityToDtoFunction auscultateSuiteSchemaEntityToDtoFunction;
    @Inject
    private AuscultateSuiteSchemaDtoToEntityFunction auscultateSuiteSchemaDtoToEntityFunction;
    @Inject
    private UserRepository userRepository;

    private User user;
    private Timestamp timestamp;

    public AuscultateSuiteSchemaSyncResponseDto prepareResponseAndSaveValidIfNoConflict(AuscultateSuiteSchemaDto auscultateSuiteSchemaDto, AuscultateSuiteSchema auscultateSuiteSchema) {
        user = userRepository.findOne(1L);
        timestamp = new Timestamp(System.currentTimeMillis());

        //TODO sprawdź po nazwie, zawsze zwróć pozytywną wiadomość
        return insertAuscultateSuiteSchemaBasedOnAuscultateSuiteSchemaDto(auscultateSuiteSchemaDto);
    }

    private AuscultateSuiteSchemaSyncResponseDto insertAuscultateSuiteSchemaBasedOnAuscultateSuiteSchemaDto(AuscultateSuiteSchemaDto auscultateSuiteSchemaDto) {
        AuscultateSuiteSchema auscultateSuiteSchema = auscultateSuiteSchemaDtoToEntityFunction.apply(auscultateSuiteSchemaDto);
        auscultateSuiteSchema.setId(null);
        auscultateSuiteSchema.setCreatedBy(user);
        auscultateSuiteSchema.setCreatedDate(timestamp);
        auscultateSuiteSchema.setLastModifiedBy(user);
        auscultateSuiteSchema.setLastModifiedDate(timestamp);
        auscultateSuiteSchema = auscultateSuiteSchemaRepository.save(auscultateSuiteSchema);

        return new AuscultateSuiteSchemaSyncResponseDto(true, auscultateSuiteSchemaEntityToDtoFunction.apply(auscultateSuiteSchema));
    }
}

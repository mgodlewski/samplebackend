package com.icupad.synchronization.webmodel.stethoscope;

public class AuscultateRecordingSyncResponseDto {
    private Boolean success;

    public AuscultateRecordingSyncResponseDto(Boolean success) {
        this.success = success;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}

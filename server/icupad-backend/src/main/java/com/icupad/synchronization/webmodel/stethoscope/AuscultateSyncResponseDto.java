package com.icupad.synchronization.webmodel.stethoscope;

import com.icupad.webmodel.stethoscope.AuscultateDto;

public class AuscultateSyncResponseDto {
    private Boolean success;
    private AuscultateDto auscultateDto;

    public AuscultateSyncResponseDto(Boolean success, AuscultateDto auscultateDto) {
        this.success = success;
        this.auscultateDto = auscultateDto;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public AuscultateDto getAuscultateDto() {
        return auscultateDto;
    }

    public void setAuscultateDto(AuscultateDto auscultateDto) {
        this.auscultateDto = auscultateDto;
    }
}

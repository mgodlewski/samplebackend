package com.icupad.synchronization.webmodel.stethoscope;

import com.icupad.webmodel.stethoscope.AuscultateSuiteDto;

public class AuscultateSuiteSyncResponseDto {
    private Boolean success;
    private AuscultateSuiteDto auscultateSuiteDto;

    public AuscultateSuiteSyncResponseDto(Boolean success, AuscultateSuiteDto auscultateSuiteDto) {
        this.success = success;
        this.auscultateSuiteDto = auscultateSuiteDto;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public AuscultateSuiteDto getAuscultateSuiteDto() {
        return auscultateSuiteDto;
    }

    public void setAuscultateSuiteDto(AuscultateSuiteDto auscultateSuiteDto) {
        this.auscultateSuiteDto = auscultateSuiteDto;
    }
}

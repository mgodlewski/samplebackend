package com.icupad.synchronization.webmodel.ecmo;

import com.icupad.webmodel.ecmo.EcmoPatientDto;

/**
 * Created by Marcin on 02.04.2017.
 */
public class EcmoPatientSyncResponseDto {
    private Boolean success;
    private EcmoPatientDto ecmoPatientDto;

    public EcmoPatientSyncResponseDto(Boolean success, EcmoPatientDto ecmoPatientDto) {
        this.success = success;
        this.ecmoPatientDto = ecmoPatientDto;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public EcmoPatientDto getEcmoPatientDto() {
        return ecmoPatientDto;
    }

    public void setEcmoPatientDto(EcmoPatientDto ecmoPatientDto) {
        this.ecmoPatientDto = ecmoPatientDto;
    }
}

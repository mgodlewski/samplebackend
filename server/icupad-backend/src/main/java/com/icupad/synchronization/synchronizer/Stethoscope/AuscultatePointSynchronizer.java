package com.icupad.synchronization.synchronizer.stethoscope;

import com.icupad.domain.User;
import com.icupad.entity.stethoscope.AuscultatePoint;
import com.icupad.function.dtotoentity.stethoscope.AuscultatePointDtoToEntityFunction;
import com.icupad.function.entitytodto.stethoscope.AuscultatePointEntityToDtoFunction;
import com.icupad.repository.UserRepository;
import com.icupad.repository.stethoscope.AuscultatePointRepository;
import com.icupad.synchronization.webmodel.stethoscope.AuscultatePointSyncResponseDto;
import com.icupad.webmodel.stethoscope.AuscultatePointDto;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.sql.Timestamp;

@Component
public class AuscultatePointSynchronizer {
    @Inject
    private AuscultatePointRepository auscultatePointRepository;
    @Inject
    private AuscultatePointEntityToDtoFunction auscultatePointEntityToDtoFunction;
    @Inject
    private AuscultatePointDtoToEntityFunction auscultatePointDtoToEntityFunction;
    @Inject
    private UserRepository userRepository;

    private User user;
    private Timestamp timestamp;

    public AuscultatePointSyncResponseDto prepareResponseAndSaveValidIfNoConflict(AuscultatePointDto auscultatePointDto, AuscultatePoint auscultatePoint) {
        user = userRepository.findOne(1L);
        timestamp = new Timestamp(System.currentTimeMillis());

        if(auscultatePoint == null) {
            return insertAuscultatePointBasedOnAuscultateDto(auscultatePointDto);
        }
        if(ifAuscultatePointDtoHasCurrentLastModifiedDateAndId(auscultatePointDto, auscultatePoint)) {
            return updateAuscultatePointBasedOnAuscultateDto(auscultatePointDto);
        }
        if(ifAuscultatePointDtoHasNoNewData(auscultatePointDto, auscultatePoint)) {
            return updateLastModifiedDate(auscultatePoint);
        }
        return returnConflict(auscultatePoint);
    }

    private AuscultatePointSyncResponseDto insertAuscultatePointBasedOnAuscultateDto(AuscultatePointDto auscultatePointDto) {
        AuscultatePoint auscultatePoint = auscultatePointDtoToEntityFunction.apply(auscultatePointDto);
        auscultatePoint.setId(null);
        auscultatePoint.setCreatedBy(user);
        auscultatePoint.setCreatedDate(timestamp);
        auscultatePoint.setLastModifiedBy(user);
        auscultatePoint.setLastModifiedDate(timestamp);
        auscultatePoint = auscultatePointRepository.save(auscultatePoint);
        return new AuscultatePointSyncResponseDto(true, auscultatePointEntityToDtoFunction.apply(auscultatePoint));
    }

    private AuscultatePointSyncResponseDto updateAuscultatePointBasedOnAuscultateDto(AuscultatePointDto auscultatePointDto) {
        AuscultatePoint auscultatePoint = auscultatePointDtoToEntityFunction.apply(auscultatePointDto);
        auscultatePoint.setLastModifiedBy(user);
        auscultatePoint.setLastModifiedDate(timestamp);
        auscultatePoint = auscultatePointRepository.save(auscultatePoint);
        return new AuscultatePointSyncResponseDto(true, auscultatePointEntityToDtoFunction.apply(auscultatePoint));
    }

    private boolean ifAuscultatePointDtoHasCurrentLastModifiedDateAndId(AuscultatePointDto auscultatePointDto, AuscultatePoint auscultatePoint) {
        return auscultatePoint.getLastModifiedDate().equals(new Timestamp(auscultatePointDto.getLastModifiedDateTime()))
                && auscultatePoint.getId().equals(auscultatePointDto.getId());
    }

    private boolean ifAuscultatePointDtoHasNoNewData(AuscultatePointDto auscultatePointDto, AuscultatePoint auscultatePoint) {
        return false;
    }

    private AuscultatePointSyncResponseDto updateLastModifiedDate(AuscultatePoint auscultatePoint) {
        auscultatePoint.setLastModifiedDate(timestamp);
        auscultatePoint.setLastModifiedBy(user);
        auscultatePoint = auscultatePointRepository.save(auscultatePoint);
        return new AuscultatePointSyncResponseDto(true, auscultatePointEntityToDtoFunction.apply(auscultatePoint));
    }

    private AuscultatePointSyncResponseDto returnConflict(AuscultatePoint auscultatePoint) {
        return new AuscultatePointSyncResponseDto(false, auscultatePointEntityToDtoFunction.apply(auscultatePoint));
    }
}

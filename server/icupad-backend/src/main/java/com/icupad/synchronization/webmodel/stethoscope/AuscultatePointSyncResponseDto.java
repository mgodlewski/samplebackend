package com.icupad.synchronization.webmodel.stethoscope;

import com.icupad.webmodel.stethoscope.AuscultatePointDto;

public class AuscultatePointSyncResponseDto {
    private Boolean success;
    private AuscultatePointDto auscultatePointDto;

    public AuscultatePointSyncResponseDto(Boolean success, AuscultatePointDto auscultatePointDto) {
        this.success = success;
        this.auscultatePointDto = auscultatePointDto;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public AuscultatePointDto getAuscultatePointDto() {
        return auscultatePointDto;
    }

    public void setAuscultatePointDto(AuscultatePointDto auscultatePointDto) {
        this.auscultatePointDto = auscultatePointDto;
    }
}

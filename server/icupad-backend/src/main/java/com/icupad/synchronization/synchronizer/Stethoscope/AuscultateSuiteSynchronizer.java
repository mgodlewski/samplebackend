package com.icupad.synchronization.synchronizer.stethoscope;

import com.icupad.domain.User;
import com.icupad.entity.stethoscope.AuscultateSuite;
import com.icupad.function.dtotoentity.stethoscope.AuscultateDtoToEntityFunction;
import com.icupad.function.dtotoentity.stethoscope.AuscultateSuiteDtoToEntityFunction;
import com.icupad.function.entitytodto.stethoscope.AuscultateSuiteEntityToDtoFunction;
import com.icupad.repository.UserRepository;
import com.icupad.repository.stethoscope.AuscultateRepository;
import com.icupad.repository.stethoscope.AuscultateSuiteRepository;
import com.icupad.synchronization.webmodel.stethoscope.AuscultateSuiteSyncResponseDto;
import com.icupad.webmodel.stethoscope.AuscultateSuiteDto;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.sql.Timestamp;

@Service
public class AuscultateSuiteSynchronizer {
    @Inject
    private AuscultateRepository auscultateRepository;
    @Inject
    private AuscultateSynchronizer auscultateSynchronizer;
    @Inject
    private AuscultateSuiteRepository auscultateSuiteRepository;
    @Inject
    private AuscultateSuiteEntityToDtoFunction auscultateSuiteEntityToDtoFunction;
    @Inject
    private AuscultateSuiteDtoToEntityFunction auscultateSuiteDtoToEntityFunction;
    @Inject
    private UserRepository userRepository;
    @Inject
    private AuscultateDtoToEntityFunction auscultateDtoToEntityFunction;

    private User user;
    private Timestamp timestamp;
    private boolean isNotConflict = true;

    public AuscultateSuiteSyncResponseDto prepareResponseAndSaveValidIfNoConflict(AuscultateSuiteDto auscultateSuiteDto, AuscultateSuite auscultateSuite) {
        user = userRepository.findOne(1L);
        timestamp = new Timestamp(System.currentTimeMillis());

        if(auscultateSuite == null) {
            return insertAuscultateSuiteBasedOnAuscultateSuiteDto(auscultateSuiteDto);
        }
        if(ifAuscultateSuiteDtoHasCurrentLastModifiedDateAndId(auscultateSuiteDto, auscultateSuite)) {
            return updateAuscultateSuiteBasedOnAuscultateSuiteDto(auscultateSuiteDto);
        }
        if(ifAuscultateSuiteDtoHasNoNewData(auscultateSuiteDto, auscultateSuite)) {
            return updateLastModifiedDate(auscultateSuiteDto, auscultateSuite);
        }
        return returnConflict(auscultateSuite);
    }

    private AuscultateSuiteSyncResponseDto insertAuscultateSuiteBasedOnAuscultateSuiteDto(AuscultateSuiteDto auscultateSuiteDto) {
        AuscultateSuite auscultateSuite = auscultateSuiteDtoToEntityFunction.apply(auscultateSuiteDto);
        auscultateSuite.setId(null);
        auscultateSuite.setCreatedBy(user);
        auscultateSuite.setCreatedDate(timestamp);
        auscultateSuite.setLastModifiedBy(user);
        auscultateSuite.setLastModifiedDate(timestamp);
        auscultateSuite = auscultateSuiteRepository.save(auscultateSuite);

        return new AuscultateSuiteSyncResponseDto(isNotConflict, auscultateSuiteEntityToDtoFunction.apply(auscultateSuite));
    }

    private AuscultateSuiteSyncResponseDto updateAuscultateSuiteBasedOnAuscultateSuiteDto(AuscultateSuiteDto auscultateSuiteDto) {
        AuscultateSuite auscultateSuite = auscultateSuiteDtoToEntityFunction.apply(auscultateSuiteDto);
        auscultateSuite.setLastModifiedBy(user);
        auscultateSuite.setLastModifiedDate(timestamp);
        auscultateSuite = auscultateSuiteRepository.save(auscultateSuite);
        return new AuscultateSuiteSyncResponseDto(isNotConflict, auscultateSuiteEntityToDtoFunction.apply(auscultateSuite));
    }

    private boolean ifAuscultateSuiteDtoHasCurrentLastModifiedDateAndId(AuscultateSuiteDto auscultateSuiteDto, AuscultateSuite auscultateSuite) {
        return auscultateSuite.getLastModifiedDate().equals(new Timestamp(auscultateSuiteDto.getLastModifiedDateTime()))
                && auscultateSuite.getId().equals(auscultateSuiteDto.getId());
    }

    private boolean ifAuscultateSuiteDtoHasNoNewData(AuscultateSuiteDto auscultateSuiteDto, AuscultateSuite auscultateSuite) {
        return false;
    }

    private AuscultateSuiteSyncResponseDto updateLastModifiedDate(AuscultateSuiteDto auscultateSuiteDto, AuscultateSuite auscultateSuite) {
        auscultateSuite.setLastModifiedDate(new Timestamp(auscultateSuiteDto.getLastModifiedDateTime()));
        auscultateSuite = auscultateSuiteRepository.save(auscultateSuite);
        return new AuscultateSuiteSyncResponseDto(isNotConflict, auscultateSuiteEntityToDtoFunction.apply(auscultateSuite));
    }

    private AuscultateSuiteSyncResponseDto returnConflict(AuscultateSuite auscultateSuite) {
        return new AuscultateSuiteSyncResponseDto(false, auscultateSuiteEntityToDtoFunction.apply(auscultateSuite));
    }
}

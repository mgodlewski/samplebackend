package com.icupad.synchronization.webmodel.ecmo;

import com.icupad.webmodel.ecmo.EcmoProcedureDto;
import com.icupad.webmodel.patient.OperationDto;

/**
 * Created by Marcin on 02.04.2017.
 */
public class EcmoProcedureSyncResponseDto {
    private Boolean success;
    private EcmoProcedureDto ecmoProcedureDto;

    public EcmoProcedureSyncResponseDto(Boolean success, EcmoProcedureDto ecmoProcedureDto) {
        this.success = success;
        this.ecmoProcedureDto = ecmoProcedureDto;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public EcmoProcedureDto getEcmoProcedureDto() {
        return ecmoProcedureDto;
    }

    public void setEcmoProcedureDto(EcmoProcedureDto ecmoProcedureDto) {
        this.ecmoProcedureDto = ecmoProcedureDto;
    }
}

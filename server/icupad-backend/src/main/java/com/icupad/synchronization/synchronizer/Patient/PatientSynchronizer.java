package com.icupad.synchronization.synchronizer.patient;

import com.icupad.domain.User;
import com.icupad.entity.patient.Patient;
import com.icupad.function.dtotoentity.patient.PatientDtoToEntityFunction;
import com.icupad.function.entitytodto.patient.PatientEntityToDtoFunction;
import com.icupad.repository.UserRepository;
import com.icupad.repository.patient.PatientRepository;
import com.icupad.webmodel.patient.PatientDto;
import com.icupad.synchronization.webmodel.patient.PatientSyncResponseDto;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.sql.Timestamp;

@Service
public class PatientSynchronizer {
    @Inject
    private PatientRepository patientRepository;
    @Inject
    private PatientEntityToDtoFunction patientEntityToDtoFunction;
    @Inject
    private PatientDtoToEntityFunction patientDtoToEntityFunction;
    @Inject
    private UserRepository userRepository;

    private User user;
    private Timestamp timestamp;

    public PatientSyncResponseDto prepareResponseAndSaveValidIfNoConflict(PatientDto patientDto, Patient patient) {
        user = userRepository.findOne(1L);
        timestamp = new Timestamp(System.currentTimeMillis());

        if(patient == null) {
            return insertPatientBasedOnPatientDto(patientDto);
        }
        if(ifPatientDtoHasCurrentLastModifiedDateAndId(patientDto, patient)) {
            return updatePatientBasedOnPatientDto(patientDto);
        }
        if(ifPatientDtoHasNoNewData(patientDto, patient)) {
            return updateLastModifiedDate(patient);
        }
        return returnConflict(patient);
    }

    private PatientSyncResponseDto insertPatientBasedOnPatientDto(PatientDto patientDto) {
        Patient patient = patientDtoToEntityFunction.apply(patientDto);
        patient.setCreatedBy(user);
        patient.setCreatedDate(timestamp);
        patient.setLastModifiedBy(user);
        patient.setLastModifiedDate(timestamp);
        patient = patientRepository.save(patient);

        return new PatientSyncResponseDto(true, patientEntityToDtoFunction.apply(patient));
    }

    private PatientSyncResponseDto updatePatientBasedOnPatientDto(PatientDto patientDto) {
        Patient patient = patientDtoToEntityFunction.apply(patientDto);
        patient.setLastModifiedBy(user);
        patient.setLastModifiedDate(timestamp);
        patient = patientRepository.save(patient);
        return new PatientSyncResponseDto(true, patientEntityToDtoFunction.apply(patient));
    }

    private boolean ifPatientDtoHasCurrentLastModifiedDateAndId(PatientDto patientDto, Patient patient) {
        return patient.getLastModifiedDate().equals(new Timestamp(patientDto.getLastModifiedDateTime()))
                && patient.getId().equals(patientDto.getId());
    }

    private boolean ifPatientDtoHasNoNewData(PatientDto patientDto, Patient patient) {
        return patient.getBloodType().equals(patientDto.getBloodType()) &&
                patient.getHeight().equals(patientDto.getHeight()) &&
                patient.getWeight().equals(patientDto.getWeight()) &&
                patient.getAllergies().equals(patientDto.getAllergies()) &&
                patient.getOtherImportantInformations().equals(patientDto.getOtherImportantInformations());
    }

    private PatientSyncResponseDto updateLastModifiedDate(Patient patient) {
        patient.setLastModifiedDate(timestamp);
        patient.setLastModifiedBy(user);
        patient = patientRepository.save(patient);
        return new PatientSyncResponseDto(true, patientEntityToDtoFunction.apply(patient));
    }

    private PatientSyncResponseDto returnConflict(Patient patient) {
        return new PatientSyncResponseDto(false, patientEntityToDtoFunction.apply(patient));
    }
}

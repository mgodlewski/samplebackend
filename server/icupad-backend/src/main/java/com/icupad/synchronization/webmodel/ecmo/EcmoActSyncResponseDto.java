package com.icupad.synchronization.webmodel.ecmo;

import com.icupad.webmodel.ecmo.EcmoActDto;

/**
 * Created by Marcin on 02.04.2017.
 */
public class EcmoActSyncResponseDto {
    private Boolean success;
    private EcmoActDto ecmoActDto;

    public EcmoActSyncResponseDto(Boolean success, EcmoActDto ecmoActDto) {
        this.success = success;
        this.ecmoActDto = ecmoActDto;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public EcmoActDto getEcmoActDto() {
        return ecmoActDto;
    }

    public void setEcmoActDto(EcmoActDto ecmoActDto) {
        this.ecmoActDto = ecmoActDto;
    }
}

package com.icupad.synchronization.webmodel.ecmo;

import com.icupad.webmodel.ecmo.EcmoParameterDto;

/**
 * Created by Marcin on 02.04.2017.
 */
public class EcmoParameterSyncResponseDto {
    private Boolean success;
    private EcmoParameterDto ecmoParameterDto;

    public EcmoParameterSyncResponseDto(Boolean success, EcmoParameterDto ecmoParameterDto) {
        this.success = success;
        this.ecmoParameterDto = ecmoParameterDto;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public EcmoParameterDto getEcmoParameterDto() {
        return ecmoParameterDto;
    }

    public void setEcmoParameterDto(EcmoParameterDto ecmoParameterDto) {
        this.ecmoParameterDto = ecmoParameterDto;
    }
}

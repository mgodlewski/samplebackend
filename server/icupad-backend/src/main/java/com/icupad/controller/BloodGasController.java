package com.icupad.controller;

import com.icupad.webmodel.bloodgas.BloodGasTestMeasurementDto;
import com.icupad.service.bloodgas.*;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

import static com.icupad.controller.BloodGasController.BASE_PATH;

@RestController
@RequestMapping(BASE_PATH)
public class BloodGasController {

    public static final String BASE_PATH = "/BloodGasTest";

    private static final int PAGE_SIZE = 10000;

    @Inject
    private BloodGasTestService bloodGasTestService;

    @RequestMapping(method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BloodGasTestMeasurementDto> getMeasurementsAfterAndPatientIdIn(@RequestParam Long after, @RequestParam int page, @RequestBody List<Long> patientIds) {
        return bloodGasTestService.getMeasurementsAfterAndPatientIdIn(after, patientIds, new PageRequest(page,PAGE_SIZE));
    }

}

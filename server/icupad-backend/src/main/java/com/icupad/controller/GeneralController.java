package com.icupad.controller;

import com.icupad.service.general.LastTimestampService;
import com.icupad.service.general.UserWebService;
import com.icupad.webmodel.general.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

import java.util.List;

import static com.icupad.controller.GeneralController.BASE_PATH;

@RestController
@RequestMapping(BASE_PATH)
@PropertySource("classpath:application.properties")
public class GeneralController {
    public static final String BASE_PATH = "/";
    public static final String PING_PATH = "ping";
    public static final String NO_AUTH_PING_PATH = "noAuthPing";
    public static final String SQLITE_PASSWORD_PATH = "SqlitePassword";
    public static final String LOGGED_USER_INFO = "loggedUserInfo";
    public static final String USER = "user";
    public static final String USERS_LIST = "user/all";
    private static final String ICUPAD_LAST_TIMESTAMP = "lastTimestamp/icupad";
    private static final String ESKULAP_LAST_TIMESTAMP = "lastTimestamp/eskulap";

    @Autowired
    private Environment env;
    @Inject
    private UserWebService userWebService;
    @Inject
    private LastTimestampService lastTimestampService;

    @RequestMapping(value = NO_AUTH_PING_PATH, method = RequestMethod.GET)
    public String noAuthPing() {
        return "noAuthPong";
    }

    @RequestMapping(value = PING_PATH, method = RequestMethod.GET)
    public String ping() {
        return "pong";
    }

    @RequestMapping(value = SQLITE_PASSWORD_PATH, method = RequestMethod.GET)
    public String getSqlitePassword() {
        return env.getProperty("android.sqlite.password");
    }

    @RequestMapping(value = ESKULAP_LAST_TIMESTAMP, method = RequestMethod.GET)
    public Long getEskulapLastTimestamp() {
        return lastTimestampService.getForEskulap();
    }

    //INFO If Eskulap would get timestamp from different source than icupad then it should be separated
    @RequestMapping(value = ICUPAD_LAST_TIMESTAMP, method = RequestMethod.GET)
    public Long getIcupadLastTimestamp() {
        return Math.max(lastTimestampService.getForIcupad(),lastTimestampService.getForEskulap());
    }

    @RequestMapping(value = LOGGED_USER_INFO, method = RequestMethod.GET)
    public UserDto getLoggedUserInfo() {
        return userWebService.getLoggedUserInfo();
    }

    @RequestMapping(value = USERS_LIST, method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserDto>> getUsers() {
        return userWebService.getUsers();
    }

    @RequestMapping(value = USER, method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> addUser(@RequestBody UserDto userDto) {
        return userWebService.addUser(userDto);
    }

    @RequestMapping(value = USER + "/{login}", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> updateUser(@RequestBody UserDto userDto, @PathVariable String login) {
        return userWebService.updateUser(userDto, login);
    }
}


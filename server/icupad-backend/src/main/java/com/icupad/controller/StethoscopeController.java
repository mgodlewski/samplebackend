package com.icupad.controller;

import com.icupad.service.stethoscope.*;
import com.icupad.synchronization.webmodel.stethoscope.*;
import com.icupad.webmodel.stethoscope.AuscultateDto;
import com.icupad.webmodel.stethoscope.AuscultatePointDto;
import com.icupad.webmodel.stethoscope.AuscultateSuiteDto;
import com.icupad.webmodel.stethoscope.AuscultateSuiteSchemaDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;

import java.text.ParseException;
import java.util.List;

import static com.icupad.controller.StethoscopeController.BASE_PATH;

@RestController
@RequestMapping(BASE_PATH)
public class StethoscopeController {

    public static final String BASE_PATH = "/Stethoscope";
    private static final String AUSCULTATE_PATH = "/Auscultate";
    private static final String AUSCULTATE_POINT_PATH = "/AuscultatePoint";
    private static final String AUSCULTATE_RECORDING_PATH = "/AuscultateRecording";
    private static final String AUSCULTATE_SUITE_PATH = "/AuscultateSuite";
    private static final String AUSCULTATE_SUITE_SCHEMA_PATH = "/AuscultateSuiteSchema";
    private static final String MYWHIZZY_GET_ANONYMOUS_DATA_PATH = "/MyWhizzy/GetAnonymousDataAfter";
    private static final String UPLOAD_PATH = "/upload";

    @Inject
    private AuscultateService auscultateService;
    @Inject
    private AuscultatePointService auscultatePointService;
    @Inject
    private AuscultateRecordingService auscultateRecordingService;
    @Inject
    private AuscultateSuiteService auscultateSuiteService;
    @Inject
    private AuscultateSuiteSchemaService auscultateSuiteSchemaService;
    @Inject
    private AnonymousAuscultateDataService anonymousAuscultateDataService;

    @RequestMapping(value = AUSCULTATE_PATH, method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AuscultateDto> getAuscultatesAfter(@RequestParam Long after, @RequestBody List<Long> patientIds) {
        return auscultateService.getAuscultatesAfterAndPatientIdIn(after, patientIds);
    }

    @RequestMapping(value = AUSCULTATE_PATH + UPLOAD_PATH, method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public AuscultateSyncResponseDto uploadAuscultate(@RequestBody AuscultateDto auscultateDto) {
        return auscultateService.uploadAuscultate(auscultateDto);
    }

    @RequestMapping(value = AUSCULTATE_POINT_PATH, method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AuscultatePointDto> getPointsAfter(@RequestParam Long after) {
        return auscultatePointService.getPointsAfter(after);
    }

    @RequestMapping(value = AUSCULTATE_POINT_PATH + UPLOAD_PATH, method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public AuscultatePointSyncResponseDto uploadPoint(@RequestBody AuscultatePointDto auscultatePointDto) {
        return auscultatePointService.uploadPoint(auscultatePointDto);
    }

    @RequestMapping(value = AUSCULTATE_SUITE_PATH, method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AuscultateSuiteDto> getSuitesAfter(@RequestParam Long after, @RequestBody List<Long> patientIds) {
        return auscultateSuiteService.getSuitesAfterAndPatientIdIn(after, patientIds);
    }

    @RequestMapping(value = AUSCULTATE_SUITE_PATH + UPLOAD_PATH, method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public AuscultateSuiteSyncResponseDto uploadSuite(@RequestBody AuscultateSuiteDto auscultateSuiteDto) {
        return auscultateSuiteService.uploadSuite(auscultateSuiteDto);
    }

    @RequestMapping(value = AUSCULTATE_SUITE_SCHEMA_PATH, method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AuscultateSuiteSchemaDto> getSuiteSchemasAfter(@RequestParam Long after) {
        return auscultateSuiteSchemaService.getSuiteSchemasAfter(after);
    }

    @RequestMapping(value = AUSCULTATE_SUITE_SCHEMA_PATH + UPLOAD_PATH, method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public AuscultateSuiteSchemaSyncResponseDto uploadSuiteSchema(@RequestBody AuscultateSuiteSchemaDto auscultateSuiteSchemaDto) {
        return auscultateSuiteSchemaService.uploadSuiteSchema(auscultateSuiteSchemaDto);
    }

    //TODO
    @RequestMapping(value = AUSCULTATE_RECORDING_PATH, method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public MultipartFile getRecordingById(@RequestParam String name) {
        return auscultateRecordingService.getRecordingByName(name);
    }

    @RequestMapping(value = AUSCULTATE_RECORDING_PATH, method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public AuscultateRecordingSyncResponseDto uploadRecording(@RequestParam("file") MultipartFile multipartFile) {
        return auscultateRecordingService.addRecording(multipartFile);
    }

    @RequestMapping(value = MYWHIZZY_GET_ANONYMOUS_DATA_PATH, method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> getAnonymousAuscultateDataAfter(@RequestParam String after) throws ParseException {
        return anonymousAuscultateDataService.getDataAfter(after);
    }
}

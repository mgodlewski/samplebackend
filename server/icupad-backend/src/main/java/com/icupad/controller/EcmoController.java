package com.icupad.controller;

import com.icupad.entity.ecmo.EcmoProcedure;
import com.icupad.service.ecmo.*;
import com.icupad.synchronization.webmodel.ecmo.*;
import com.icupad.webmodel.ecmo.*;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

import static com.icupad.controller.EcmoController.BASE_PATH;


/**
 * Created by Marcin on 04.03.2017.
 */
//TODO refactor rest get methods
@RestController
@RequestMapping(BASE_PATH)
public class EcmoController {

    public static final String BASE_PATH = "/ecmo";
    public static final String CONFIGURATION_PATH = "/configuration";
    public static final String PATIENT_PATH = "/patient";
    public static final String ACT_PATH = "/act";
    public static final String PARAMETER_PATH = "/parameter";
    public static final String GET = "/get";

    private static final int PAGE_SIZE = 10000;

    @Inject private EcmoProcedureService ecmoProcedureService;
    @Inject private EcmoConfigurationService ecmoConfigurationService;
    @Inject private EcmoPatientService ecmoPatientService;
    @Inject private EcmoActService ecmoActService;
    @Inject private EcmoParameterService ecmoParameterService;

    /**********GET**********/
    @RequestMapping(value = GET,
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8",
            produces = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8")
    public List<EcmoProcedureDto> getProcedureAfterAndPatientIdIn(@RequestParam Long after, @RequestParam int page, @RequestBody List<Long> patientIds) {
        return ecmoProcedureService.getProcedureAfterAndPatientIdIn(after, patientIds, new PageRequest(page,PAGE_SIZE));
    }

    @RequestMapping(value = CONFIGURATION_PATH+GET,
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8",
            produces = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8")
    public List<EcmoConfigurationDto> getConfigurationAfterAndPatientIdIn(@RequestParam Long after, @RequestParam int page, @RequestBody List<Long> patientIds) {
        return ecmoConfigurationService.getConfigurationAfterAndPatientIdIn(after, patientIds, new PageRequest(page,PAGE_SIZE));
    }

    @RequestMapping(value = PATIENT_PATH+GET,
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8",
            produces = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8")
    public List<EcmoPatientDto> getPatientAfterAndPatientIdIn(@RequestParam Long after, @RequestParam int page, @RequestBody List<Long> patientIds) {
        return ecmoPatientService.getPatientAfterAndPatientIdIn(after, patientIds, new PageRequest(page,PAGE_SIZE));
    }

    @RequestMapping(value = ACT_PATH+GET,
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8",
            produces = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8")
    public List<EcmoActDto> getActAfterAndPatientIdIn(@RequestParam Long after, @RequestParam int page, @RequestBody List<Long> patientIds) {
        return ecmoActService.getActAfterAndPatientIdIn(after, patientIds, new PageRequest(page,PAGE_SIZE));
    }

    @RequestMapping(value = PARAMETER_PATH+GET,
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8",
            produces = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8")
    public List<EcmoParameterDto> getParameterAfterAndPatientIdIn(@RequestParam Long after, @RequestParam int page, @RequestBody List<Long> patientIds) {
        return ecmoParameterService.getParameterAfterAndPatientIdIn(after, patientIds, new PageRequest(page,PAGE_SIZE));
    }

    /**********POST**********/
    @RequestMapping(method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8",
            produces = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8")
    public EcmoProcedureSyncResponseDto createProcedure(@RequestBody EcmoProcedureDto ecmoProcedureDto) {
        EcmoProcedureDto createdEcmoProcedureDto = ecmoProcedureService.saveProcedure(ecmoProcedureDto);
        return new EcmoProcedureSyncResponseDto(true, createdEcmoProcedureDto);
    }

    @RequestMapping(value = CONFIGURATION_PATH,
            consumes = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8",
            produces = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8")
    public EcmoConfigurationSyncResponseDto createConfiguration(@RequestBody EcmoConfigurationDto ecmoConfigurationDto) {
        Optional<EcmoConfigurationDto> createdEcmoConfigurationDto = ecmoConfigurationService.createConfiguration(ecmoConfigurationDto);
        if(createdEcmoConfigurationDto.isPresent()){
            return new EcmoConfigurationSyncResponseDto(true, createdEcmoConfigurationDto.get());
        }else{
            return new EcmoConfigurationSyncResponseDto(false, null);
        }
    }

    @RequestMapping(value = PATIENT_PATH,
            consumes = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8",
            produces = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8")
    public EcmoPatientSyncResponseDto createPatient(@RequestBody EcmoPatientDto ecmoPatientDto) {
        Optional<EcmoPatientDto> createdEcmoPatientDto = ecmoPatientService.createPatient(ecmoPatientDto);
        if(createdEcmoPatientDto.isPresent()){
            return new EcmoPatientSyncResponseDto(true, createdEcmoPatientDto.get());
        }else{
            return new EcmoPatientSyncResponseDto(false, null);
        }
    }

    @RequestMapping(value = ACT_PATH,
            consumes = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8",
            produces = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8")
    public EcmoActSyncResponseDto createAct(@RequestBody EcmoActDto ecmoActDto) {
        Optional<EcmoActDto> createdEcmoActDto = ecmoActService.createAct(ecmoActDto);
        if(createdEcmoActDto.isPresent()){
            return new EcmoActSyncResponseDto(true, createdEcmoActDto.get());
        }else{
            return new EcmoActSyncResponseDto(false, null);
        }
    }

    @RequestMapping(value = PARAMETER_PATH,
            consumes = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8",
            produces = MediaType.APPLICATION_JSON_VALUE+";charset=UTF-8")
    public EcmoParameterSyncResponseDto createParameter(@RequestBody EcmoParameterDto ecmoParameterDto) {
        Optional<EcmoParameterDto> createdEcmoParameterDto = ecmoParameterService.createParameter(ecmoParameterDto);
        if(createdEcmoParameterDto.isPresent()){
            return new EcmoParameterSyncResponseDto(true, createdEcmoParameterDto.get());
        }else{
            return new EcmoParameterSyncResponseDto(false, null);
        }
    }
}

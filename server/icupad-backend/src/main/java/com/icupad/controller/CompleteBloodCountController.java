package com.icupad.controller;

import com.icupad.service.completebloodcount.CompleteBloodCountTestService;
import com.icupad.webmodel.completebloodcount.*;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

import static com.icupad.controller.CompleteBloodCountController.BASE_PATH;

/**
 * Created by Marcin on 22.05.2017.
 */
@RestController
@RequestMapping(BASE_PATH)
public class CompleteBloodCountController {

    public static final String BASE_PATH = "/CompleteBloodCount";

    private static final int PAGE_SIZE = 10000;

    @Inject
    private CompleteBloodCountTestService completeBloodCountTestService;


    @RequestMapping(method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CompleteBloodCountTestMeasurementDto> getMeasurementsAfterAndPatientIdIn(@RequestParam Long after, @RequestParam int page, @RequestBody List<Long> patientIds) {
        return completeBloodCountTestService.getMeasurementsAfterAndPatientIdIn(after, patientIds, new PageRequest(page,PAGE_SIZE));
    }

}

package com.icupad.controller;

import com.icupad.service.patient.OperationService;
import com.icupad.service.patient.PatientService;
import com.icupad.service.patient.StayService;
import com.icupad.synchronization.webmodel.patient.OperationSyncResponseDto;
import com.icupad.synchronization.webmodel.patient.PatientSyncResponseDto;
import com.icupad.webmodel.patient.OperationDto;
import com.icupad.webmodel.patient.PatientDto;
import com.icupad.webmodel.patient.StayDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

import java.util.List;

import static com.icupad.controller.PatientController.BASE_PATH;

@RestController
@RequestMapping(BASE_PATH)
public class PatientController {

    public static final String BASE_PATH = "/Patient";
    public static final String STAY_PATH = "/Stay";
    public static final String OPERATION_PATH = "/Operation";
    private static final String UPLOAD_PATH = "/upload";

    @Inject
    private PatientService patientService;
    @Inject
    private OperationService operationService;
    @Inject
    private StayService stayService;

    @RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PatientDto> getPatientsAfter(@RequestParam Long after) {
        if (after>0L) {
            return patientService.getPatientsAfter(after);
        }else{
            return patientService.getActivePatientsAfter(after);
        }
    }

    @RequestMapping(method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public PatientSyncResponseDto uploadPatient(@RequestBody PatientDto patientDto) {
        return patientService.uploadPatient(patientDto);
    }

    @RequestMapping(value = OPERATION_PATH, method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OperationDto> getOperationsAfter(@RequestParam Long after, @RequestBody List<Long> patientIds) {
        return operationService.getOperationsAfter(after, patientIds);
    }

    @RequestMapping(value = OPERATION_PATH + UPLOAD_PATH, method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public OperationSyncResponseDto uploadOperation(@RequestBody OperationDto operationDto) {
        return operationService.uploadOperation(operationDto);
    }

    @RequestMapping(value = STAY_PATH, method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<StayDto> getStaysAfter(@RequestParam Long after) {
        if (after > 0L) {
            return stayService.getStaysAfter(after);
        }else{
            return stayService.getActiveStaysAfter(after);
        }

    }
}

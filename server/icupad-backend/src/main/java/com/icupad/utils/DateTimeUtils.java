package com.icupad.utils;

import java.sql.Timestamp;

public class DateTimeUtils {
    public static Timestamp getTimestampOrNullFromLong(Long k) {
        if(k == null) {
            return null;
        }
        else {
            return new Timestamp(k);
        }
    }
}

package com.icupad.function.dtotoentity.stethoscope;

import com.icupad.builder.entitybuilder.stethoscope.AuscultateSuiteSchemaBuilder;
import com.icupad.domain.User;
import com.icupad.entity.stethoscope.AuscultateSuiteSchema;
import com.icupad.repository.UserRepository;
import com.icupad.utils.DateTimeUtils;
import com.icupad.webmodel.stethoscope.AuscultateSuiteSchemaDto;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.function.Function;

@Component
public class AuscultateSuiteSchemaDtoToEntityFunction implements Function<AuscultateSuiteSchemaDto, AuscultateSuiteSchema> {
    @Inject
    private UserRepository userRepository;

    @Override
    public AuscultateSuiteSchema apply(AuscultateSuiteSchemaDto input) {
        User createdByUser = userRepository.findOne(input.getCreatedById());
        User lastModifiedByUser = userRepository.findOne(input.getLastModifiedById());

        AuscultateSuiteSchema auscultateSuiteSchema = AuscultateSuiteSchemaBuilder.anAuscultateSuiteSchema()
                .withName(input.getName())
                .withIsPublic(input.getIsPublic())

                .withId(input.getId())
                .withCreatedDate(DateTimeUtils.getTimestampOrNullFromLong(input.getCreatedDateTime()))
                .withLastModifiedDate(DateTimeUtils.getTimestampOrNullFromLong(input.getLastModifiedDateTime()))
                .withCreatedBy(createdByUser)
                .withLastModifiedBy(lastModifiedByUser)
                .build();
        return auscultateSuiteSchema;
    }
}

package com.icupad.function.utils;

import com.icupad.domain.User;
import com.icupad.entity.enums.Abnormality;
import com.icupad.entity.patient.Address;
import com.icupad.function.entitytodto.patient.AddressEntityToDtoFunction;
import com.icupad.webmodel.patient.AddressDto;

import java.sql.Timestamp;

public class Utils {

    public static AddressDto getAddress(Address address) {
        if (address == null){
            return null;
        }
        return new AddressEntityToDtoFunction().apply(address);
    }

    public static Long getIdOrNull(User user) {
        if(user == null) {
            return null;
        }
        return user.getId();
    }

    public static Long getTimeOrNull(Timestamp timestamp) {
        if(timestamp == null) {
            return null;
        }
        return timestamp.getTime();
    }

    public static String toStringOrNull(Enum abnormality) {
        if (abnormality != null) {
            return abnormality.toString();
        }
        {
            return null;
        }
    }
}

package com.icupad.function.entitytodto.patient;

import com.icupad.builder.dtobuilder.patient.StayDtoBuilder;
import com.icupad.entity.patient.Stay;
import com.icupad.function.utils.Utils;
import com.icupad.webmodel.patient.StayDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class StayEntityToDtoFunction implements Function<Stay, StayDto> {

    @Override
    public StayDto apply(Stay input) {
        return StayDtoBuilder.aStayWebModel()
                .withId(input.getId())
                .withActive(input.isActive())
                .withAdmitDate(Utils.getTimeOrNull(input.getAdmitDate()))
                .withAdmittingDoctorHl7Id(input.getAdmittingDoctor().getHl7Id())
                .withAdmittingDoctorName(input.getAdmittingDoctor().getName())
                .withAdmittingDoctorSurname(input.getAdmittingDoctor().getSurname())
                .withAdmittingDoctorNpwz(input.getAdmittingDoctor().getNpwz())
                .withAssignedPatientLocationName(input.getAssignedPatientLocation().getName())
                .withDischargeDate(Utils.getTimeOrNull(input.getDischargeDate()))
                .withHl7Id(input.getHl7Id())
                .withStayType(input.getType().toString())
                .withPatientId(input.getPatient().getId())
                .withCreatedDateTime(Utils.getTimeOrNull(input.getCreatedDate()))
                .withLastModifiedDateTime(Utils.getTimeOrNull(input.getLastModifiedDate()))
                .withCreatedById(Utils.getIdOrNull(input.getCreatedBy()))
                .withLastModifiedById(Utils.getIdOrNull(input.getLastModifiedBy()))
                .build();
    }
}

package com.icupad.function.dtotoentity.patient;

import com.icupad.builder.entitybuilder.patient.AddressBuilder;
import com.icupad.entity.patient.Address;
import com.icupad.webmodel.patient.AddressDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class AddressDtoToEntityFunction implements Function<AddressDto, Address> {
    @Override
    public Address apply(AddressDto input) {
        return AddressBuilder.anAddress()
                .withCity(input.getCity())
                .withHouseNumber(input.getHouseNumber())
                .withPostalCode(input.getPostalCode())
                .withStreet(input.getStreet())
                .withStreetNumber(input.getStreetNumber())
                .build();
    }
}

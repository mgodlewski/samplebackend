package com.icupad.function.entitytodto.patient;

import com.icupad.builder.dtobuilder.patient.PatientDtoBuilder;
import com.icupad.entity.patient.Patient;
import com.icupad.function.utils.Utils;
import com.icupad.webmodel.patient.PatientDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class PatientEntityToDtoFunction implements Function<Patient, PatientDto> {
    @Override
    public PatientDto apply(Patient input) {
        return PatientDtoBuilder.aPatientDto()
                .withId(input.getId())
                .withAddress(Utils.getAddress(input.getAddress()))
                .withBirthDate(Utils.getTimeOrNull(input.getBirthDate()))
                .withHl7Id(input.getHl7Id())
                .withName(input.getName())
                .withPesel(input.getPesel())
                .withSex(input.getSex().toString())
                .withSurname(input.getSurname())

                .withHeight(input.getHeight())
                .withWeight(input.getWeight())
                .withBloodType(input.getBloodType())
                .withAllergies(input.getAllergies())
                .withOtherImportantInformations(input.getOtherImportantInformations())

                .withCreatedById(Utils.getIdOrNull(input.getCreatedBy()))
                .withLastModifiedById(Utils.getIdOrNull(input.getLastModifiedBy()))
                .withCreatedDateTime(Utils.getTimeOrNull(input.getCreatedDate()))
                .withLastModifiedDateTime(Utils.getTimeOrNull(input.getLastModifiedDate()))
                .build();
    }
}

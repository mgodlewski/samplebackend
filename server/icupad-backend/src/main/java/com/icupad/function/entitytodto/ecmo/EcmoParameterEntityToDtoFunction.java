package com.icupad.function.entitytodto.ecmo;

import com.icupad.entity.ecmo.EcmoParameter;
import com.icupad.webmodel.ecmo.EcmoParameterDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

/**
 * Created by Marcin on 04.03.2017.
 */
@Component
public class EcmoParameterEntityToDtoFunction implements Function<EcmoParameter, EcmoParameterDto> {
    @Override
    public EcmoParameterDto apply(EcmoParameter ecmoParameter) {
        return new EcmoParameterDto(ecmoParameter);
    }
}

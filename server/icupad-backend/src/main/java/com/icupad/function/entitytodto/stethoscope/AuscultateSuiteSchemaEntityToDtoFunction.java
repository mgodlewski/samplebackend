package com.icupad.function.entitytodto.stethoscope;

import com.icupad.entity.stethoscope.AuscultateSuiteSchema;
import com.icupad.function.utils.Utils;
import com.icupad.webmodel.stethoscope.AuscultateSuiteSchemaDto;
import com.icupad.builder.dtobuilder.stethoscope.AuscultateSuiteSchemaDtoBuilder;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class AuscultateSuiteSchemaEntityToDtoFunction implements Function<AuscultateSuiteSchema, AuscultateSuiteSchemaDto> {

    @Override
    public AuscultateSuiteSchemaDto apply(AuscultateSuiteSchema input) {
        return AuscultateSuiteSchemaDtoBuilder.anAuscultateSuiteSchemaDto()
                .withId(input.getId())
                .withName(input.getName())
                .withIsPublic(input.getIsPublic())
                .withCreatedById(Utils.getIdOrNull(input.getCreatedBy()))
                .withCreatedDateTime(Utils.getTimeOrNull(input.getCreatedDate()))
                .withLastModifiedById(Utils.getIdOrNull(input.getLastModifiedBy()))
                .withLastModifiedDateTime(Utils.getTimeOrNull(input.getLastModifiedDate()))
                .build();
    }
}

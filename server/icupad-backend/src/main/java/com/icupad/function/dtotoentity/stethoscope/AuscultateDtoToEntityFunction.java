package com.icupad.function.dtotoentity.stethoscope;

import com.icupad.builder.entitybuilder.stethoscope.AuscultateBuilder;
import com.icupad.domain.User;
import com.icupad.entity.stethoscope.Auscultate;
import com.icupad.entity.stethoscope.AuscultatePoint;
import com.icupad.entity.stethoscope.AuscultateSuite;
import com.icupad.repository.UserRepository;
import com.icupad.repository.stethoscope.AuscultatePointRepository;
import com.icupad.repository.stethoscope.AuscultateSuiteRepository;
import com.icupad.utils.DateTimeUtils;
import com.icupad.webmodel.stethoscope.AuscultateDto;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.function.Function;

@Component
public class AuscultateDtoToEntityFunction implements Function<AuscultateDto, Auscultate> {
    @Inject
    private UserRepository userRepository;
    @Inject
    private AuscultatePointRepository auscultatePointRepository;
    @Inject
    private AuscultateSuiteRepository auscultateSuiteRepository;


    @Override
    public Auscultate apply(AuscultateDto input) {
        User createdByUser = userRepository.findOne(input.getCreatedById());
        User lastModifiedByUser = userRepository.findOne(input.getLastModifiedById());

        AuscultatePoint auscultatePoint = auscultatePointRepository.findOne(input.getAuscultatePointId());
        AuscultateSuite auscultateSuite = auscultateSuiteRepository.findOne(input.getAuscultateSuiteId());
        return AuscultateBuilder.anAuscultate()
                .withId(input.getId())
                .withDescription(input.getDescription())
                .withPollResult(input.getPollResult())
                .withWavName(input.getWavName())

                .withAuscultatePoint(auscultatePoint)
                .withAuscultateSuite(auscultateSuite)

                .withCreatedDate(DateTimeUtils.getTimestampOrNullFromLong(input.getCreatedDateTime()))
                .withLastModifiedDate(DateTimeUtils.getTimestampOrNullFromLong(input.getLastModifiedDateTime()))
                .withCreatedBy(createdByUser)
                .withLastModifiedBy(lastModifiedByUser)
                .build();
    }
}

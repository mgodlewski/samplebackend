package com.icupad.function.entitytodto.patient;

import com.icupad.builder.dtobuilder.patient.OperationDtoBuilder;
import com.icupad.entity.patient.Operation;
import com.icupad.function.utils.Utils;
import com.icupad.webmodel.patient.OperationDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class OperationEntityToDtoFunction implements Function<Operation, OperationDto> {
    @Override
    public OperationDto apply(Operation input) {
        return OperationDtoBuilder.anOperationDto()
                .withId(input.getId())
                .withStayId(input.getStay().getId())
                .withOperatingDate(Utils.getTimeOrNull(input.getOperatingDate()))
                .withDescription(input.getDescription())
                .withArchived(input.isArchived())

                .withCreatedDateTime(Utils.getTimeOrNull(input.getCreatedDate()))
                .withLastModifiedDateTime(Utils.getTimeOrNull(input.getLastModifiedDate()))
                .withCreatedById(Utils.getIdOrNull(input.getCreatedBy()))
                .withLastModifiedById(Utils.getIdOrNull(input.getLastModifiedBy()))
                .build();
    }
}

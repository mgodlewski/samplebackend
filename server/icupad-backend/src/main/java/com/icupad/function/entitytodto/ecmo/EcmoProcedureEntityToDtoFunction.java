package com.icupad.function.entitytodto.ecmo;

import com.icupad.entity.ecmo.EcmoProcedure;
import com.icupad.webmodel.ecmo.EcmoProcedureDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

/**
 * Created by Marcin on 04.03.2017.
 */
@Component
public class EcmoProcedureEntityToDtoFunction implements Function<EcmoProcedure, EcmoProcedureDto> {
    @Override
    public EcmoProcedureDto apply(EcmoProcedure aEcmoProcedure) {
        return new EcmoProcedureDto(aEcmoProcedure);
    }
}

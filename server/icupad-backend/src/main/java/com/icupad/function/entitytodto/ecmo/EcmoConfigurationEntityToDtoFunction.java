package com.icupad.function.entitytodto.ecmo;

import com.icupad.entity.ecmo.EcmoConfiguration;
import com.icupad.entity.ecmo.UsedStructures;
import com.icupad.entity.ecmo.VeinCannula;
import com.icupad.repository.ecmo.UsedStucturesRepository;
import com.icupad.repository.ecmo.VeinCannulaRepository;
import com.icupad.webmodel.ecmo.EcmoConfigurationDto;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;
import java.util.function.Function;

/**
 * Created by Marcin on 04.03.2017.
 */
@Component
public class EcmoConfigurationEntityToDtoFunction implements Function<EcmoConfiguration, EcmoConfigurationDto> {

    @Inject UsedStucturesRepository usedStucturesRepository;

    @Inject VeinCannulaRepository veinCannulaRepository;

    @Override
    public EcmoConfigurationDto apply(EcmoConfiguration ecmoConfiguration) {
        List<UsedStructures> ecmoUsedStructuresList = usedStucturesRepository.findByConfigurationIdAndDate(ecmoConfiguration, ecmoConfiguration.getDate());
        List<VeinCannula> veinCannulaList = veinCannulaRepository.findByConfigurationIdAndDate(ecmoConfiguration, ecmoConfiguration.getDate());

        return new EcmoConfigurationDto(ecmoConfiguration, ecmoUsedStructuresList, veinCannulaList);
    }
}

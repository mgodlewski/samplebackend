package com.icupad.function.entitytodto.stethoscope;

import com.icupad.builder.dtobuilder.stethoscope.AuscultateDtoBuilder;
import com.icupad.entity.stethoscope.Auscultate;
import com.icupad.function.utils.Utils;
import com.icupad.webmodel.stethoscope.AuscultateDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class AuscultateEntityToDtoFunction implements Function<Auscultate, AuscultateDto> {
    @Override
    public AuscultateDto apply(Auscultate input) {
        return AuscultateDtoBuilder.anAuscultateDto()
                .withId(input.getId())
                .withAuscultatePointId(input.getAuscultatePoint().getId())
                .withAuscultateSuiteId(input.getAuscultateSuite().getId())
                .withDescription(input.getDescription())
                .withPollResult(input.getPollResult())
                .withWavName(input.getWavName())

                .withCreatedById(Utils.getIdOrNull(input.getCreatedBy()))
                .withCreatedDateTime(Utils.getTimeOrNull(input.getCreatedDate()))
                .withLastModifiedById(Utils.getIdOrNull(input.getLastModifiedBy()))
                .withLastModifiedDateTime(Utils.getTimeOrNull(input.getLastModifiedDate()))
                .build();
    }
}

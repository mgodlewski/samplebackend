package com.icupad.function.dtotoentity.ecmo;

import com.icupad.domain.User;
import com.icupad.entity.ecmo.EcmoAct;
import com.icupad.entity.ecmo.EcmoProcedure;
import com.icupad.repository.UserRepository;
import com.icupad.repository.ecmo.EcmoProcedureRepository;
import com.icupad.webmodel.ecmo.EcmoActDto;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.function.Function;

/**
 * Created by Marcin on 04.03.2017.
 */
@Component
public class EcmoActDtoToEntityFunction implements Function<EcmoActDto, EcmoAct> {

    @Inject private UserRepository userRepository;
    @Inject private EcmoProcedureRepository ecmoProcedureRepository;

    @Override
    public EcmoAct apply(EcmoActDto ecmoActDto) {
        User createdByUser = userRepository.findOne(ecmoActDto.getCreatedBy());
        EcmoProcedure ecmoProcedure = ecmoProcedureRepository.findOne(ecmoActDto.getProcedureId());
        return new EcmoAct(ecmoActDto, ecmoProcedure ,createdByUser);
    }
}

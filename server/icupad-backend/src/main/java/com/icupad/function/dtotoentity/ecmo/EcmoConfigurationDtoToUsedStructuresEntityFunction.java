package com.icupad.function.dtotoentity.ecmo;

import com.icupad.entity.ecmo.EcmoConfiguration;
import com.icupad.entity.ecmo.UsedStructures;
import com.icupad.entity.ecmo.VeinCannula;
import com.icupad.repository.ecmo.EcmoConfigurationRepository;
import com.icupad.utils.DateTimeUtils;
import com.icupad.webmodel.ecmo.EcmoConfigurationDto;
import com.icupad.webmodel.ecmo.VeinCannulaDto;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * Created by Marcin on 04.03.2017.
 */
@Component
public class EcmoConfigurationDtoToUsedStructuresEntityFunction implements Function<EcmoConfigurationDto,  List<UsedStructures>> {

    @Inject private EcmoConfigurationRepository ecmoConfigurationRepository;

    @Override
    public List<UsedStructures> apply(EcmoConfigurationDto ecmoConfigurationDto) {

        EcmoConfiguration ecmoConfiguration = ecmoConfigurationRepository.findOne(ecmoConfigurationDto.getId());

        List<UsedStructures> usedStructures = new ArrayList<>();
        for (String structure : ecmoConfigurationDto.getCannulationStructures()){
            usedStructures.add(new UsedStructures(structure, ecmoConfiguration, DateTimeUtils.getTimestampOrNullFromLong(ecmoConfigurationDto.getDate())));
        }
        return usedStructures;
    }
}

package com.icupad.function.dtotoentity.patient;

import com.icupad.builder.entitybuilder.patient.OperationBuilder;
import com.icupad.domain.User;
import com.icupad.entity.patient.Operation;
import com.icupad.entity.patient.Stay;
import com.icupad.repository.UserRepository;
import com.icupad.repository.patient.StayRepository;
import com.icupad.utils.DateTimeUtils;
import com.icupad.webmodel.patient.OperationDto;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.function.Function;

@Component
public class OperationDtoToEntityFunction implements Function<OperationDto, Operation> {

    @Inject
    private UserRepository userRepository;
    @Inject
    private StayRepository stayRepository;

    @Override
    public Operation apply(OperationDto input) {
        User createdByUser = userRepository.findOne(input.getCreatedById());
        User lastModifiedByUser = userRepository.findOne(input.getLastModifiedById());
        Stay stay = stayRepository.findOne(input.getStayId());

        return OperationBuilder.anOperation()
                .withId(input.getId())
                .withStayId(stay)
                .withDescription(input.getDescription())
                .withArchived(input.isArchived())
                .withOperatingDate(DateTimeUtils.getTimestampOrNullFromLong(input.getOperatingDate()))

                .withCreatedDate(DateTimeUtils.getTimestampOrNullFromLong(input.getCreatedDateTime()))
                .withLastModifiedDate(DateTimeUtils.getTimestampOrNullFromLong(input.getLastModifiedDateTime()))
                .withCreatedBy(createdByUser)
                .withLastModifiedBy(lastModifiedByUser)
                .build();

    }
}

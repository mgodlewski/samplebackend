package com.icupad.function.dtotoentity.stethoscope;

import com.icupad.builder.entitybuilder.stethoscope.AuscultateSuiteBuilder;
import com.icupad.builder.entitybuilder.TestResultExecutorBuilder;
import com.icupad.domain.User;
import com.icupad.entity.patient.Patient;
import com.icupad.entity.stethoscope.AuscultateSuite;
import com.icupad.entity.stethoscope.AuscultateSuiteSchema;
import com.icupad.repository.UserRepository;
import com.icupad.repository.patient.PatientRepository;
import com.icupad.repository.stethoscope.AuscultateSuiteSchemaRepository;
import com.icupad.utils.DateTimeUtils;
import com.icupad.webmodel.stethoscope.AuscultateSuiteDto;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.function.Function;

@Component
public class AuscultateSuiteDtoToEntityFunction implements Function<AuscultateSuiteDto, AuscultateSuite> {
    @Inject
    private UserRepository userRepository;
    @Inject
    private PatientRepository patientRepository;
    @Inject
    private AuscultateSuiteSchemaRepository auscultateSuiteSchemaRepository;


    @Override
    public AuscultateSuite apply(AuscultateSuiteDto input) {
        User createdByUser = userRepository.findOne(input.getCreatedById());
        User lastModifiedByUser = userRepository.findOne(input.getLastModifiedById());
        Patient patient = patientRepository.findOne(input.getPatientId());
        AuscultateSuiteSchema auscultateSuiteSchema = auscultateSuiteSchemaRepository.findOne(input.getAuscultateSuiteSchemaId());

        return AuscultateSuiteBuilder.anAuscultateSuite()
                .withId(input.getId())
                .withDescription(input.getDescription())
                .withPatient(patient)
                .withPosition(input.getPosition())
                .withTemperature(input.getTemperature())
                .withIsRespirated(input.getIsRespirated())
                .withPassiveOxygenTherapy(input.getPassiveOxygenTherapy())
                .withExecutor(TestResultExecutorBuilder.aTestResultExecutor()
                    .withExecutorHl7Id(input.getExecutorHl7Id()).withName(input.getExecutorName()).withSurname(input.getExecutorSurname()).build())
                .withExaminationDateTime(DateTimeUtils.getTimestampOrNullFromLong(input.getExaminationDateTime()))
                .withAuscultateSuiteSchema(auscultateSuiteSchema)

                .withCreatedDate(DateTimeUtils.getTimestampOrNullFromLong(input.getCreatedDateTime()))
                .withLastModifiedDate(DateTimeUtils.getTimestampOrNullFromLong(input.getLastModifiedDateTime()))
                .withCreatedBy(createdByUser)
                .withLastModifiedBy(lastModifiedByUser)
                .build();
    }
}

package com.icupad.function.dtotoentity.ecmo;

import com.icupad.domain.User;
import com.icupad.entity.ecmo.EcmoPatient;
import com.icupad.entity.ecmo.EcmoProcedure;
import com.icupad.repository.UserRepository;
import com.icupad.repository.ecmo.EcmoProcedureRepository;
import com.icupad.webmodel.ecmo.EcmoPatientDto;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.function.Function;

/**
 * Created by Marcin on 04.03.2017.
 */
@Component
public class EcmoPatientDtoToEntityFunction implements Function<EcmoPatientDto, EcmoPatient> {

    @Inject private UserRepository userRepository;
    @Inject private EcmoProcedureRepository ecmoProcedureRepository;


    @Override
    public EcmoPatient apply(EcmoPatientDto ecmoPatientDto) {
        User createdByUser = userRepository.findOne(ecmoPatientDto.getCreatedBy());
        EcmoProcedure ecmoProcedure = ecmoProcedureRepository.findOne(ecmoPatientDto.getProcedureId());
        return new EcmoPatient(ecmoPatientDto, ecmoProcedure ,createdByUser);
    }
}

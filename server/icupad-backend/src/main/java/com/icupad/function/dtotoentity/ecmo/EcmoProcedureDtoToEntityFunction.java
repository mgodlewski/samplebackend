package com.icupad.function.dtotoentity.ecmo;

import com.icupad.domain.User;
import com.icupad.entity.ecmo.EcmoProcedure;
import com.icupad.entity.patient.Stay;
import com.icupad.repository.UserRepository;
import com.icupad.repository.patient.StayRepository;
import com.icupad.webmodel.ecmo.EcmoProcedureDto;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.function.Function;

/**
 * Created by Marcin on 04.03.2017.
 */
@Component
public class EcmoProcedureDtoToEntityFunction implements Function<EcmoProcedureDto, EcmoProcedure> {

    @Inject private UserRepository userRepository;
    @Inject private StayRepository stayRepository;

    @Override
    public EcmoProcedure apply(EcmoProcedureDto ecmoProcedureDto) {
        User createdByUser = userRepository.findOne(ecmoProcedureDto.getCreatedBy());
        Stay stay = stayRepository.findOne(ecmoProcedureDto.getStay());
        User endByUser = null;
        if(ecmoProcedureDto.getEndBy() != null) {
            endByUser = userRepository.findOne(ecmoProcedureDto.getEndBy());
        }

        return new EcmoProcedure(ecmoProcedureDto, createdByUser, stay, endByUser);
    }
}

package com.icupad.function.dtotoentity.stethoscope;

import com.icupad.builder.entitybuilder.stethoscope.AuscultatePointBuilder;
import com.icupad.domain.User;
import com.icupad.entity.stethoscope.AuscultatePoint;
import com.icupad.entity.stethoscope.AuscultateSuiteSchema;
import com.icupad.repository.UserRepository;
import com.icupad.repository.stethoscope.AuscultateSuiteSchemaRepository;
import com.icupad.utils.DateTimeUtils;
import com.icupad.webmodel.stethoscope.AuscultatePointDto;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.function.Function;

@Component
public class AuscultatePointDtoToEntityFunction implements Function<AuscultatePointDto, AuscultatePoint> {
    @Inject
    private UserRepository userRepository;
    @Inject
    private AuscultateSuiteSchemaRepository auscultateSuiteSchemaRepository;


    @Override
    public AuscultatePoint apply(AuscultatePointDto input) {
        User createdByUser = userRepository.findOne(input.getCreatedById());
        User lastModifiedByUser = userRepository.findOne(input.getLastModifiedById());
        AuscultateSuiteSchema auscultateSuiteSchema = auscultateSuiteSchemaRepository.findOne(input.getAuscultateSuiteSchemaId());

        return AuscultatePointBuilder.anAuscultatePoint()
                .withId(input.getId())
                .withName(input.getName())
                .withIsFront(input.getFront())
                .withQueuePosition(input.getQueuePosition())
                .withX(input.getX())
                .withY(input.getY())
                .withAuscultateSuiteSchema(auscultateSuiteSchema)

                .withCreatedDate(DateTimeUtils.getTimestampOrNullFromLong(input.getCreatedDateTime()))
                .withLastModifiedDate(DateTimeUtils.getTimestampOrNullFromLong(input.getLastModifiedDateTime()))
                .withCreatedBy(createdByUser)
                .withLastModifiedBy(lastModifiedByUser)
                .build();
    }
}

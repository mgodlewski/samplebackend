package com.icupad.function.entitytodto.stethoscope;

import com.icupad.builder.dtobuilder.stethoscope.AuscultateSuiteDtoBuilder;
import com.icupad.entity.stethoscope.AuscultateSuite;
import com.icupad.function.utils.Utils;
import com.icupad.webmodel.stethoscope.AuscultateSuiteDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class AuscultateSuiteEntityToDtoFunction implements Function<AuscultateSuite, AuscultateSuiteDto> {
    @Override
    public AuscultateSuiteDto apply(AuscultateSuite input) {
        AuscultateSuiteDto auscultateSuiteDto = AuscultateSuiteDtoBuilder.anAuscultateSuiteDto()
                .withPatientId(input.getPatient().getId())
                .withPosition(input.getPosition())
                .withTemperature(input.getTemperature())
                .withIsRespirated(input.getIsRespirated())
                .withPassiveOxygenTherapy(input.getPassiveOxygenTherapy())
                .withAuscultateSuiteSchemaId(input.getAuscultateSuiteSchema().getId())


                .withExaminationDateTime(Utils.getTimeOrNull(input.getExaminationDateTime()))

                .withDescription(input.getDescription())
                .withId(input.getId())
                .withCreatedById(Utils.getIdOrNull(input.getCreatedBy()))
                .withCreatedDateTime(Utils.getTimeOrNull(input.getCreatedDate()))
                .withLastModifiedById(Utils.getIdOrNull(input.getLastModifiedBy()))
                .withLastModifiedDateTime(Utils.getTimeOrNull(input.getLastModifiedDate()))
                .build();
        auscultateSuiteDto = updateAndReturnWithExecutorInfoIfNotNullTo(auscultateSuiteDto, input);
        return auscultateSuiteDto;
    }

    private AuscultateSuiteDto updateAndReturnWithExecutorInfoIfNotNullTo(AuscultateSuiteDto auscultateSuiteDto, AuscultateSuite input) {
        if(input.getExecutor() != null) {
            auscultateSuiteDto.setExecutorHl7Id(input.getExecutor().getExecutorHl7Id());
            auscultateSuiteDto.setExecutorName(input.getExecutor().getName());
            auscultateSuiteDto.setExecutorSurname(input.getExecutor().getSurname());
        }
        return auscultateSuiteDto;
    }
}

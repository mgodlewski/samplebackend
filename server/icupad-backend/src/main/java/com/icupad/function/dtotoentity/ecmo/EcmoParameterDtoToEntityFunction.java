package com.icupad.function.dtotoentity.ecmo;

import com.icupad.domain.User;
import com.icupad.entity.ecmo.EcmoParameter;
import com.icupad.entity.ecmo.EcmoProcedure;
import com.icupad.repository.UserRepository;
import com.icupad.repository.ecmo.EcmoProcedureRepository;
import com.icupad.webmodel.ecmo.EcmoParameterDto;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.function.Function;

/**
 * Created by Marcin on 04.03.2017.
 */
@Component
public class EcmoParameterDtoToEntityFunction implements Function<EcmoParameterDto, EcmoParameter> {

    @Inject private UserRepository userRepository;
    @Inject private EcmoProcedureRepository ecmoProcedureRepository;

    @Override
    public EcmoParameter apply(EcmoParameterDto ecmoParameterDto) {
        User createdByUser = userRepository.findOne(ecmoParameterDto.getCreatedBy());
        EcmoProcedure ecmoProcedure = ecmoProcedureRepository.findOne(ecmoParameterDto.getProcedureId());
        return new EcmoParameter(ecmoParameterDto, ecmoProcedure ,createdByUser);
    }
}

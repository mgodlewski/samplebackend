package com.icupad.function.entitytodto.ecmo;

import com.icupad.entity.ecmo.EcmoAct;
import com.icupad.webmodel.ecmo.EcmoActDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

/**
 * Created by Marcin on 04.03.2017.
 */
@Component
public class EcmoActEntityToDtoFunction implements Function<EcmoAct,EcmoActDto> {
    @Override
    public EcmoActDto apply(EcmoAct ecmoAct) {
        return new EcmoActDto(ecmoAct);
    }
}

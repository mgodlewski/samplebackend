package com.icupad.function.entitytodto.patient;

import com.icupad.builder.dtobuilder.patient.AddressDtoBuilder;
import com.icupad.entity.patient.Address;
import com.icupad.webmodel.patient.AddressDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class AddressEntityToDtoFunction implements Function<Address, AddressDto> {
    @Override
    public AddressDto apply(Address address) {
        return AddressDtoBuilder.anAddressWebModel()
                .withCity(address.getCity())
                .withHouseNumber(address.getHouseNumber())
                .withPostalCode(address.getPostalCode())
                .withStreet(address.getStreet())
                .withStreetNumber(address.getStreetNumber())
                .build();
    }
}

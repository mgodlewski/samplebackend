package com.icupad.function.dtotoentity.patient;

import com.icupad.builder.entitybuilder.patient.PatientBuilder;
import com.icupad.domain.User;
import com.icupad.entity.patient.Patient;
import com.icupad.entity.enums.Sex;
import com.icupad.repository.UserRepository;
import com.icupad.utils.DateTimeUtils;
import com.icupad.webmodel.patient.PatientDto;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.function.Function;

@Component
public class PatientDtoToEntityFunction implements Function<PatientDto, Patient> {
    @Inject
    private UserRepository userRepository;

    @Override
    public Patient apply(PatientDto input) {
        User createdByUser = userRepository.findOne(input.getCreatedById());
        User lastModifiedByUser = userRepository.findOne(input.getLastModifiedById());

        return PatientBuilder.aPatient()
                .withId(input.getId())
                .withAddress(new AddressDtoToEntityFunction().apply(input.getAddress()))
                .withBirthDate(DateTimeUtils.getTimestampOrNullFromLong(input.getBirthDate()))
                .withHl7Id(input.getHl7Id())
                .withName(input.getName())
                .withPesel(input.getPesel())
                .withSex(Sex.valueOf(input.getSex()))
                .withSurname(input.getSurname())

                .withHeight(input.getHeight())
                .withWeight(input.getWeight())
                .withBloodType(input.getBloodType())
                .withAllergies(input.getAllergies())
                .withOtherImportantInformations(input.getOtherImportantInformations())

                .withCreatedDate(DateTimeUtils.getTimestampOrNullFromLong(input.getCreatedDateTime()))
                .withLastModifiedDate(DateTimeUtils.getTimestampOrNullFromLong(input.getLastModifiedDateTime()))
                .withCreatedBy(createdByUser)
                .withLastModifiedBy(lastModifiedByUser)
                .build();
    }
}

package com.icupad.function.dtotoentity.ecmo;

import com.icupad.domain.User;
import com.icupad.entity.ecmo.EcmoConfiguration;
import com.icupad.entity.ecmo.EcmoProcedure;
import com.icupad.repository.UserRepository;
import com.icupad.repository.ecmo.EcmoProcedureRepository;
import com.icupad.webmodel.ecmo.EcmoConfigurationDto;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.function.Function;

/**
 * Created by Marcin on 04.03.2017.
 */
@Component
public class EcmoConfigurationDtoToEntityFunction implements Function<EcmoConfigurationDto, EcmoConfiguration> {

    @Inject private UserRepository userRepository;
    @Inject private EcmoProcedureRepository ecmoProcedureRepository;

    @Override
    public EcmoConfiguration apply(EcmoConfigurationDto ecmoConfigurationDto) {
        User createdByUser = userRepository.findOne(ecmoConfigurationDto.getCreatedBy());
        EcmoProcedure ecmoProcedure = ecmoProcedureRepository.findOne(ecmoConfigurationDto.getProcedureId());
        return new EcmoConfiguration(ecmoConfigurationDto, ecmoProcedure ,createdByUser);
    }
}

package com.icupad.function.entitytodto.ecmo;

import com.icupad.entity.ecmo.EcmoPatient;
import com.icupad.webmodel.ecmo.EcmoPatientDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

/**
 * Created by Marcin on 04.03.2017.
 */
@Component
public class EcmoPatientEntityToDtoFunction implements Function<EcmoPatient, EcmoPatientDto> {
    @Override
    public EcmoPatientDto apply(EcmoPatient ecmoPatient) {
        return new EcmoPatientDto(ecmoPatient);
    }
}

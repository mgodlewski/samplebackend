package com.icupad.function.entitytodto.stethoscope;

import com.icupad.builder.dtobuilder.stethoscope.AuscultatePointDtoBuilder;
import com.icupad.entity.stethoscope.AuscultatePoint;
import com.icupad.function.utils.Utils;
import com.icupad.webmodel.stethoscope.AuscultatePointDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class AuscultatePointEntityToDtoFunction implements Function<AuscultatePoint, AuscultatePointDto> {
    @Override
    public AuscultatePointDto apply(AuscultatePoint input) {
        return AuscultatePointDtoBuilder.anAuscultatePointDto()
                .withId(input.getId())
                .withName(input.getName())
                .withQueuePosition(input.getQueuePosition())
                .withFront(input.getIsFront())
                .withX(input.getX())
                .withY(input.getY())
                .withAuscultateSuiteSchemaId(input.getAuscultateSuiteSchema().getId())
                .withCreatedById(Utils.getIdOrNull(input.getCreatedBy()))
                .withCreatedDateTime(Utils.getTimeOrNull(input.getCreatedDate()))
                .withLastModifiedById(Utils.getIdOrNull(input.getLastModifiedBy()))
                .withLastModifiedDateTime(Utils.getTimeOrNull(input.getLastModifiedDate()))
                .build();
    }
}

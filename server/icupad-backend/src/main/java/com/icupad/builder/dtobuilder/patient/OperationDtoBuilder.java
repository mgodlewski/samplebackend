package com.icupad.builder.dtobuilder.patient;

import com.icupad.webmodel.patient.OperationDto;

public final class OperationDtoBuilder {
    private Long id;
    private Long stayId;
    private Long operatingDate;
    private String description;
    private boolean archived;

    private Long createdById;
    private Long lastModifiedById;
    private Long createdDateTime;
    private Long lastModifiedDateTime;

    private OperationDtoBuilder() {
    }

    public static OperationDtoBuilder anOperationDto() {
        return new OperationDtoBuilder();
    }

    public OperationDtoBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public OperationDtoBuilder withStayId(Long stayId) {
        this.stayId = stayId;
        return this;
    }

    public OperationDtoBuilder withOperatingDate(Long operatingDate) {
        this.operatingDate = operatingDate;
        return this;
    }

    public OperationDtoBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public OperationDtoBuilder withArchived(boolean archived) {
        this.archived = archived;
        return this;
    }

    public OperationDtoBuilder withCreatedById(Long createdById) {
        this.createdById = createdById;
        return this;
    }

    public OperationDtoBuilder withLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
        return this;
    }

    public OperationDtoBuilder withCreatedDateTime(Long createdDateTime) {
        this.createdDateTime = createdDateTime;
        return this;
    }

    public OperationDtoBuilder withLastModifiedDateTime(Long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
        return this;
    }

    public OperationDto build() {
        OperationDto operationDto = new OperationDto();
        operationDto.setId(id);
        operationDto.setStayId(stayId);
        operationDto.setOperatingDate(operatingDate);
        operationDto.setDescription(description);
        operationDto.setArchived(archived);
        operationDto.setCreatedById(createdById);
        operationDto.setLastModifiedById(lastModifiedById);
        operationDto.setCreatedDateTime(createdDateTime);
        operationDto.setLastModifiedDateTime(lastModifiedDateTime);
        return operationDto;
    }
}

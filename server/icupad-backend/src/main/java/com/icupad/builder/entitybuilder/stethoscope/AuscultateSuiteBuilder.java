package com.icupad.builder.entitybuilder.stethoscope;

import com.icupad.domain.User;
import com.icupad.entity.patient.Patient;
import com.icupad.entity.stethoscope.Auscultate;
import com.icupad.entity.stethoscope.AuscultateSuite;
import com.icupad.entity.stethoscope.AuscultateSuiteSchema;
import com.icupad.entity.submodel.TestResultExecutor;

import java.sql.Timestamp;
import java.util.Set;

public final class AuscultateSuiteBuilder {
    private Patient patient;
    private String description;
    private TestResultExecutor executor;
    private AuscultateSuiteSchema auscultateSuiteSchema;
    private Set<Auscultate> auscultates;
    private Long position;
    private Integer temperature;
    private Boolean isRespirated;
    private Boolean passiveOxygenTherapy;
    private Timestamp examinationDateTime;
    private Long id;
    private User createdBy;
    private Timestamp createdDate;
    private User lastModifiedBy;
    private Timestamp lastModifiedDate;

    private AuscultateSuiteBuilder() {
    }

    public static AuscultateSuiteBuilder anAuscultateSuite() {
        return new AuscultateSuiteBuilder();
    }

    public AuscultateSuiteBuilder withPatient(Patient patient) {
        this.patient = patient;
        return this;
    }

    public AuscultateSuiteBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public AuscultateSuiteBuilder withExecutor(TestResultExecutor executor) {
        this.executor = executor;
        return this;
    }

    public AuscultateSuiteBuilder withAuscultateSuiteSchema(AuscultateSuiteSchema auscultateSuiteSchema) {
        this.auscultateSuiteSchema = auscultateSuiteSchema;
        return this;
    }

    public AuscultateSuiteBuilder withAuscultates(Set<Auscultate> auscultates) {
        this.auscultates = auscultates;
        return this;
    }

    public AuscultateSuiteBuilder withPosition(Long position) {
        this.position = position;
        return this;
    }

    public AuscultateSuiteBuilder withTemperature(Integer temperature) {
        this.temperature = temperature;
        return this;
    }

    public AuscultateSuiteBuilder withIsRespirated(Boolean isRespirated) {
        this.isRespirated = isRespirated;
        return this;
    }

    public AuscultateSuiteBuilder withPassiveOxygenTherapy(Boolean passiveOxygenTherapy) {
        this.passiveOxygenTherapy = passiveOxygenTherapy;
        return this;
    }

    public AuscultateSuiteBuilder withExaminationDateTime(Timestamp examinationDateTime) {
        this.examinationDateTime = examinationDateTime;
        return this;
    }

    public AuscultateSuiteBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public AuscultateSuiteBuilder withCreatedBy(User createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public AuscultateSuiteBuilder withCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public AuscultateSuiteBuilder withLastModifiedBy(User lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public AuscultateSuiteBuilder withLastModifiedDate(Timestamp lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public AuscultateSuite build() {
        AuscultateSuite auscultateSuite = new AuscultateSuite();
        auscultateSuite.setPatient(patient);
        auscultateSuite.setDescription(description);
        auscultateSuite.setExecutor(executor);
        auscultateSuite.setAuscultateSuiteSchema(auscultateSuiteSchema);
        auscultateSuite.setAuscultates(auscultates);
        auscultateSuite.setPosition(position);
        auscultateSuite.setTemperature(temperature);
        auscultateSuite.setIsRespirated(isRespirated);
        auscultateSuite.setPassiveOxygenTherapy(passiveOxygenTherapy);
        auscultateSuite.setExaminationDateTime(examinationDateTime);
        auscultateSuite.setId(id);
        auscultateSuite.setCreatedBy(createdBy);
        auscultateSuite.setCreatedDate(createdDate);
        auscultateSuite.setLastModifiedBy(lastModifiedBy);
        auscultateSuite.setLastModifiedDate(lastModifiedDate);
        return auscultateSuite;
    }
}

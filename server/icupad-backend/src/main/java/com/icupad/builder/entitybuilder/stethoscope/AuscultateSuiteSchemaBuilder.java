package com.icupad.builder.entitybuilder.stethoscope;

import com.icupad.domain.User;
import com.icupad.entity.stethoscope.AuscultatePoint;
import com.icupad.entity.stethoscope.AuscultateSuiteSchema;

import java.sql.Timestamp;
import java.util.Set;

public final class AuscultateSuiteSchemaBuilder {
    private String name;
    private Boolean isPublic;
    private Set<AuscultatePoint> auscultatePoints;
    private Long id;
    private User createdBy;
    private Timestamp createdDate;
    private User lastModifiedBy;
    private Timestamp lastModifiedDate;

    private AuscultateSuiteSchemaBuilder() {
    }

    public static AuscultateSuiteSchemaBuilder anAuscultateSuiteSchema() {
        return new AuscultateSuiteSchemaBuilder();
    }

    public AuscultateSuiteSchemaBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public AuscultateSuiteSchemaBuilder withIsPublic(Boolean isPublic) {
        this.isPublic = isPublic;
        return this;
    }

    public AuscultateSuiteSchemaBuilder withAuscultatePoints(Set<AuscultatePoint> auscultatePoints) {
        this.auscultatePoints = auscultatePoints;
        return this;
    }

    public AuscultateSuiteSchemaBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public AuscultateSuiteSchemaBuilder withCreatedBy(User createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public AuscultateSuiteSchemaBuilder withCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public AuscultateSuiteSchemaBuilder withLastModifiedBy(User lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public AuscultateSuiteSchemaBuilder withLastModifiedDate(Timestamp lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public AuscultateSuiteSchema build() {
        AuscultateSuiteSchema auscultateSuiteSchema = new AuscultateSuiteSchema();
        auscultateSuiteSchema.setName(name);
        auscultateSuiteSchema.setIsPublic(isPublic);
        auscultateSuiteSchema.setAuscultatePoints(auscultatePoints);
        auscultateSuiteSchema.setId(id);
        auscultateSuiteSchema.setCreatedBy(createdBy);
        auscultateSuiteSchema.setCreatedDate(createdDate);
        auscultateSuiteSchema.setLastModifiedBy(lastModifiedBy);
        auscultateSuiteSchema.setLastModifiedDate(lastModifiedDate);
        return auscultateSuiteSchema;
    }
}

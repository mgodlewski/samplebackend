package com.icupad.builder.dtobuilder.stethoscope;

import com.icupad.webmodel.stethoscope.AuscultateDto;

public final class AuscultateDtoBuilder {
    private Long id;
    private String description;
    private String wavName;
    private Long auscultatePointId;
    private Long auscultateSuiteId;
    private Long pollResult;
    private Long createdById;
    private Long createdDateTime;
    private Long lastModifiedById;
    private Long lastModifiedDateTime;

    private AuscultateDtoBuilder() {
    }

    public static AuscultateDtoBuilder anAuscultateDto() {
        return new AuscultateDtoBuilder();
    }

    public AuscultateDtoBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public AuscultateDtoBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public AuscultateDtoBuilder withWavName(String wavName) {
        this.wavName = wavName;
        return this;
    }

    public AuscultateDtoBuilder withAuscultatePointId(Long auscultatePointId) {
        this.auscultatePointId = auscultatePointId;
        return this;
    }

    public AuscultateDtoBuilder withAuscultateSuiteId(Long auscultateSuiteId) {
        this.auscultateSuiteId = auscultateSuiteId;
        return this;
    }

    public AuscultateDtoBuilder withPollResult(Long pollResult) {
        this.pollResult = pollResult;
        return this;
    }

    public AuscultateDtoBuilder withCreatedById(Long createdById) {
        this.createdById = createdById;
        return this;
    }

    public AuscultateDtoBuilder withCreatedDateTime(Long createdDateTime) {
        this.createdDateTime = createdDateTime;
        return this;
    }

    public AuscultateDtoBuilder withLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
        return this;
    }

    public AuscultateDtoBuilder withLastModifiedDateTime(Long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
        return this;
    }

    public AuscultateDto build() {
        AuscultateDto auscultateDto = new AuscultateDto();
        auscultateDto.setId(id);
        auscultateDto.setDescription(description);
        auscultateDto.setWavName(wavName);
        auscultateDto.setAuscultatePointId(auscultatePointId);
        auscultateDto.setAuscultateSuiteId(auscultateSuiteId);
        auscultateDto.setPollResult(pollResult);
        auscultateDto.setCreatedById(createdById);
        auscultateDto.setCreatedDateTime(createdDateTime);
        auscultateDto.setLastModifiedById(lastModifiedById);
        auscultateDto.setLastModifiedDateTime(lastModifiedDateTime);
        return auscultateDto;
    }
}

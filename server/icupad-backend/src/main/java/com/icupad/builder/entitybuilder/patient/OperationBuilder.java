package com.icupad.builder.entitybuilder.patient;

import com.icupad.domain.User;
import com.icupad.entity.patient.Operation;
import com.icupad.entity.patient.Stay;

import java.sql.Timestamp;

public final class OperationBuilder {
    private Stay stay;
    private Timestamp operatingDate;
    private String description;
    private boolean archived;

    private Long id;
    private User createdBy;
    private Timestamp createdDate;
    private User lastModifiedBy;
    private Timestamp lastModifiedDate;

    private OperationBuilder() {
    }

    public static OperationBuilder anOperation() {
        return new OperationBuilder();
    }

    public OperationBuilder withStayId(Stay stay) {
        this.stay = stay;
        return this;
    }

    public OperationBuilder withOperatingDate(Timestamp operatingDate) {
        this.operatingDate = operatingDate;
        return this;
    }

    public OperationBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public OperationBuilder withArchived(boolean archived) {
        this.archived = archived;
        return this;
    }

    public OperationBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public OperationBuilder withCreatedBy(User createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public OperationBuilder withCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public OperationBuilder withLastModifiedBy(User lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public OperationBuilder withLastModifiedDate(Timestamp lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public Operation build() {
        Operation operation = new Operation();
        operation.setStay(stay);
        operation.setOperatingDate(operatingDate);
        operation.setDescription(description);
        operation.setArchived(archived);
        operation.setId(id);
        operation.setCreatedBy(createdBy);
        operation.setCreatedDate(createdDate);
        operation.setLastModifiedBy(lastModifiedBy);
        operation.setLastModifiedDate(lastModifiedDate);
        return operation;
    }
}

package com.icupad.builder.entitybuilder;

import com.icupad.entity.submodel.TestResultExecutor;

public final class TestResultExecutorBuilder {
    private String executorHl7Id;
    private String name;
    private String surname;

    private TestResultExecutorBuilder() {
    }

    public static TestResultExecutorBuilder aTestResultExecutor() {
        return new TestResultExecutorBuilder();
    }

    public TestResultExecutorBuilder withExecutorHl7Id(String executorHl7Id) {
        this.executorHl7Id = executorHl7Id;
        return this;
    }

    public TestResultExecutorBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public TestResultExecutorBuilder withSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public TestResultExecutor build() {
        TestResultExecutor testResultExecutor = new TestResultExecutor();
        testResultExecutor.setExecutorHl7Id(executorHl7Id);
        testResultExecutor.setName(name);
        testResultExecutor.setSurname(surname);
        return testResultExecutor;
    }
}

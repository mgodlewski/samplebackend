package com.icupad.builder.entitybuilder.stethoscope;

import com.icupad.domain.User;
import com.icupad.entity.stethoscope.AuscultatePoint;
import com.icupad.entity.stethoscope.AuscultateSuiteSchema;

import java.sql.Timestamp;

public final class AuscultatePointBuilder {
    private String name;
    private int queuePosition;
    private Double x;
    private Double y;
    private boolean isFront;
    private AuscultateSuiteSchema auscultateSuiteSchema;
    private Long id;
    private User createdBy;
    private Timestamp createdDate;
    private User lastModifiedBy;
    private Timestamp lastModifiedDate;

    private AuscultatePointBuilder() {
    }

    public static AuscultatePointBuilder anAuscultatePoint() {
        return new AuscultatePointBuilder();
    }

    public AuscultatePointBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public AuscultatePointBuilder withQueuePosition(int queuePosition) {
        this.queuePosition = queuePosition;
        return this;
    }

    public AuscultatePointBuilder withX(Double x) {
        this.x = x;
        return this;
    }

    public AuscultatePointBuilder withY(Double y) {
        this.y = y;
        return this;
    }

    public AuscultatePointBuilder withIsFront(boolean isFront) {
        this.isFront = isFront;
        return this;
    }

    public AuscultatePointBuilder withAuscultateSuiteSchema(AuscultateSuiteSchema auscultateSuiteSchema) {
        this.auscultateSuiteSchema = auscultateSuiteSchema;
        return this;
    }

    public AuscultatePointBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public AuscultatePointBuilder withCreatedBy(User createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public AuscultatePointBuilder withCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public AuscultatePointBuilder withLastModifiedBy(User lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public AuscultatePointBuilder withLastModifiedDate(Timestamp lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public AuscultatePoint build() {
        AuscultatePoint auscultatePoint = new AuscultatePoint();
        auscultatePoint.setName(name);
        auscultatePoint.setQueuePosition(queuePosition);
        auscultatePoint.setX(x);
        auscultatePoint.setY(y);
        auscultatePoint.setIsFront(isFront);
        auscultatePoint.setAuscultateSuiteSchema(auscultateSuiteSchema);
        auscultatePoint.setId(id);
        auscultatePoint.setCreatedBy(createdBy);
        auscultatePoint.setCreatedDate(createdDate);
        auscultatePoint.setLastModifiedBy(lastModifiedBy);
        auscultatePoint.setLastModifiedDate(lastModifiedDate);
        return auscultatePoint;
    }
}

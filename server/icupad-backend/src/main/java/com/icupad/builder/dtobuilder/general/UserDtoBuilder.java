package com.icupad.builder.dtobuilder.general;

import com.icupad.webmodel.general.UserDto;

import java.util.List;

public final class UserDtoBuilder {
    private Long id;
    private String login;
    private String password;
    private List<String> roles;
    private String name;
    private String surname;
    private String hl7Id;
    private boolean passwordAttached;

    private UserDtoBuilder() {
    }

    public static UserDtoBuilder aLoggedUserInfoDto() {
        return new UserDtoBuilder();
    }

    public UserDtoBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public UserDtoBuilder withLogin(String login) {
        this.login = login;
        return this;
    }

    public UserDtoBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public UserDtoBuilder withRoles(List<String> roles) {
        this.roles = roles;
        return this;
    }

    public UserDtoBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public UserDtoBuilder withSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public UserDtoBuilder withHl7Id(String hl7Id) {
        this.hl7Id = hl7Id;
        return this;
    }
    public UserDtoBuilder withPasswordAttached(boolean passwordAttached) {
        this.passwordAttached = passwordAttached;
        return this;
    }

    public UserDto build() {
        UserDto userDto = new UserDto();
        userDto.setId(id);
        userDto.setLogin(login);
        userDto.setPassword(password);
        userDto.setRoles(roles);
        userDto.setName(name);
        userDto.setSurname(surname);
        userDto.setHl7Id(hl7Id);
        userDto.setPasswordAttached(passwordAttached);
        return userDto;
    }
}

package com.icupad.builder.dtobuilder.stethoscope;

import com.icupad.webmodel.stethoscope.AuscultatePointDto;

public final class AuscultatePointDtoBuilder {
    private Long id;
    private String name;
    private Integer queuePosition;
    private Double x;
    private Double y;
    private Boolean front;
    private Long auscultateSuiteSchemaId;
    private Long createdById;
    private Long createdDateTime;
    private Long lastModifiedById;
    private Long lastModifiedDateTime;

    private AuscultatePointDtoBuilder() {
    }

    public static AuscultatePointDtoBuilder anAuscultatePointDto() {
        return new AuscultatePointDtoBuilder();
    }

    public AuscultatePointDtoBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public AuscultatePointDtoBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public AuscultatePointDtoBuilder withQueuePosition(Integer queuePosition) {
        this.queuePosition = queuePosition;
        return this;
    }

    public AuscultatePointDtoBuilder withX(Double x) {
        this.x = x;
        return this;
    }

    public AuscultatePointDtoBuilder withY(Double y) {
        this.y = y;
        return this;
    }

    public AuscultatePointDtoBuilder withFront(Boolean front) {
        this.front = front;
        return this;
    }

    public AuscultatePointDtoBuilder withAuscultateSuiteSchemaId(Long auscultateSuiteSchemaId) {
        this.auscultateSuiteSchemaId = auscultateSuiteSchemaId;
        return this;
    }

    public AuscultatePointDtoBuilder withCreatedById(Long createdById) {
        this.createdById = createdById;
        return this;
    }

    public AuscultatePointDtoBuilder withCreatedDateTime(Long createdDateTime) {
        this.createdDateTime = createdDateTime;
        return this;
    }

    public AuscultatePointDtoBuilder withLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
        return this;
    }

    public AuscultatePointDtoBuilder withLastModifiedDateTime(Long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
        return this;
    }

    public AuscultatePointDto build() {
        AuscultatePointDto auscultatePointDto = new AuscultatePointDto();
        auscultatePointDto.setId(id);
        auscultatePointDto.setName(name);
        auscultatePointDto.setQueuePosition(queuePosition);
        auscultatePointDto.setX(x);
        auscultatePointDto.setY(y);
        auscultatePointDto.setFront(front);
        auscultatePointDto.setAuscultateSuiteSchemaId(auscultateSuiteSchemaId);
        auscultatePointDto.setCreatedById(createdById);
        auscultatePointDto.setCreatedDateTime(createdDateTime);
        auscultatePointDto.setLastModifiedById(lastModifiedById);
        auscultatePointDto.setLastModifiedDateTime(lastModifiedDateTime);
        return auscultatePointDto;
    }
}

package com.icupad.builder.dtobuilder.patient;

import com.icupad.webmodel.patient.AddressDto;

public final class AddressDtoBuilder {
    private String street;
    private String streetNumber;
    private String houseNumber;
    private String postalCode;
    private String city;

    private AddressDtoBuilder() {
    }

    public static AddressDtoBuilder anAddressWebModel() {
        return new AddressDtoBuilder();
    }

    public AddressDtoBuilder withStreet(String street) {
        this.street = street;
        return this;
    }

    public AddressDtoBuilder withStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
        return this;
    }

    public AddressDtoBuilder withHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
        return this;
    }

    public AddressDtoBuilder withPostalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public AddressDtoBuilder withCity(String city) {
        this.city = city;
        return this;
    }

    public AddressDto build() {
        AddressDto addressDto = new AddressDto();
        addressDto.setStreet(street);
        addressDto.setStreetNumber(streetNumber);
        addressDto.setHouseNumber(houseNumber);
        addressDto.setPostalCode(postalCode);
        addressDto.setCity(city);
        return addressDto;
    }
}

package com.icupad.builder.dtobuilder.stethoscope;

import com.icupad.webmodel.stethoscope.AuscultateSuiteDto;

public final class AuscultateSuiteDtoBuilder {
    private Long id;
    private Long patientId;
    private String description;
    private String executorHl7Id;
    private String executorName;
    private String executorSurname;
    private Long auscultateSuiteSchemaId;
    private Long position;
    private Integer temperature;
    private Boolean isRespirated;
    private Boolean passiveOxygenTherapy;
    private Long examinationDateTime;
    private Long createdById;
    private Long createdDateTime;
    private Long lastModifiedById;
    private Long lastModifiedDateTime;

    private AuscultateSuiteDtoBuilder() {
    }

    public static AuscultateSuiteDtoBuilder anAuscultateSuiteDto() {
        return new AuscultateSuiteDtoBuilder();
    }

    public AuscultateSuiteDtoBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public AuscultateSuiteDtoBuilder withPatientId(Long patientId) {
        this.patientId = patientId;
        return this;
    }

    public AuscultateSuiteDtoBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public AuscultateSuiteDtoBuilder withExecutorHl7Id(String executorHl7Id) {
        this.executorHl7Id = executorHl7Id;
        return this;
    }

    public AuscultateSuiteDtoBuilder withExecutorName(String executorName) {
        this.executorName = executorName;
        return this;
    }

    public AuscultateSuiteDtoBuilder withExecutorSurname(String executorSurname) {
        this.executorSurname = executorSurname;
        return this;
    }

    public AuscultateSuiteDtoBuilder withAuscultateSuiteSchemaId(Long auscultateSuiteSchemaId) {
        this.auscultateSuiteSchemaId = auscultateSuiteSchemaId;
        return this;
    }

    public AuscultateSuiteDtoBuilder withPosition(Long position) {
        this.position = position;
        return this;
    }

    public AuscultateSuiteDtoBuilder withTemperature(Integer temperature) {
        this.temperature = temperature;
        return this;
    }

    public AuscultateSuiteDtoBuilder withIsRespirated(Boolean isRespirated) {
        this.isRespirated = isRespirated;
        return this;
    }

    public AuscultateSuiteDtoBuilder withPassiveOxygenTherapy(Boolean passiveOxygenTherapy) {
        this.passiveOxygenTherapy = passiveOxygenTherapy;
        return this;
    }

    public AuscultateSuiteDtoBuilder withExaminationDateTime(Long examinationDateTime) {
        this.examinationDateTime = examinationDateTime;
        return this;
    }

    public AuscultateSuiteDtoBuilder withCreatedById(Long createdById) {
        this.createdById = createdById;
        return this;
    }

    public AuscultateSuiteDtoBuilder withCreatedDateTime(Long createdDateTime) {
        this.createdDateTime = createdDateTime;
        return this;
    }

    public AuscultateSuiteDtoBuilder withLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
        return this;
    }

    public AuscultateSuiteDtoBuilder withLastModifiedDateTime(Long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
        return this;
    }

    public AuscultateSuiteDto build() {
        AuscultateSuiteDto auscultateSuiteDto = new AuscultateSuiteDto();
        auscultateSuiteDto.setId(id);
        auscultateSuiteDto.setPatientId(patientId);
        auscultateSuiteDto.setDescription(description);
        auscultateSuiteDto.setExecutorHl7Id(executorHl7Id);
        auscultateSuiteDto.setExecutorName(executorName);
        auscultateSuiteDto.setExecutorSurname(executorSurname);
        auscultateSuiteDto.setAuscultateSuiteSchemaId(auscultateSuiteSchemaId);
        auscultateSuiteDto.setPosition(position);
        auscultateSuiteDto.setTemperature(temperature);
        auscultateSuiteDto.setIsRespirated(isRespirated);
        auscultateSuiteDto.setPassiveOxygenTherapy(passiveOxygenTherapy);
        auscultateSuiteDto.setExaminationDateTime(examinationDateTime);
        auscultateSuiteDto.setCreatedById(createdById);
        auscultateSuiteDto.setCreatedDateTime(createdDateTime);
        auscultateSuiteDto.setLastModifiedById(lastModifiedById);
        auscultateSuiteDto.setLastModifiedDateTime(lastModifiedDateTime);
        return auscultateSuiteDto;
    }
}

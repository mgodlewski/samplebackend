package com.icupad.builder.entitybuilder.patient;

import com.icupad.domain.User;
import com.icupad.entity.enums.Sex;
import com.icupad.entity.patient.Address;
import com.icupad.entity.patient.Patient;

import java.sql.Timestamp;

public final class PatientBuilder {
    private String hl7Id;
    private String pesel;
    private String name;
    private String surname;
    private Timestamp birthDate;
    private Sex sex;
    private Address address;
    private String height;
    private String weight;
    private String bloodType;
    private String allergies;
    private String otherImportantInformations;
    private Long id;
    private User createdBy;
    private Timestamp createdDate;
    private User lastModifiedBy;
    private Timestamp lastModifiedDate;

    private PatientBuilder() {
    }

    public static PatientBuilder aPatient() {
        return new PatientBuilder();
    }

    public PatientBuilder withHl7Id(String hl7Id) {
        this.hl7Id = hl7Id;
        return this;
    }

    public PatientBuilder withPesel(String pesel) {
        this.pesel = pesel;
        return this;
    }

    public PatientBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public PatientBuilder withSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public PatientBuilder withBirthDate(Timestamp birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public PatientBuilder withSex(Sex sex) {
        this.sex = sex;
        return this;
    }

    public PatientBuilder withAddress(Address address) {
        this.address = address;
        return this;
    }

    public PatientBuilder withHeight(String height) {
        this.height = height;
        return this;
    }

    public PatientBuilder withWeight(String weight) {
        this.weight = weight;
        return this;
    }

    public PatientBuilder withBloodType(String bloodType) {
        this.bloodType = bloodType;
        return this;
    }

    public PatientBuilder withAllergies(String allergies) {
        this.allergies = allergies;
        return this;
    }

    public PatientBuilder withOtherImportantInformations(String otherImportantInformations) {
        this.otherImportantInformations = otherImportantInformations;
        return this;
    }

    public PatientBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public PatientBuilder withCreatedBy(User createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public PatientBuilder withCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public PatientBuilder withLastModifiedBy(User lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public PatientBuilder withLastModifiedDate(Timestamp lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public Patient build() {
        Patient patient = new Patient();
        patient.setHl7Id(hl7Id);
        patient.setPesel(pesel);
        patient.setName(name);
        patient.setSurname(surname);
        patient.setBirthDate(birthDate);
        patient.setSex(sex);
        patient.setAddress(address);
        patient.setHeight(height);
        patient.setWeight(weight);
        patient.setBloodType(bloodType);
        patient.setAllergies(allergies);
        patient.setOtherImportantInformations(otherImportantInformations);
        patient.setId(id);
        patient.setCreatedBy(createdBy);
        patient.setCreatedDate(createdDate);
        patient.setLastModifiedBy(lastModifiedBy);
        patient.setLastModifiedDate(lastModifiedDate);
        return patient;
    }
}

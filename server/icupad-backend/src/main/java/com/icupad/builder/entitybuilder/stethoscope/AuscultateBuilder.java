package com.icupad.builder.entitybuilder.stethoscope;

import com.icupad.domain.User;
import com.icupad.entity.stethoscope.Auscultate;
import com.icupad.entity.stethoscope.AuscultatePoint;
import com.icupad.entity.stethoscope.AuscultateSuite;

import java.sql.Timestamp;

public final class AuscultateBuilder {
    private String description;
    private String wavName;
    private AuscultatePoint auscultatePoint;
    private AuscultateSuite auscultateSuite;
    private Long pollResult;
    private Long id;
    private User createdBy;
    private Timestamp createdDate;
    private User lastModifiedBy;
    private Timestamp lastModifiedDate;

    private AuscultateBuilder() {
    }

    public static AuscultateBuilder anAuscultate() {
        return new AuscultateBuilder();
    }

    public AuscultateBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public AuscultateBuilder withWavName(String wavName) {
        this.wavName = wavName;
        return this;
    }

    public AuscultateBuilder withAuscultatePoint(AuscultatePoint auscultatePoint) {
        this.auscultatePoint = auscultatePoint;
        return this;
    }

    public AuscultateBuilder withAuscultateSuite(AuscultateSuite auscultateSuite) {
        this.auscultateSuite = auscultateSuite;
        return this;
    }

    public AuscultateBuilder withPollResult(Long pollResult) {
        this.pollResult = pollResult;
        return this;
    }

    public AuscultateBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public AuscultateBuilder withCreatedBy(User createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public AuscultateBuilder withCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public AuscultateBuilder withLastModifiedBy(User lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public AuscultateBuilder withLastModifiedDate(Timestamp lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public Auscultate build() {
        Auscultate auscultate = new Auscultate();
        auscultate.setDescription(description);
        auscultate.setWavName(wavName);
        auscultate.setAuscultatePoint(auscultatePoint);
        auscultate.setAuscultateSuite(auscultateSuite);
        auscultate.setPollResult(pollResult);
        auscultate.setId(id);
        auscultate.setCreatedBy(createdBy);
        auscultate.setCreatedDate(createdDate);
        auscultate.setLastModifiedBy(lastModifiedBy);
        auscultate.setLastModifiedDate(lastModifiedDate);
        return auscultate;
    }
}

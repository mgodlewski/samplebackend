package com.icupad.builder.dtobuilder.stethoscope;

import com.icupad.webmodel.stethoscope.AuscultateSuiteSchemaDto;

public final class AuscultateSuiteSchemaDtoBuilder {
    private Long id;
    private String name;
    private Boolean isPublic;
    private Long createdById;
    private Long createdDateTime;
    private Long lastModifiedById;
    private Long lastModifiedDateTime;

    private AuscultateSuiteSchemaDtoBuilder() {
    }

    public static AuscultateSuiteSchemaDtoBuilder anAuscultateSuiteSchemaDto() {
        return new AuscultateSuiteSchemaDtoBuilder();
    }

    public AuscultateSuiteSchemaDtoBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public AuscultateSuiteSchemaDtoBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public AuscultateSuiteSchemaDtoBuilder withIsPublic(Boolean isPublic) {
        this.isPublic = isPublic;
        return this;
    }

    public AuscultateSuiteSchemaDtoBuilder withCreatedById(Long createdById) {
        this.createdById = createdById;
        return this;
    }

    public AuscultateSuiteSchemaDtoBuilder withCreatedDateTime(Long createdDateTime) {
        this.createdDateTime = createdDateTime;
        return this;
    }

    public AuscultateSuiteSchemaDtoBuilder withLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
        return this;
    }

    public AuscultateSuiteSchemaDtoBuilder withLastModifiedDateTime(Long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
        return this;
    }

    public AuscultateSuiteSchemaDto build() {
        AuscultateSuiteSchemaDto auscultateSuiteSchemaDto = new AuscultateSuiteSchemaDto();
        auscultateSuiteSchemaDto.setId(id);
        auscultateSuiteSchemaDto.setName(name);
        auscultateSuiteSchemaDto.setIsPublic(isPublic);
        auscultateSuiteSchemaDto.setCreatedById(createdById);
        auscultateSuiteSchemaDto.setCreatedDateTime(createdDateTime);
        auscultateSuiteSchemaDto.setLastModifiedById(lastModifiedById);
        auscultateSuiteSchemaDto.setLastModifiedDateTime(lastModifiedDateTime);
        return auscultateSuiteSchemaDto;
    }
}

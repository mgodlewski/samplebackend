package com.icupad.builder.dtobuilder.patient;

import com.icupad.webmodel.patient.StayDto;

public final class StayDtoBuilder {
    private Long id;
    private Boolean active;
    private Long admitDate;
    private String admittingDoctorHl7Id;
    private String admittingDoctorName;
    private String admittingDoctorSurname;
    private String admittingDoctorNpwz;
    private String assignedPatientLocationName;
    private Long dischargeDate;
    private String hl7Id;
    private String stayType;
    private Long patientId;
    private Long createdDateTime;
    private Long lastModifiedDateTime;
    private Long createdById;
    private Long lastModifiedById;

    private StayDtoBuilder() {
    }

    public static StayDtoBuilder aStayWebModel() {
        return new StayDtoBuilder();
    }

    public StayDtoBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public StayDtoBuilder withActive(Boolean active) {
        this.active = active;
        return this;
    }

    public StayDtoBuilder withAdmitDate(Long admitDate) {
        this.admitDate = admitDate;
        return this;
    }

    public StayDtoBuilder withAdmittingDoctorHl7Id(String admittingDoctorHl7Id) {
        this.admittingDoctorHl7Id = admittingDoctorHl7Id;
        return this;
    }

    public StayDtoBuilder withAdmittingDoctorName(String admittingDoctorName) {
        this.admittingDoctorName = admittingDoctorName;
        return this;
    }

    public StayDtoBuilder withAdmittingDoctorSurname(String admittingDoctorSurname) {
        this.admittingDoctorSurname = admittingDoctorSurname;
        return this;
    }

    public StayDtoBuilder withAdmittingDoctorNpwz(String admittingDoctorNpwz) {
        this.admittingDoctorNpwz = admittingDoctorNpwz;
        return this;
    }

    public StayDtoBuilder withAssignedPatientLocationName(String assignedPatientLocationName) {
        this.assignedPatientLocationName = assignedPatientLocationName;
        return this;
    }

    public StayDtoBuilder withDischargeDate(Long dischargeDate) {
        this.dischargeDate = dischargeDate;
        return this;
    }

    public StayDtoBuilder withHl7Id(String hl7Id) {
        this.hl7Id = hl7Id;
        return this;
    }

    public StayDtoBuilder withStayType(String stayType) {
        this.stayType = stayType;
        return this;
    }

    public StayDtoBuilder withPatientId(Long patientId) {
        this.patientId = patientId;
        return this;
    }

    public StayDtoBuilder withCreatedDateTime(Long createdDateTime) {
        this.createdDateTime = createdDateTime;
        return this;
    }

    public StayDtoBuilder withLastModifiedDateTime(Long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
        return this;
    }

    public StayDtoBuilder withCreatedById(Long createdById) {
        this.createdById = createdById;
        return this;
    }

    public StayDtoBuilder withLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
        return this;
    }

    public StayDto build() {
        StayDto stayDto = new StayDto();
        stayDto.setId(id);
        stayDto.setActive(active);
        stayDto.setAdmitDate(admitDate);
        stayDto.setAdmittingDoctorHl7Id(admittingDoctorHl7Id);
        stayDto.setAdmittingDoctorName(admittingDoctorName);
        stayDto.setAdmittingDoctorSurname(admittingDoctorSurname);
        stayDto.setAdmittingDoctorNpwz(admittingDoctorNpwz);
        stayDto.setAssignedPatientLocationName(assignedPatientLocationName);
        stayDto.setDischargeDate(dischargeDate);
        stayDto.setHl7Id(hl7Id);
        stayDto.setStayType(stayType);
        stayDto.setPatientId(patientId);
        stayDto.setCreatedDateTime(createdDateTime);
        stayDto.setLastModifiedDateTime(lastModifiedDateTime);
        stayDto.setCreatedById(createdById);
        stayDto.setLastModifiedById(lastModifiedById);
        return stayDto;
    }
}

package com.icupad.builder.dtobuilder.patient;

import com.icupad.webmodel.patient.AddressDto;
import com.icupad.webmodel.patient.PatientDto;

public final class PatientDtoBuilder {
    private Long id;
    private AddressDto address;
    private Long birthDate;
    private String hl7Id;
    private String name;
    private String pesel;
    private String sex;
    private String surname;
    private String height;
    private String weight;
    private String bloodType;
    private String allergies;
    private String otherImportantInformations;
    private Long createdById;
    private Long lastModifiedById;
    private Long createdDateTime;
    private Long lastModifiedDateTime;

    private PatientDtoBuilder() {
    }

    public static PatientDtoBuilder aPatientDto() {
        return new PatientDtoBuilder();
    }

    public PatientDtoBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public PatientDtoBuilder withAddress(AddressDto address) {
        this.address = address;
        return this;
    }

    public PatientDtoBuilder withBirthDate(Long birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public PatientDtoBuilder withHl7Id(String hl7Id) {
        this.hl7Id = hl7Id;
        return this;
    }

    public PatientDtoBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public PatientDtoBuilder withPesel(String pesel) {
        this.pesel = pesel;
        return this;
    }

    public PatientDtoBuilder withSex(String sex) {
        this.sex = sex;
        return this;
    }

    public PatientDtoBuilder withSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public PatientDtoBuilder withHeight(String height) {
        this.height = height;
        return this;
    }

    public PatientDtoBuilder withWeight(String weight) {
        this.weight = weight;
        return this;
    }

    public PatientDtoBuilder withBloodType(String bloodType) {
        this.bloodType = bloodType;
        return this;
    }

    public PatientDtoBuilder withAllergies(String allergies) {
        this.allergies = allergies;
        return this;
    }

    public PatientDtoBuilder withOtherImportantInformations(String otherImportantInformations) {
        this.otherImportantInformations = otherImportantInformations;
        return this;
    }

    public PatientDtoBuilder withCreatedById(Long createdById) {
        this.createdById = createdById;
        return this;
    }

    public PatientDtoBuilder withLastModifiedById(Long lastModifiedById) {
        this.lastModifiedById = lastModifiedById;
        return this;
    }

    public PatientDtoBuilder withCreatedDateTime(Long createdDateTime) {
        this.createdDateTime = createdDateTime;
        return this;
    }

    public PatientDtoBuilder withLastModifiedDateTime(Long lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
        return this;
    }

    public PatientDto build() {
        PatientDto patientDto = new PatientDto();
        patientDto.setId(id);
        patientDto.setAddress(address);
        patientDto.setBirthDate(birthDate);
        patientDto.setHl7Id(hl7Id);
        patientDto.setName(name);
        patientDto.setPesel(pesel);
        patientDto.setSex(sex);
        patientDto.setSurname(surname);
        patientDto.setHeight(height);
        patientDto.setWeight(weight);
        patientDto.setBloodType(bloodType);
        patientDto.setAllergies(allergies);
        patientDto.setOtherImportantInformations(otherImportantInformations);
        patientDto.setCreatedById(createdById);
        patientDto.setLastModifiedById(lastModifiedById);
        patientDto.setCreatedDateTime(createdDateTime);
        patientDto.setLastModifiedDateTime(lastModifiedDateTime);
        return patientDto;
    }
}

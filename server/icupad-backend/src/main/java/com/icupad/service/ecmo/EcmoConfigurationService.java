package com.icupad.service.ecmo;

import com.icupad.domain.User;
import com.icupad.entity.ecmo.EcmoConfiguration;
import com.icupad.entity.ecmo.UsedStructures;
import com.icupad.entity.ecmo.VeinCannula;
import com.icupad.function.dtotoentity.ecmo.EcmoConfigurationDtoToEntityFunction;
import com.icupad.function.dtotoentity.ecmo.EcmoConfigurationDtoToUsedStructuresEntityFunction;
import com.icupad.function.dtotoentity.ecmo.EcmoConfigurationDtoToVeinCannulaFunction;
import com.icupad.function.entitytodto.ecmo.EcmoConfigurationEntityToDtoFunction;
import com.icupad.repository.UserRepository;
import com.icupad.repository.ecmo.EcmoConfigurationRepository;
import com.icupad.repository.ecmo.UsedStucturesRepository;
import com.icupad.repository.ecmo.VeinCannulaRepository;
import com.icupad.webmodel.ecmo.EcmoConfigurationDto;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Marcin on 05.03.2017.
 */
@Service
public class EcmoConfigurationService {

    @Inject private EcmoConfigurationRepository ecmoConfigurationRepository;
    @Inject private UserRepository userRepository;
    @Inject private VeinCannulaRepository veinCannulaRepository;
    @Inject private UsedStucturesRepository usedStucturesRepository;

    @Inject private EcmoConfigurationDtoToEntityFunction ecmoConfigurationDtoToEntityFunction;
    @Inject private EcmoConfigurationEntityToDtoFunction ecmoConfigurationEntityToDtoFunction;
    @Inject private EcmoConfigurationDtoToVeinCannulaFunction ecmoConfigurationDtoToVeinCannulaFunction;
    @Inject private EcmoConfigurationDtoToUsedStructuresEntityFunction ecmoConfigurationDtoToUsedStructuresEntityFunction;

    public List<EcmoConfigurationDto> getConfigurationAfterAndPatientIdIn(Long time, List<Long> patientIds, Pageable pageable){
        return ecmoConfigurationRepository.findAllAfterAndPatientIdIn(new Timestamp(time), patientIds, pageable).stream()
                .map(ecmoConfigurationEntityToDtoFunction::apply)
                .collect(Collectors.toList());
    }

    @Transactional
    public Optional<EcmoConfigurationDto> createConfiguration(EcmoConfigurationDto ecmoConfigurationDto){
        User user = userRepository.findOne(1L); //TODO add current user
        if(ecmoConfigurationDto.getId() == null) {
            return saveConfiguration(ecmoConfigurationDto, user);
        }else {
            EcmoConfiguration ec = ecmoConfigurationRepository.findOne(ecmoConfigurationDto.getId());
            if(ec == null){
                return saveConfiguration(ecmoConfigurationDto, user);
            }else{
                return Optional.empty();
            }
        }
    }

    private Optional<EcmoConfigurationDto> saveConfiguration(EcmoConfigurationDto ecmoConfigurationDto, User user){
        EcmoConfiguration ecmoConfiguration = ecmoConfigurationDtoToEntityFunction.apply(ecmoConfigurationDto);
        ecmoConfiguration.setCreatedBy(user);
        ecmoConfiguration = ecmoConfigurationRepository.save(ecmoConfiguration);

        ecmoConfigurationDto.setId(ecmoConfiguration.getId());

        ecmoConfigurationDtoToVeinCannulaFunction.apply(ecmoConfigurationDto)
                .forEach(e -> veinCannulaRepository.save(e));

        ecmoConfigurationDtoToUsedStructuresEntityFunction.apply(ecmoConfigurationDto)
                .forEach(e -> usedStucturesRepository.save(e));

        return Optional.ofNullable(ecmoConfigurationEntityToDtoFunction.apply(ecmoConfiguration));
    }

}

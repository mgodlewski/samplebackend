package com.icupad.service.ecmo;

import com.icupad.domain.User;
import com.icupad.entity.ecmo.EcmoAct;
import com.icupad.entity.ecmo.EcmoPatient;
import com.icupad.entity.ecmo.EcmoProcedure;
import com.icupad.function.dtotoentity.ecmo.EcmoActDtoToEntityFunction;
import com.icupad.function.entitytodto.ecmo.EcmoActEntityToDtoFunction;
import com.icupad.repository.UserRepository;
import com.icupad.repository.ecmo.EcmoActRepository;
import com.icupad.webmodel.ecmo.EcmoActDto;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Marcin on 06.03.2017.
 */
@Service
public class EcmoActService {

    @Inject EcmoActRepository ecmoActRepository;
    @Inject UserRepository userRepository;
    @Inject EcmoActEntityToDtoFunction ecmoActEntityToDtoFunction;
    @Inject EcmoActDtoToEntityFunction ecmoActDtoToEntityFunction;

    public List<EcmoActDto> getActAfterAndPatientIdIn(Long time, List<Long> patientIds, Pageable pageable) {
        return ecmoActRepository.findAllAfterAndPatientIdIn(new Timestamp(time), patientIds, pageable).stream()
                .map(ecmoActEntityToDtoFunction::apply)
                .collect(Collectors.toList());
    }

    @Transactional
    public Optional<EcmoActDto> createAct(EcmoActDto ecmoActDto) {
        User user = userRepository.findOne(1L); //TODO add current user
        if(ecmoActDto.getId()==null) {
            EcmoAct ecmoAct = ecmoActDtoToEntityFunction.apply(ecmoActDto);
            ecmoAct.setCreatedBy(user);
            ecmoAct = ecmoActRepository.save(ecmoAct);
            return Optional.ofNullable(ecmoActEntityToDtoFunction.apply(ecmoAct));
//        }else if(!ecmoActDto.getActual()){
//            EcmoAct ecmoAct = ecmoActDtoToEntityFunction.apply(ecmoActDto);
//            ecmoAct = ecmoActRepository.save(ecmoAct);
//            return Optional.ofNullable(ecmoActEntityToDtoFunction.apply(ecmoAct));
        }else{
            return Optional.empty();
        }
    }
}

package com.icupad.service.defaulttest;

import com.icupad.repository.defaulttest.DefaultTestMeasurementRepository;
import com.icupad.webmodel.defaulttest.DefaultTestMeasurementDto;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Marcin on 22.05.2017.
 */
@Service
public class DefaultTestService {

    @Inject
    private DefaultTestMeasurementRepository defaultTestMeasurementRepository;

    public List<DefaultTestMeasurementDto> getMeasurementsAfterAndPatientIdIn(Long time, List<Long> patientIds, Pageable pageable){
        List<DefaultTestMeasurementDto> defaultTestMeasurementDto = defaultTestMeasurementRepository.findAllAfterAndPatientIdIn(new Timestamp(time), patientIds, pageable);
        return defaultTestMeasurementDto;
    }
}

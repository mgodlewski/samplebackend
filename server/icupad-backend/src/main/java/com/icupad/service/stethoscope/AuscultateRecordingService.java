package com.icupad.service.stethoscope;

import com.icupad.synchronization.webmodel.stethoscope.AuscultateRecordingSyncResponseDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Service
public class AuscultateRecordingService {

    @Transactional
    public AuscultateRecordingSyncResponseDto addRecording(MultipartFile multipartFile) {
        return new AuscultateRecordingSyncResponseDto(
                convertMultipartToFileAndSave(multipartFile));
    }

    private boolean convertMultipartToFileAndSave(@RequestParam("file") MultipartFile multipartFile) {
        try {
            File dir1 = new File ("C:\\ICUpad");
            dir1.mkdir();
            File dir = new File ("C:\\ICUpad\\wav");
            dir.mkdir();
            File convFile = new File(dir, multipartFile.getOriginalFilename() + ".wav");
            if( !convFile.exists()) {
                convFile.createNewFile();
                multipartFile.transferTo(convFile);
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public MultipartFile getRecordingByName(String name) {
        return null;
    }
}

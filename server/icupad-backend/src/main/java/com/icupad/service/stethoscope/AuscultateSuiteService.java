package com.icupad.service.stethoscope;

import com.icupad.entity.stethoscope.AuscultateSuite;
import com.icupad.function.entitytodto.stethoscope.AuscultateSuiteEntityToDtoFunction;
import com.icupad.repository.stethoscope.AuscultateSuiteRepository;
import com.icupad.synchronization.synchronizer.stethoscope.AuscultateSuiteSynchronizer;
import com.icupad.synchronization.webmodel.stethoscope.AuscultateSuiteSyncResponseDto;
import com.icupad.webmodel.stethoscope.AuscultateSuiteDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuscultateSuiteService {

    @Inject
    private AuscultateSuiteRepository auscultateSuiteRepository;
    @Inject
    private AuscultateSuiteEntityToDtoFunction auscultateSuiteEntityToDtoFunction;
    @Inject
    private AuscultateSuiteSynchronizer auscultateSuiteSynchronizer;

    public List<AuscultateSuiteDto> getSuitesAfterAndPatientIdIn(Long time, List<Long> patientIds) {
        return auscultateSuiteRepository.findAllAfterAndPatientIdIn(new Timestamp(time), patientIds).stream()
                .map(auscultateSuiteEntityToDtoFunction::apply)
                .collect(Collectors.toList());
    }

    @Transactional
    public AuscultateSuiteSyncResponseDto uploadSuite(AuscultateSuiteDto auscultateSuiteDto) {
        AuscultateSuite auscultateSuite = auscultateSuiteRepository.findOne(auscultateSuiteDto.getId());
        return auscultateSuiteSynchronizer.prepareResponseAndSaveValidIfNoConflict(auscultateSuiteDto, auscultateSuite);
    }
}

package com.icupad.service.bloodgas;

import com.icupad.entity.bloodgas.BloodGasTestDefaultNorms;
import com.icupad.repository.bloodgas.*;
import com.icupad.webmodel.bloodgas.BloodGasTestMeasurementDto;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by Marcin on 09.06.2017.
 */

@Service
public class BloodGasTestService {

    @Inject
    private BloodGasTestMeasurementRepository bloodGasTestMeasurementRepository;
    @Inject
    private BloodGasTestDefaultNormsRepository bloodGasTestDefaultNormsRepository;

    public List<BloodGasTestMeasurementDto> getMeasurementsAfterAndPatientIdIn(Long time, List<Long> patientIds, Pageable pageable) {
        List<BloodGasTestMeasurementDto> bloodGasTestMeasurementDtoList = bloodGasTestMeasurementRepository.findAllAfterAndPatientIdIn(new Timestamp(time), patientIds, pageable);
        List<BloodGasTestDefaultNorms> bloodGasTestDefaultNormsList = (List<BloodGasTestDefaultNorms>) bloodGasTestDefaultNormsRepository.findAll();

        for(BloodGasTestMeasurementDto measurement : bloodGasTestMeasurementDtoList){
            for(BloodGasTestDefaultNorms norm : bloodGasTestDefaultNormsList){
                if(norm.getTestId().equals(measurement.getTestId())){
                    double months = calculateMonths(measurement.getResultDate(), measurement.getBirthDate());
                    if(norm.getBottomMonthsAge() < months){
                        if(norm.getTopMonthsAge() >= months){
                            measurement.setBottomDefaultNorm(norm.getBottomDefaultNorm());
                            measurement.setTopDefaultNorm(norm.getTopDefaultNorm());
                            break;
                        }
                    }
                }
            }
        }
        return bloodGasTestMeasurementDtoList;
    }

    private double calculateMonths(Date d1, Date d2){
        long diff = Math.abs(d1.getTime() - d2.getTime());
        long diffDays = diff / (24 * 60 * 60 * 1000);
        double months = diffDays/30.4375;
        return months;
    }

}

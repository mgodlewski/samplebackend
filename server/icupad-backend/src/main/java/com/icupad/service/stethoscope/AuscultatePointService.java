package com.icupad.service.stethoscope;

import com.icupad.entity.stethoscope.AuscultatePoint;
import com.icupad.function.entitytodto.stethoscope.AuscultatePointEntityToDtoFunction;
import com.icupad.repository.stethoscope.AuscultatePointRepository;
import com.icupad.synchronization.synchronizer.stethoscope.AuscultatePointSynchronizer;
import com.icupad.synchronization.webmodel.stethoscope.AuscultatePointSyncResponseDto;
import com.icupad.webmodel.stethoscope.AuscultatePointDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuscultatePointService {
    @Inject
    private AuscultatePointRepository auscultatePointRepository;
    @Inject
    private AuscultatePointEntityToDtoFunction auscultateEntityToDtoFunction;
    @Inject
    private AuscultatePointSynchronizer auscultatePointSynchronizer;

    public List<AuscultatePointDto> getPointsAfter(Long time) {
        return auscultatePointRepository.findAllAfter(new Timestamp(time)).stream()
                .map(auscultateEntityToDtoFunction::apply)
                .collect(Collectors.toList());
    }

    @Transactional
    public AuscultatePointSyncResponseDto uploadPoint(AuscultatePointDto auscultatePointDto) {
        AuscultatePoint auscultatePoint = auscultatePointRepository.findOne(auscultatePointDto.getId());
        return auscultatePointSynchronizer.prepareResponseAndSaveValidIfNoConflict(auscultatePointDto, auscultatePoint);
    }
}

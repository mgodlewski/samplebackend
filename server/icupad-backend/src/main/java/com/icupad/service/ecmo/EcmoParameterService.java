package com.icupad.service.ecmo;

import com.icupad.domain.User;
import com.icupad.entity.ecmo.EcmoParameter;
import com.icupad.function.dtotoentity.ecmo.EcmoParameterDtoToEntityFunction;
import com.icupad.function.entitytodto.ecmo.EcmoActEntityToDtoFunction;
import com.icupad.function.entitytodto.ecmo.EcmoParameterEntityToDtoFunction;
import com.icupad.repository.UserRepository;
import com.icupad.repository.ecmo.EcmoParameterRepository;
import com.icupad.webmodel.ecmo.EcmoParameterDto;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Marcin on 06.03.2017.
 */
@Service
public class EcmoParameterService {

    @Inject EcmoParameterRepository ecmoParameterRepository;
    @Inject UserRepository userRepository;
    @Inject EcmoParameterEntityToDtoFunction ecmoParameterEntityToDtoFunction;
    @Inject EcmoParameterDtoToEntityFunction ecmoParameterDtoToEntityFunction;


    public List<EcmoParameterDto> getParameterAfterAndPatientIdIn(Long time, List<Long> patientIds, Pageable pageable) {
        return ecmoParameterRepository.findAllAfterAndPatientIdIn(new Timestamp(time), patientIds, pageable).stream()
                .map(ecmoParameterEntityToDtoFunction::apply)
                .collect(Collectors.toList());
    }

    @Transactional
    public Optional<EcmoParameterDto> createParameter(EcmoParameterDto ecmoParameterDto) {
        User user = userRepository.findOne(1L); //TODO add current user
        if(ecmoParameterDto.getId()==null) {
            EcmoParameter ecmoParameter = ecmoParameterDtoToEntityFunction.apply(ecmoParameterDto);
            ecmoParameter.setCreatedBy(user);
            ecmoParameter = ecmoParameterRepository.save(ecmoParameter);
            return Optional.ofNullable(ecmoParameterEntityToDtoFunction.apply(ecmoParameter));
//        }else if(!ecmoParameterDto.getActual()){
//            EcmoParameter ecmoParameter = ecmoParameterDtoToEntityFunction.apply(ecmoParameterDto);
//            ecmoParameter = ecmoParameterRepository.save(ecmoParameter);
//            return Optional.ofNullable(ecmoParameterEntityToDtoFunction.apply(ecmoParameter));
        }else{
            return Optional.empty();
        }
    }
}

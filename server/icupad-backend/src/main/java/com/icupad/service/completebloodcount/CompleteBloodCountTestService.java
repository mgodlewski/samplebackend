package com.icupad.service.completebloodcount;

import com.icupad.repository.completebloodcount.CompleteBloodCountTestMeasurementRepository;
import com.icupad.webmodel.completebloodcount.CompleteBloodCountTestMeasurementDto;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Marcin on 22.05.2017.
 */
@Service
public class CompleteBloodCountTestService {

    @Inject
    private CompleteBloodCountTestMeasurementRepository completeBloodCountTestMeasurementRepository;

    public List<CompleteBloodCountTestMeasurementDto> getMeasurementsAfterAndPatientIdIn(Long time, List<Long> patientIds, Pageable pageable){
        List<CompleteBloodCountTestMeasurementDto> completeBloodCountTestMeasurementDto = completeBloodCountTestMeasurementRepository.findAllAfterAndPatientIdIn(new Timestamp(time), patientIds, pageable);
        return completeBloodCountTestMeasurementDto;
    }
}

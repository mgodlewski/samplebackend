package com.icupad.service.patient;

import com.icupad.function.entitytodto.patient.StayEntityToDtoFunction;
import com.icupad.repository.patient.StayRepository;
import com.icupad.webmodel.patient.StayDto;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StayService {

    @Inject
    private StayRepository stayRepository;

    public List<StayDto> getStaysAfter(Long time) {
        return stayRepository.findAllAfter(new Timestamp(time)).stream()
                .map(new StayEntityToDtoFunction()::apply)
                .collect(Collectors.toList());
    }

    public List<StayDto> getActiveStaysAfter(Long time){
        return stayRepository.findAllActiveAfter(new Timestamp(time)).stream()
                .map(new StayEntityToDtoFunction()::apply)
                .collect(Collectors.toList());
    }
}

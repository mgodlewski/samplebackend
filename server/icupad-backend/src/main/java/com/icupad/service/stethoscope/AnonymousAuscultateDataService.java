package com.icupad.service.stethoscope;

import com.google.common.collect.Lists;
import com.icupad.entity.patient.Patient;
import com.icupad.entity.stethoscope.Auscultate;
import com.icupad.entity.stethoscope.AuscultateSuite;
import com.icupad.function.utils.Utils;
import com.icupad.repository.patient.PatientRepository;
import com.icupad.repository.stethoscope.AuscultateRepository;
import com.icupad.repository.stethoscope.AuscultateSuiteRepository;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.io.*;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class AnonymousAuscultateDataService {
    @Inject
    private PatientRepository patientRepository;
    @Inject
    private AuscultateRepository auscultateRepository;
    @Inject
    private AuscultateSuiteRepository auscultateSuiteRepository;

    @Transactional
    public List<String> getDataAfter(String input) throws ParseException {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTime date = formatter.parseDateTime(input);
        Long after = date.getMillis();

        StringBuilder sb = new StringBuilder();
        sb.append("patientId;months;weight;height;examinationSuiteId;examinationSuiteCreatedDateTime;" +
                "examinationSuiteLastModifiedDateTime;examinationSuiteDescription;position;isRespirated;passiveOxygenTherapy;temperature;" +
                "examinationId;examinationLastModifiedDateTime;examinationQueueId;pointDescription;wavName;" +
                "rzężenieDrobnobańkowe;rzężenieGrubobańkowe;rzęrzenieTrzeszczenia;świstyWdechowe;świstyStridor;" +
                "świstyWydechowe;furczenia;tarciaOpłucnowe;szelestPęcherzykowyPrawidłowy;szelestPęcherzykowyŚciszony;" +
                "szelestPęcherzykowyZaostrzony;szelestOskrzelowyPatologiczny;szelestOskrzelowyPrawidłowy;zmianyUdzielone;błędneNagranie\n");

        List<AuscultateSuite> suites = auscultateSuiteRepository.findAllAfter(new Timestamp(after));
        List<String> wavNames = new ArrayList<>();
        for(AuscultateSuite suite : suites) {
            StringBuilder suiteSb = new StringBuilder();
            Patient patient = suite.getPatient();
            int months = (int)((Utils.getTimeOrNull(suite.getExaminationDateTime()) - Utils.getTimeOrNull(patient.getBirthDate()))/2629800000L);
            suiteSb.append(patient.getId() + ";" + months + ";" + patient.getWeight() + ";" + patient.getHeight() + ";" +
                    suite.getId() + ";" + suite.getCreatedDate() + ";" + suite.getLastModifiedDate() + ";" +
                    suite.getDescription() + ";" + suite.getPosition() + ";" + suite.getIsRespirated() + ";" +
                    suite.getPassiveOxygenTherapy() + ";" + suite.getTemperature());
            String suitePrefix = suiteSb.toString();
            for(Auscultate auscultate : suite.getAuscultates()) {
                StringBuilder auscultateSb = new StringBuilder();
                auscultateSb.append(suitePrefix + ";" + auscultate.getId() + ";" + auscultate.getLastModifiedDate() + ";" +
                        auscultate.getAuscultatePoint().getQueuePosition() + ";" + auscultate.getDescription() + ";" +
                        auscultate.getWavName());
                Long pollResult = auscultate.getPollResult();
                for(int i = 0; i < 15; i++, pollResult/=2) {
                    auscultateSb.append(";" + (pollResult%2 == 1));
                }
                sb.append(auscultateSb.toString() + "\n");
                wavNames.add(auscultate.getWavName());
            }
        }


        try {

            File dir1 = new File ("C:\\ICUpad");
            dir1.mkdir();
            File dir = new File ("C:\\ICUpad\\MyWhizzy");
            dir.mkdir();

            File f = new File("C:\\ICUpad\\MyWhizzy\\" + input + " - " + formatter.print(System.currentTimeMillis()) + ".zip");
            ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(f));

            ZipEntry e = new ZipEntry("data.txt");
            zos.putNextEntry(e);
            byte[] data = sb.toString().getBytes();
            zos.write(data, 0, data.length);
            zos.closeEntry();

            for(String wavName : wavNames) {
                addToZipFile(wavName + ".wav", zos);
            }

            zos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Lists.newArrayList("from " + after);
    }

    public static void addToZipFile(String fileName, ZipOutputStream zos) throws IOException {

        String fullFileName = "C:\\ICUpad\\wav\\" + fileName;
        System.out.println("Writing '" + fullFileName + "' to zip file");

        File file = new File(fullFileName);
        FileInputStream fis = new FileInputStream(file);
        ZipEntry zipEntry = new ZipEntry(fileName);
        zos.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }

        zos.closeEntry();
        fis.close();
    }
}

package com.icupad.service.ecmo;

import com.icupad.domain.User;
import com.icupad.entity.ecmo.EcmoProcedure;
import com.icupad.function.dtotoentity.ecmo.EcmoProcedureDtoToEntityFunction;
import com.icupad.function.entitytodto.ecmo.EcmoProcedureEntityToDtoFunction;
import com.icupad.repository.UserRepository;
import com.icupad.repository.ecmo.EcmoProcedureRepository;
import com.icupad.utils.DateTimeUtils;
import com.icupad.webmodel.ecmo.EcmoProcedureDto;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Marcin on 05.03.2017.
 */
@Service
public class EcmoProcedureService {

    @Inject private EcmoProcedureRepository ecmoProcedureRepository;
    @Inject private EcmoProcedureEntityToDtoFunction ecmoProcedureEntityToDtoFunction;
    @Inject private EcmoProcedureDtoToEntityFunction ecmoProcedureDtoToEntityFunction;
    @Inject private UserRepository userRepository;

    public List<EcmoProcedureDto> getProcedureAfterAndPatientIdIn(java.lang.Long time, List<Long> patientIds, Pageable pageable){
        return ecmoProcedureRepository.findAllAfterAndPatientIdIn(new Timestamp(time), patientIds, pageable).stream()
                .map(ecmoProcedureEntityToDtoFunction::apply)
                .collect(Collectors.toList());
    }

    @Transactional
    public EcmoProcedureDto saveProcedure (EcmoProcedureDto ecmoProcedureDto){
        User user = userRepository.findOne(1L); //TODO add current user
        if(ecmoProcedureDto.getId()!=null) {
            EcmoProcedure aEcmoProcedure = ecmoProcedureRepository.findOne(ecmoProcedureDto.getId());
            if (aEcmoProcedure != null) {
                aEcmoProcedure.setEndCause(ecmoProcedureDto.getEndCause());
                aEcmoProcedure.setEndDate(DateTimeUtils.getTimestampOrNullFromLong(ecmoProcedureDto.getEndDate()));
                return endProcedure(aEcmoProcedure, user);
            } else {
                return createNewProcedure(ecmoProcedureDto, user);
            }
        }  else {
            return createNewProcedure(ecmoProcedureDto, user);
        }
    }

    private EcmoProcedureDto createNewProcedure (EcmoProcedureDto ecmoProcedureDto, User user ){
        EcmoProcedure aEcmoProcedure = ecmoProcedureDtoToEntityFunction.apply(ecmoProcedureDto);
        aEcmoProcedure.setCreatedBy(user);
        aEcmoProcedure = ecmoProcedureRepository.save(aEcmoProcedure);
        return ecmoProcedureEntityToDtoFunction.apply(aEcmoProcedure);
    }

    private EcmoProcedureDto endProcedure (EcmoProcedure aEcmoProcedure, User user ){
        aEcmoProcedure.setEndBy(user);
        aEcmoProcedure = ecmoProcedureRepository.save(aEcmoProcedure);
        return ecmoProcedureEntityToDtoFunction.apply(aEcmoProcedure);
    }
}

package com.icupad.service.general;

import com.icupad.domain.User;
import com.icupad.repository.UserRepository;
import com.icupad.security.domain.UserDetailsUserAware;
import com.icupad.webmodel.general.UserDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserWebService {

    public static final String ADMIN_ROLE = "Admin";

    @Inject
    private UserService userService;
    @Inject
    private UserRepository userRepository;

    public UserDto getLoggedUserInfo() {
        return userService.getLoggedUserInfo();
    }

    public ResponseEntity<List<UserDto>> getUsers() {
        if( !loggedUserHasAdminRole()) {
            return returnUnauthorized();
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userService.getUsers());
    }

    public boolean loggedUserHasAdminRole() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsUserAware) auth.getPrincipal()).getUser();
        return getStringRoles(user).contains(ADMIN_ROLE);
    }

    private List<String> getStringRoles(User user) {
        return user.getRoles().stream().map(role -> role.getName()).collect(Collectors.toList());
    }

    private <T> ResponseEntity<T> returnUnauthorized() {
        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(null);
    }

    public ResponseEntity<UserDto> addUser(UserDto userDto) {
        if( !loggedUserHasAdminRole()) {
            return returnUnauthorized();
        }
        if(userWithLoginExists(userDto)) {
            return returnConflictError();
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userService.addNewUser(userDto));
    }

    private boolean userWithLoginExists(UserDto userDto) {
        return userRepository.findByLogin(userDto.getLogin()) != null;
    }

    private ResponseEntity<UserDto> returnConflictError() {
        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .body(null);
    }

    public ResponseEntity<UserDto> updateUser(UserDto userDto, String login) {
        if( !loggedUserHasAdminRole()) {
            return returnUnauthorized();
        }
        if( !userWithLoginExists(userDto)) {
            return returnNotFoundError();
        }
        if(!login.equals(userDto.getLogin())) {
            return returnBadRequestError(userDto);
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userService.updateUser(userDto));

    }

    private ResponseEntity<UserDto> returnNotFoundError() {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(null);
    }

    private ResponseEntity<UserDto> returnBadRequestError(UserDto userDto) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(userService.updateUser(userDto));
    }
}

package com.icupad.service.ecmo;

import com.icupad.domain.User;
import com.icupad.entity.ecmo.EcmoPatient;
import com.icupad.function.dtotoentity.ecmo.EcmoPatientDtoToEntityFunction;
import com.icupad.function.entitytodto.ecmo.EcmoPatientEntityToDtoFunction;
import com.icupad.repository.UserRepository;
import com.icupad.repository.ecmo.EcmoPatientRepository;
import com.icupad.webmodel.ecmo.EcmoPatientDto;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.swing.text.html.Option;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Marcin on 06.03.2017.
 */
@Service
public class EcmoPatientService {

    @Inject EcmoPatientRepository ecmoPatientRepository;
    @Inject UserRepository userRepository;
    @Inject EcmoPatientEntityToDtoFunction ecmoPatientEntityToDtoFunction;
    @Inject EcmoPatientDtoToEntityFunction ecmoPatientDtoToEntityFunction;

    public List<EcmoPatientDto> getPatientAfterAndPatientIdIn(Long time, List<Long> patientIds, Pageable pageable){
        return ecmoPatientRepository.findAllAfterAndPatientIdIn(new Timestamp(time), patientIds, pageable).stream()
                .map(ecmoPatientEntityToDtoFunction::apply)
                .collect(Collectors.toList());
    }

    @Transactional
    public Optional<EcmoPatientDto> createPatient(EcmoPatientDto ecmoPatientDto){
        User user = userRepository.findOne(1L); //TODO add current user
        if(ecmoPatientDto.getId()==null) {
            EcmoPatient ecmoPatient = ecmoPatientDtoToEntityFunction.apply(ecmoPatientDto);
            ecmoPatient.setCreatedBy(user);
            ecmoPatient = ecmoPatientRepository.save(ecmoPatient);
            return Optional.ofNullable(ecmoPatientEntityToDtoFunction.apply(ecmoPatient));
//        }else if(!ecmoPatientDto.getActual()){
//            EcmoPatient ecmoPatient = ecmoPatientDtoToEntityFunction.apply(ecmoPatientDto);
//            ecmoPatient = ecmoPatientRepository.save(ecmoPatient);
//            return Optional.ofNullable(ecmoPatientEntityToDtoFunction.apply(ecmoPatient));
        }else{
            return Optional.empty();
        }
    }
}

package com.icupad.service.patient;

import com.icupad.entity.patient.Operation;
import com.icupad.function.entitytodto.patient.OperationEntityToDtoFunction;
import com.icupad.repository.patient.OperationRepository;
import com.icupad.synchronization.synchronizer.patient.OperationSynchronizer;
import com.icupad.synchronization.webmodel.patient.OperationSyncResponseDto;
import com.icupad.webmodel.patient.OperationDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OperationService {
    @Inject
    private OperationRepository operationRepository;
    @Inject
    private OperationSynchronizer operationSynchronizer;
    @Inject
    private OperationEntityToDtoFunction operationEntityToDtoFunction;

    public List<OperationDto> getOperationsAfter(Long time, List<Long> patientIds) {
        return operationRepository.findAllAfter(new Timestamp(time), patientIds).stream()
                .map(operationEntityToDtoFunction::apply)
                .collect(Collectors.toList());
    }

    @Transactional
    public OperationSyncResponseDto uploadOperation(OperationDto operationDto) {
        Operation operation = operationRepository.findOne(operationDto.getId());
        return operationSynchronizer.prepareResponseAndSaveValidIfNoConflict(operationDto, operation);
    }
}

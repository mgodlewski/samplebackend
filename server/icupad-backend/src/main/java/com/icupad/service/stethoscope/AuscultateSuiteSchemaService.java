package com.icupad.service.stethoscope;

import com.icupad.entity.stethoscope.AuscultateSuiteSchema;
import com.icupad.function.entitytodto.stethoscope.AuscultateSuiteSchemaEntityToDtoFunction;
import com.icupad.repository.stethoscope.AuscultateSuiteSchemaRepository;
import com.icupad.synchronization.synchronizer.stethoscope.AuscultateSuiteSchemaSynchronizer;
import com.icupad.synchronization.webmodel.stethoscope.AuscultateSuiteSchemaSyncResponseDto;
import com.icupad.webmodel.stethoscope.AuscultateSuiteSchemaDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuscultateSuiteSchemaService {
    @Inject
    private AuscultateSuiteSchemaRepository auscultateSuiteSchemaRepository;
    @Inject
    private AuscultateSuiteSchemaSynchronizer auscultateSuiteSchemaSynchronizer;

    public List<AuscultateSuiteSchemaDto> getSuiteSchemasAfter(Long time) {
        List<AuscultateSuiteSchema> auscultateSuiteSchemas = auscultateSuiteSchemaRepository.findAllAfter(new Timestamp(time));

        List<AuscultateSuiteSchemaDto> output =
                auscultateSuiteSchemas.stream()
                        .map(a -> new AuscultateSuiteSchemaEntityToDtoFunction().apply(a))
                        .collect(Collectors.toList());
        return output;
    }

    @Transactional
    public AuscultateSuiteSchemaSyncResponseDto uploadSuiteSchema(AuscultateSuiteSchemaDto auscultateSuiteSchemaDto) {
        AuscultateSuiteSchema auscultateSuiteSchema = auscultateSuiteSchemaRepository.findOne(auscultateSuiteSchemaDto.getId());
        return auscultateSuiteSchemaSynchronizer.prepareResponseAndSaveValidIfNoConflict(auscultateSuiteSchemaDto, auscultateSuiteSchema);
    }
}

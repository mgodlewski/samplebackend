package com.icupad.service.patient;

import com.icupad.entity.patient.Patient;
import com.icupad.function.entitytodto.patient.PatientEntityToDtoFunction;
import com.icupad.repository.patient.PatientRepository;
import com.icupad.synchronization.synchronizer.patient.PatientSynchronizer;
import com.icupad.synchronization.webmodel.patient.PatientSyncResponseDto;
import com.icupad.webmodel.patient.PatientDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PatientService {
    @Inject
    private PatientRepository patientRepository;
    @Inject
    private PatientSynchronizer patientSynchronizer;
    @Inject
    private PatientEntityToDtoFunction patientEntityToDtoFunction;

    public List<PatientDto> getPatientsAfter(Long time) {
        return patientRepository.findAllAfter(new Timestamp(time)).stream()
                .map(patientEntityToDtoFunction::apply)
                .collect(Collectors.toList());
    }

    @Transactional
    public PatientSyncResponseDto uploadPatient(PatientDto patientDto) {
        Patient patient = patientRepository.findOne(patientDto.getId());
        return patientSynchronizer.prepareResponseAndSaveValidIfNoConflict(patientDto, patient);
    }

    public List<PatientDto> getActivePatientsAfter(Long time){
        return patientRepository.findAllActiveAfter(new Timestamp(time)).stream()
                .map(patientEntityToDtoFunction::apply)
                .collect(Collectors.toList());
    }
}

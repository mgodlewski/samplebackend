package com.icupad.service.stethoscope;

import com.icupad.entity.stethoscope.Auscultate;
import com.icupad.function.entitytodto.stethoscope.AuscultateEntityToDtoFunction;
import com.icupad.repository.stethoscope.AuscultateRepository;
import com.icupad.synchronization.synchronizer.stethoscope.AuscultateSynchronizer;
import com.icupad.synchronization.webmodel.stethoscope.AuscultateSyncResponseDto;
import com.icupad.webmodel.stethoscope.AuscultateDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuscultateService {
    @Inject
    private AuscultateRepository auscultateRepository;
    @Inject
    private AuscultateEntityToDtoFunction auscultateEntityToDtoFunction;
    @Inject
    private AuscultateSynchronizer auscultateSynchronizer;

    public List<AuscultateDto> getAuscultatesAfterAndPatientIdIn(Long time, List<Long> patientIds) {
        return auscultateRepository.findAllAfterAndPatientIdIn(new Timestamp(time), patientIds).stream()
                .map(auscultateEntityToDtoFunction::apply)
                .collect(Collectors.toList());
    }

    @Transactional
    public AuscultateSyncResponseDto uploadAuscultate(AuscultateDto auscultateDto) {
        Auscultate auscultate = auscultateRepository.findOne(auscultateDto.getId());
        return auscultateSynchronizer.prepareResponseAndSaveValidIfNoConflict(auscultateDto, auscultate);
    }
}

package com.icupad.service.general;

import com.icupad.repository.bloodgas.*;
import com.icupad.repository.completebloodcount.CompleteBloodCountTestPanelResultRepository;
import com.icupad.repository.completebloodcount.CompleteBloodCountTestRepository;
import com.icupad.repository.completebloodcount.CompleteBloodCountTestRequestRepository;
import com.icupad.repository.completebloodcount.CompleteBloodCountTestResultRepository;
import com.icupad.repository.defaulttest.DefaultTestPanelResultRepository;
import com.icupad.repository.defaulttest.DefaultTestRepository;
import com.icupad.repository.defaulttest.DefaultTestRequestRepository;
import com.icupad.repository.defaulttest.DefaultTestResultRepository;
import com.icupad.repository.ecmo.*;
import com.icupad.repository.patient.OperationRepository;
import com.icupad.repository.patient.PatientRepository;
import com.icupad.repository.patient.StayRepository;
import com.icupad.repository.stethoscope.AuscultatePointRepository;
import com.icupad.repository.stethoscope.AuscultateRepository;
import com.icupad.repository.stethoscope.AuscultateSuiteRepository;
import com.icupad.repository.stethoscope.AuscultateSuiteSchemaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.sql.Timestamp;

@Service
public class LastTimestampService {
    @Inject
    private PatientRepository patientRepository;

    @Inject
    private BloodGasTestPanelResultRepository bloodGasTestPanelResultRepository;
    @Inject
    private BloodGasTestRepository bloodGasTestRepository;
    @Inject
    private BloodGasTestRequestRepository bloodGasTestRequestRepository;
    @Inject
    private BloodGasTestResultRepository bloodGasTestResultRepository;
    @Inject
    private StayRepository stayRepository;

    @Inject
    private BloodGasTestDefaultNormsRepository bloodGasTestDefaultNormsRepository;
    @Inject
    private OperationRepository operationRepository;
    @Inject
    private AuscultatePointRepository auscultatePointRepository;
    @Inject
    private AuscultateRepository auscultateRepository;
    @Inject
    private AuscultateSuiteRepository auscultateSuiteRepository;
    @Inject
    private AuscultateSuiteSchemaRepository auscultateSuiteSchemaRepository;

    @Inject private EcmoActRepository ecmoActRepository;
    @Inject private EcmoConfigurationRepository ecmoConfigurationRepository;
    @Inject private EcmoParameterRepository ecmoParameterRepository;
    @Inject private EcmoPatientRepository ecmoPatientRepository;
    @Inject private EcmoProcedureRepository ecmoProcedureRepository;
    @Inject private VeinCannulaRepository veinCannulaRepository;
    @Inject private UsedStucturesRepository usedStucturesRepository;

    @Inject private DefaultTestRepository defaultTestRepository;
    @Inject private DefaultTestPanelResultRepository defaultTestPanelResultRepository;
    @Inject private DefaultTestRequestRepository defaultTestRequestRepository;
    @Inject private DefaultTestResultRepository defaultTestResultRepository;

    @Inject private CompleteBloodCountTestRepository completeBloodCountTestRepository;
    @Inject private CompleteBloodCountTestPanelResultRepository completeBloodCountTestPanelResultRepository;
    @Inject private CompleteBloodCountTestResultRepository completeBloodCountTestResultRepository;
    @Inject private CompleteBloodCountTestRequestRepository completeBloodCountTestRequestRepository;

    private Long eskulapMaxTimestamp;
    private Long icupadMaxTimestamp;

    @Transactional
    public Long getForEskulap() {
        eskulapMaxTimestamp = 0L;

        updateEskulapMaxWith(patientRepository.getMaxLastModifiedDateWithLastModifiedByIcupadEqual(false));
        updateEskulapMaxWith(patientRepository.getMaxCreatedDateWithLastModifiedByIcupadEqual(false));

        updateEskulapMaxWith(bloodGasTestPanelResultRepository.getMaxLastModifiedDate());
        updateEskulapMaxWith(bloodGasTestPanelResultRepository.getMaxCreatedDate());

        updateEskulapMaxWith(bloodGasTestRepository.getMaxLastModifiedDate());
        updateEskulapMaxWith(bloodGasTestRepository.getMaxCreatedDate());

        updateEskulapMaxWith(bloodGasTestRequestRepository.getMaxLastModifiedDate());
        updateEskulapMaxWith(bloodGasTestRequestRepository.getMaxCreatedDate());

        updateEskulapMaxWith(bloodGasTestResultRepository.getMaxLastModifiedDate());
        updateEskulapMaxWith(bloodGasTestResultRepository.getMaxCreatedDate());

        updateEskulapMaxWith(stayRepository.getMaxLastModifiedDate());
        updateEskulapMaxWith(stayRepository.getMaxCreatedDate());

        updateEskulapMaxWith(defaultTestRepository.getMaxLastModifiedDate());
        updateEskulapMaxWith(defaultTestRepository.getMaxCreatedDate());
        updateEskulapMaxWith(defaultTestPanelResultRepository.getMaxLastModifiedDate());
        updateEskulapMaxWith(defaultTestPanelResultRepository.getMaxCreatedDate());
        updateEskulapMaxWith(defaultTestRequestRepository.getMaxLastModifiedDate());
        updateEskulapMaxWith(defaultTestRequestRepository.getMaxCreatedDate());
        updateEskulapMaxWith(defaultTestResultRepository.getMaxLastModifiedDate());
        updateEskulapMaxWith(defaultTestResultRepository.getMaxCreatedDate());

        updateEskulapMaxWith(completeBloodCountTestRepository.getMaxLastModifiedDate());
        updateEskulapMaxWith(completeBloodCountTestRepository.getMaxCreatedDate());
        updateEskulapMaxWith(completeBloodCountTestPanelResultRepository.getMaxLastModifiedDate());
        updateEskulapMaxWith(completeBloodCountTestPanelResultRepository.getMaxCreatedDate());
        updateEskulapMaxWith(completeBloodCountTestResultRepository.getMaxLastModifiedDate());
        updateEskulapMaxWith(completeBloodCountTestResultRepository.getMaxCreatedDate());
        updateEskulapMaxWith(completeBloodCountTestRequestRepository.getMaxLastModifiedDate());
        updateEskulapMaxWith(completeBloodCountTestRequestRepository.getMaxCreatedDate());

        return eskulapMaxTimestamp;
    }

    private void updateEskulapMaxWith(Timestamp timestamp) {
        if(timestamp != null) {
            eskulapMaxTimestamp = Math.max(eskulapMaxTimestamp, timestamp.getTime());
        }
    }

    @Transactional
    public Long getForIcupad() {
        icupadMaxTimestamp = 0L;

        updateIcupadMaxWith(patientRepository.getMaxLastModifiedDateWithLastModifiedByIcupadEqual(true));
        updateIcupadMaxWith(patientRepository.getMaxCreatedDateWithLastModifiedByIcupadEqual(true));

        updateIcupadMaxWith(bloodGasTestDefaultNormsRepository.getMaxLastModifiedDate());
        updateIcupadMaxWith(bloodGasTestDefaultNormsRepository.getMaxCreatedDate());

        updateIcupadMaxWith(operationRepository.getMaxLastModifiedDate());
        updateIcupadMaxWith(operationRepository.getMaxCreatedDate());

        updateIcupadMaxWith(auscultatePointRepository.getMaxLastModifiedDate());
        updateIcupadMaxWith(auscultatePointRepository.getMaxCreatedDate());

        updateIcupadMaxWith(auscultateRepository.getMaxLastModifiedDate());
        updateIcupadMaxWith(auscultateRepository.getMaxCreatedDate());

        updateIcupadMaxWith(auscultateSuiteRepository.getMaxLastModifiedDate());
        updateIcupadMaxWith(auscultateSuiteRepository.getMaxCreatedDate());

        updateIcupadMaxWith(auscultateSuiteSchemaRepository.getMaxLastModifiedDate());
        updateIcupadMaxWith(auscultateSuiteSchemaRepository.getMaxCreatedDate());

        updateIcupadMaxWith(ecmoActRepository.getMaxCreatedDate());
        updateIcupadMaxWith(ecmoConfigurationRepository.getMaxCreatedDate());
        updateIcupadMaxWith(ecmoParameterRepository.getMaxCreatedDate());
        updateIcupadMaxWith(ecmoPatientRepository.getMaxCreatedDate());
        updateIcupadMaxWith(ecmoProcedureRepository.getMaxCreatedDate());
        updateIcupadMaxWith(ecmoProcedureRepository.getMaxEndDate());
        updateIcupadMaxWith(veinCannulaRepository.getMaxCreatedDate());
        updateIcupadMaxWith(usedStucturesRepository.getMaxCreatedDate());

        return icupadMaxTimestamp;
    }

    private void updateIcupadMaxWith(Timestamp timestamp) {
        if(timestamp != null) {
            icupadMaxTimestamp = Math.max(icupadMaxTimestamp, timestamp.getTime());
        }
    }
}

package com.icupad.service.general;

import com.icupad.builder.dtobuilder.general.UserDtoBuilder;
import com.icupad.domain.Role;
import com.icupad.domain.User;
import com.icupad.repository.UserRepository;
import com.icupad.security.domain.UserDetailsUserAware;
import com.icupad.service.RoleService;
import com.icupad.webmodel.general.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private RoleService roleService;
    @Inject
    private PasswordEncoder encoder;
    @Inject
    private UserRepository userRepository;

    public UserDto getLoggedUserInfo() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsUserAware) auth.getPrincipal()).getUser();
        return UserDtoBuilder.aLoggedUserInfoDto()
                .withId(user.getId())
                .withLogin(user.getLogin())
                .withRoles(getStringRoles(user))
                .withPasswordAttached(false)
                .withName(user.getName())
                .withSurname(user.getSurname())
                .build();
    }

    private List<String> getStringRoles(User user) {
        return user.getRoles().stream().map(role -> role.getName()).collect(Collectors.toList());
    }

    public UserDto addNewUser(UserDto userDto){
        User user = new User();
        user.setLogin(userDto.getLogin());
        user.setPassword(encoder.encode(userDto.getPassword()));
        user.setEnabled(true);
        user.setName(userDto.getName());
        user.setSurname(userDto.getSurname());
        user.setHl7Id(userDto.getHl7Id());
        addRolesToUserByRolesNamesOrThrowExceptionIfDoesntExist(userDto, user);
        user = userRepository.save(user);
        return userToUserInfoDto(user);
    }

    private UserDto userToUserInfoDto(User user) {
        return UserDtoBuilder.aLoggedUserInfoDto()
                .withId(user.getId())
                .withLogin(user.getLogin())
                .withRoles(getStringRoles(user))
                .withName(user.getName())
                .withSurname(user.getSurname())
                .withHl7Id(user.getHl7Id())
                .withPasswordAttached(false)
                .build();
    }

    public List<UserDto> getUsers(){
        return userRepository.findAll().stream().map(user -> userToUserInfoDto(user)).collect(Collectors.toList());
    }

    public UserDto updateUser(UserDto userDto){
        User user = userRepository.findByLogin(userDto.getLogin());
        addRolesToUserByRolesNamesOrThrowExceptionIfDoesntExist(userDto, user);
        if(userDto.isPasswordAttached()) {
            user.setPassword(encoder.encode(userDto.getPassword()));
        }
        user.setName(userDto.getName());
        user.setSurname(userDto.getSurname());
        user.setHl7Id(userDto.getHl7Id());
        user = userRepository.save(user);
        return userToUserInfoDto(user);
    }

    private void addRolesToUserByRolesNamesOrThrowExceptionIfDoesntExist(UserDto userDto, User user) {
        for(String roleName : userDto.getRoles()) {
            Role role = roleService.findByName(roleName);
            if (role == null) {
                throw new RuntimeException("Missing role " + roleName);
            }
            user.getRoles().add(role);
        }
    }
}

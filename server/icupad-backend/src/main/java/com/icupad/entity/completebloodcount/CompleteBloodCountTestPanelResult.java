package com.icupad.entity.completebloodcount;

import com.icupad.entity.BaseEntity;
import com.icupad.entity.bloodgas.BloodGasTestRequest;
import com.icupad.entity.enums.BloodSource;
import com.icupad.entity.submodel.TestResultExecutor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 22.05.2017.
 */
@Entity
@Table(name = "complete_blood_count_test_panel_result")
public class CompleteBloodCountTestPanelResult extends BaseEntity {


    @Column(name = "request_date", nullable = false)
    @NotNull
    private Timestamp requestDate;

    @Embedded
    private TestResultExecutor testResultExecutor;

    @Column(name = "blood_source")
    @Enumerated(value = EnumType.STRING)
    @NotNull
    private BloodSource bloodSource;

    @OneToMany(mappedBy = "completeBloodCountTestPanelResult")
    private List<CompleteBloodCountTestRequest> completeBloodCountTestRequests = new ArrayList<>();




    public Timestamp getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Timestamp requestDate) {
        this.requestDate = requestDate;
    }

    public TestResultExecutor getTestResultExecutor() {
        return testResultExecutor;
    }

    public void setTestResultExecutor(TestResultExecutor testResultExecutor) {
        this.testResultExecutor = testResultExecutor;
    }

    public BloodSource getBloodSource() {
        return bloodSource;
    }

    public void setBloodSource(BloodSource bloodSource) {
        this.bloodSource = bloodSource;
    }

    public List<CompleteBloodCountTestRequest> getCompleteBloodCountTestRequests() {
        return completeBloodCountTestRequests;
    }

    public void setCompleteBloodCountTestRequests(List<CompleteBloodCountTestRequest> completeBloodCountTestRequests) {
        this.completeBloodCountTestRequests = completeBloodCountTestRequests;
    }
}

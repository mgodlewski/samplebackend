package com.icupad.entity.patient;

import com.icupad.domain.BaseEntity;
import com.icupad.entity.enums.Sex;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

@Entity
@Table(name = "patient")
public class Patient extends BaseEntity {
    @Column(name = "hl7Id", nullable = false, unique = true)
    @Size(min = 1, max = 255)
    @NotNull
    private String hl7Id;

    @Column(name = "pesel", length = 11)
    @Size(min = 11, max = 11)
    @Pattern(regexp = "[0-9]+")
    private String pesel;

    @Column(name = "name", length = 15, nullable = false)
    @Size(min = 1, max = 15)
    @NotNull
    private String name;

    @Column(name = "surname", length = 30, nullable = false)
    @Size(min = 1, max = 30)
    @NotNull
    private String surname;

    @Column(name = "birth_date", nullable = false)
    @NotNull
    private Timestamp birthDate;

    @Column(name = "sex", nullable = false)
    @Enumerated(EnumType.STRING)
    @NotNull
    private Sex sex;

    @Embedded
    private Address address;

    @Column(name = "height", length = 30)
    @Size(max = 30)
    private String height;

    @Column(name = "weight", length = 30)
    @Size(max = 30)
    private String weight;

    @Column(name = "blood_type", length = 30)
    @Size(max = 30)
    private String bloodType;

    @Column(name = "allergies", length = 200)
    @Size(max = 200)
    private String allergies;

    @Column(name = "other_important_informations", length = 1000)
    @Size(max = 1000)
    private String otherImportantInformations;

    @Column(name = "last_modified_by_icupad")
    private Boolean lastModifiedByIcupad;

    @Override
    public int hashCode() {
        int result = pesel != null ? pesel.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (sex != null ? sex.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Patient patient = (Patient) o;

        if (address != null ? !address.equals(patient.address) : patient.address != null) return false;
        if (birthDate != null ? !birthDate.equals(patient.birthDate) : patient.birthDate != null) return false;
        if (name != null ? !name.equals(patient.name) : patient.name != null) return false;
        if (pesel != null ? !pesel.equals(patient.pesel) : patient.pesel != null) return false;
        if (sex != patient.sex) return false;
        return !(surname != null ? !surname.equals(patient.surname) : patient.surname != null);

    }


    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Timestamp getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Timestamp birthDate) {
        this.birthDate = birthDate;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getHl7Id() {
        return hl7Id;
    }

    public void setHl7Id(String hl7Id) {
            this.hl7Id = hl7Id;
        }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getAllergies() {
        return allergies;
    }

    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }

    public String getOtherImportantInformations() {
        return otherImportantInformations;
    }

    public void setOtherImportantInformations(String otherImportantInformations) {
        this.otherImportantInformations = otherImportantInformations;
    }

    public Boolean isLastModifiedByIcupad() {
        return lastModifiedByIcupad;
    }

    public void setLastModifiedByIcupad(Boolean lastModifiedByIcupad) {
        this.lastModifiedByIcupad = lastModifiedByIcupad;
    }
}

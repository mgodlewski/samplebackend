package com.icupad.entity.bloodgas;

import com.icupad.domain.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Table(name = "blood_gas_test_request")
public class BloodGasTestRequest  extends BaseEntity {

    @Column(name = "hl7id", nullable = false)
    @NotNull
    private String hl7Id;

    @Column(name = "request_date", nullable = false)
    @NotNull
    private Timestamp requestDate;

    @ManyToOne
    @JoinColumn(name = "test_id", referencedColumnName = "id")
    @NotNull
    private BloodGasTest bloodGasTest;

    @ManyToOne
    @JoinColumn(name = "test_panel_result_id", referencedColumnName = "id")
    @NotNull
    private BloodGasTestPanelResult bloodGasTestPanelResult;

    @OneToOne(mappedBy="bloodGasTestRequest")
    private BloodGasTestResult bloodGasTestResult;

    public String getHl7Id() {
        return hl7Id;
    }

    public void setHl7Id(String hl7Id) {
        this.hl7Id = hl7Id;
    }

    public Timestamp getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Timestamp requestDate) {
        this.requestDate = requestDate;
    }

    public BloodGasTest getBloodGasTest() {
        return bloodGasTest;
    }

    public void setBloodGasTest(BloodGasTest bloodGasTest) {
        this.bloodGasTest = bloodGasTest;
    }

    public BloodGasTestPanelResult getBloodGasTestPanelResult() {
        return bloodGasTestPanelResult;
    }

    public void setBloodGasTestPanelResult(BloodGasTestPanelResult bloodGasTestPanelResult) {
        this.bloodGasTestPanelResult = bloodGasTestPanelResult;
    }

    public BloodGasTestResult getBloodGasTestResult() {
        return bloodGasTestResult;
    }

    public void setBloodGasTestResult(BloodGasTestResult bloodGasTestResult) {
        this.bloodGasTestResult = bloodGasTestResult;
    }

    @Override
    public String toString() {
        return "BloodGasTestRequest{" +
                "hl7Id='" + hl7Id + '\'' +
                ", requestDate=" + requestDate +
                ", bloodGasTest=" + bloodGasTest +
                ", bloodGasTestPanelResult=" + bloodGasTestPanelResult +
                '}';
    }
}

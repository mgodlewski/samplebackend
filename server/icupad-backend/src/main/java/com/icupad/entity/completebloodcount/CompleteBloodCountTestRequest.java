package com.icupad.entity.completebloodcount;

import com.icupad.entity.BaseEntity;
import com.icupad.entity.bloodgas.BloodGasTest;
import com.icupad.entity.bloodgas.BloodGasTestPanelResult;
import com.icupad.entity.bloodgas.BloodGasTestResult;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/**
 * Created by Marcin on 22.05.2017.
 */
@Entity
@Table(name = "complete_blood_count_test_request")
public class CompleteBloodCountTestRequest extends BaseEntity {

    @Column(name = "hl7id", nullable = false)
    @NotNull
    private String hl7Id;

    @Column(name = "request_date", nullable = false)
    @NotNull
    private Timestamp requestDate;

    @ManyToOne
    @JoinColumn(name = "test_id", referencedColumnName = "id")
    @NotNull
    private CompleteBloodCountTest completeBloodCountTest;

    @ManyToOne
    @JoinColumn(name = "test_panel_result_id", referencedColumnName = "id")
    @NotNull
    private CompleteBloodCountTestPanelResult completeBloodCountTestPanelResult;

    @OneToOne(mappedBy="completeBloodCountTestRequest")
    private CompleteBloodCountTestResult  completeBloodCountTestResult ;



    public String getHl7Id() {
        return hl7Id;
    }

    public void setHl7Id(String hl7Id) {
        this.hl7Id = hl7Id;
    }

    public Timestamp getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Timestamp requestDate) {
        this.requestDate = requestDate;
    }

    public CompleteBloodCountTest getCompleteBloodCountTest() {
        return completeBloodCountTest;
    }

    public void setCompleteBloodCountTest(CompleteBloodCountTest completeBloodCountTest) {
        this.completeBloodCountTest = completeBloodCountTest;
    }

    public CompleteBloodCountTestPanelResult getCompleteBloodCountTestPanelResult() {
        return completeBloodCountTestPanelResult;
    }

    public void setCompleteBloodCountTestPanelResult(CompleteBloodCountTestPanelResult completeBloodCountTestPanelResult) {
        this.completeBloodCountTestPanelResult = completeBloodCountTestPanelResult;
    }

    public CompleteBloodCountTestResult getCompleteBloodCountTestRequest() {
        return completeBloodCountTestResult;
    }

    public void setCompleteBloodCountTestRequest(CompleteBloodCountTestResult completeBloodCountTestResult) {
        this.completeBloodCountTestResult = completeBloodCountTestResult;
    }
}

package com.icupad.entity.ecmo;

import com.icupad.domain.User;
import com.icupad.utils.DateTimeUtils;
import com.icupad.webmodel.ecmo.EcmoPatientDto;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/**
 * Created by Marcin on 03.03.2017.
 */
@Entity
@Table(name = "ecmo_patient")
public class EcmoPatient {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ecmo_procedure", referencedColumnName = "id")
    @NotNull
    private EcmoProcedure procedureId;

    @ManyToOne
    private User createdBy;

    @Column
    @NotNull
    private Timestamp date;

    @Column(name = "sat", nullable = true)
    private Double sat;

    @Column(name = "abp", nullable = true)
    private Double abp;

    @Column(name = "vcp", nullable = true)
    private Double vcp;

    @Column(name = "hr", nullable = true)
    private Double hr;

    @Column(name = "temp_surface", nullable = true)
    private Double tempSurface;

    @Column(name = "temp_core", nullable = true)
    private Double tempCore;

    @Column(name = "diuresis", nullable = true)
    private Double diuresis;

    @Column(name = "ultrafiltraction", nullable = true)
    private Double ultrafiltraction;

//    @Column(name = "actual", nullable = false)
//    private Boolean actual;

    public EcmoPatient (){};

    public EcmoPatient(EcmoPatientDto ecmoPatientDto, EcmoProcedure procedure, User createdBy){
        this.id = ecmoPatientDto.getId();
        this.procedureId = procedure;
        this.createdBy = createdBy;
        this.date = DateTimeUtils.getTimestampOrNullFromLong(ecmoPatientDto.getDate());
        this.sat = ecmoPatientDto.getSat();
        this.abp = ecmoPatientDto.getAbp();
        this.vcp = ecmoPatientDto.getVcp();
        this.hr = ecmoPatientDto.getHr();
        this.tempSurface = ecmoPatientDto.getTempSurface();
        this.tempCore = ecmoPatientDto.getTempCore();
        this.diuresis = ecmoPatientDto.getDiuresis();
        this.ultrafiltraction = ecmoPatientDto.getUltrafiltraction();
//        this.actual = ecmoPatientDto.getActual();
    }

    public java.lang.Long getId() {
        return id;
    }

    public void setId(java.lang.Long id) {
        this.id = id;
    }

    public EcmoProcedure getProcedureId() {
        return procedureId;
    }

    public void setProcedureId(EcmoProcedure procedureId) {
        this.procedureId = procedureId;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Double getSat() {
        return sat;
    }

    public void setSat(Double sat) {
        this.sat = sat;
    }

    public Double getAbp() {
        return abp;
    }

    public void setAbp(Double abp) {
        this.abp = abp;
    }

    public Double getVcp() {
        return vcp;
    }

    public void setVcp(Double vcp) {
        this.vcp = vcp;
    }

    public Double getHr() {
        return hr;
    }

    public void setHr(Double hr) {
        this.hr = hr;
    }

    public Double getTempSurface() {
        return tempSurface;
    }

    public void setTempSurface(Double tempSurface) {
        this.tempSurface = tempSurface;
    }

    public Double getTempCore() {
        return tempCore;
    }

    public void setTempCore(Double tempCore) {
        this.tempCore = tempCore;
    }

    public Double getDiuresis() {
        return diuresis;
    }

    public void setDiuresis(Double diuresis) {
        this.diuresis = diuresis;
    }

    public Double getUltrafiltraction() {
        return ultrafiltraction;
    }

    public void setUltrafiltraction(Double ultrafiltraction) {
        this.ultrafiltraction = ultrafiltraction;
    }

//    public Boolean getActual() {
//        return actual;
//    }
//
//    public void setActual(Boolean actual) {
//        this.actual = actual;
//    }
}

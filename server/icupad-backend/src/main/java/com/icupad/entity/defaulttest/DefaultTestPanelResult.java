package com.icupad.entity.defaulttest;

import com.icupad.entity.BaseEntity;
import com.icupad.entity.bloodgas.BloodGasTestRequest;
import com.icupad.entity.enums.BloodSource;
import com.icupad.entity.submodel.TestResultExecutor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 22.05.2017.
 */
@Entity
@Table(name = "default_test_panel_result")
public class DefaultTestPanelResult {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "created_date", nullable = false)
    @NotNull
    private Timestamp createdDate;

    @Column(name = "last_modified_date")
    private Timestamp lastModifiedDate;

    @Column(name = "request_date", nullable = false)
    @NotNull
    private Timestamp requestDate;

    @Embedded
    private TestResultExecutor testResultExecutor;

    @OneToMany(mappedBy = "defaultTestPanelResult")
    private List<DefaultTestRequest> defaultTestRequests = new ArrayList<>();



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Timestamp getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Timestamp lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Timestamp getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Timestamp requestDate) {
        this.requestDate = requestDate;
    }

    public TestResultExecutor getTestResultExecutor() {
        return testResultExecutor;
    }

    public void setTestResultExecutor(TestResultExecutor testResultExecutor) {
        this.testResultExecutor = testResultExecutor;
    }

    public List<DefaultTestRequest> getDefaultTestResults() {
        return defaultTestRequests;
    }

    public void setDefaultTestResults(List<DefaultTestRequest> defaultTestResults) {
        this.defaultTestRequests = defaultTestResults;
    }
}

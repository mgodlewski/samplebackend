package com.icupad.entity.bloodgas;

import com.icupad.domain.BaseEntity;
import com.icupad.entity.patient.Stay;
import com.icupad.entity.enums.Abnormality;
import com.icupad.entity.submodel.TestResultExecutor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Entity
@Table(name = "blood_gas_test_result")
public class BloodGasTestResult extends BaseEntity{

    @Column(name = "hl7id", nullable = false)
    private String hl7Id;

    @Column(name = "value", nullable = false)
    private Double value;

    @Column(name = "unit", nullable = false)
    private String unit;

    @Column(name = "norm")
    private String norm;

    @Column(name = "abnormality")
    @Enumerated(EnumType.STRING)
    private Abnormality abnormality;

    @OneToOne
    @JoinColumn(name = "test_request_id", referencedColumnName = "id")
    @NotNull
    private BloodGasTestRequest bloodGasTestRequest;

    @ManyToOne
    @JoinColumn(name = "stay_id", referencedColumnName = "id")
    @NotNull
    private Stay stay;

    @Column(name = "result_date", nullable = false)
    private Timestamp resultDate;

    @Embedded
    private TestResultExecutor executor;

    public String getHl7Id() {
        return hl7Id;
    }

    public void setHl7Id(String hl7Id) {
        this.hl7Id = hl7Id;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getNorm() {
        return norm;
    }

    public void setNorm(String norm) {
        this.norm = norm;
    }

    public Abnormality getAbnormality() {
        return abnormality;
    }

    public void setAbnormality(Abnormality abnormality) {
        this.abnormality = abnormality;
    }

    public BloodGasTestRequest getBloodGasTestRequest() {
        return bloodGasTestRequest;
    }

    public void setBloodGasTestRequest(BloodGasTestRequest bloodGasTestRequest) {
        this.bloodGasTestRequest = bloodGasTestRequest;
    }

    public Stay getStay() {
        return stay;
    }

    public void setStay(Stay stay) {
        this.stay = stay;
    }

    public Timestamp getResultDate() {
        return resultDate;
    }

    public void setResultDate(Timestamp resultDate) {
        this.resultDate = resultDate;
    }

    public TestResultExecutor getExecutor() {
        return executor;
    }

    public void setExecutor(TestResultExecutor executor) {
        this.executor = executor;
    }

    @Override
    public String toString() {
        return "BloodGasTestResult{" +
                "hl7Id='" + hl7Id + '\'' +
                ", value=" + value +
                ", unit='" + unit + '\'' +
                ", norm='" + norm + '\'' +
                ", abnormality=" + abnormality +
                ", bloodGasTestRequest=" + bloodGasTestRequest +
                ", stay=" + stay +
                ", resultDate=" + resultDate +
                ", executor=" + executor +
                '}';
    }
}

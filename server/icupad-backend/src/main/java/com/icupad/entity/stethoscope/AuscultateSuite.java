package com.icupad.entity.stethoscope;

import com.icupad.domain.BaseEntity;
import com.icupad.entity.patient.Patient;
import com.icupad.entity.submodel.TestResultExecutor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name = "auscultate_suite")
public class AuscultateSuite extends BaseEntity{
    @ManyToOne
    @JoinColumn(name = "patient_id", referencedColumnName = "id")
    private Patient patient;
    @Column(name = "description", nullable = false)
    private String description;
    @Embedded
    private TestResultExecutor executor;
    @ManyToOne
    @JoinColumn(name = "suite_schema_id", referencedColumnName = "id")
    @NotNull
    private AuscultateSuiteSchema auscultateSuiteSchema;
    @OneToMany(mappedBy = "auscultateSuite")
    private Set<Auscultate> auscultates;
    @Column(name = "position")
    private Long position;
    @Column(name="temperature")
    private Integer temperature;
    @Column(name = "is_respirated")
    private Boolean isRespirated;
    @Column(name = "passive_oxygen_therapy")
    private Boolean passiveOxygenTherapy;
    @Column(name = "examination_datetime")
    private Timestamp examinationDateTime;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TestResultExecutor getExecutor() {
        return executor;
    }

    public void setExecutor(TestResultExecutor executor) {
        this.executor = executor;
    }

    public AuscultateSuiteSchema getAuscultateSuiteSchema() {
        return auscultateSuiteSchema;
    }

    public void setAuscultateSuiteSchema(AuscultateSuiteSchema auscultateSuiteSchema) {
        this.auscultateSuiteSchema = auscultateSuiteSchema;
    }

    public Set<Auscultate> getAuscultates() {
        return auscultates;
    }

    public void setAuscultates(Set<Auscultate> auscultates) {
        this.auscultates = auscultates;
    }

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public Boolean getIsRespirated() {
        return isRespirated;
    }

    public void setIsRespirated(Boolean isRespirated) {
        this.isRespirated = isRespirated;
    }

    public Boolean getPassiveOxygenTherapy() {
        return passiveOxygenTherapy;
    }

    public void setPassiveOxygenTherapy(Boolean passiveOxygenTherapy) {
        this.passiveOxygenTherapy = passiveOxygenTherapy;
    }

    public Timestamp getExaminationDateTime() {
        return examinationDateTime;
    }

    public void setExaminationDateTime(Timestamp examinationDateTime) {
        this.examinationDateTime = examinationDateTime;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
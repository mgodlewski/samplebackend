package com.icupad.entity.enums;

public enum BloodSource {
    VEIN,
    ARTERY,
    CAPILLARY,
    UNKNOWN
}

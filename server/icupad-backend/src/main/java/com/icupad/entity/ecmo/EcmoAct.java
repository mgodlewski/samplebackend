package com.icupad.entity.ecmo;

import com.icupad.domain.User;
import com.icupad.utils.DateTimeUtils;
import com.icupad.webmodel.ecmo.EcmoActDto;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/**
 * Created by Marcin on 03.03.2017.
 */
@Entity
@Table(name = "ecmo_act")
public class EcmoAct {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ecmo_procedure", referencedColumnName = "id")
    @NotNull
    private EcmoProcedure procedureId;

    @ManyToOne
    private User createdBy;

    @Column
    @NotNull
    private Timestamp date;

    @Column(name = "act", nullable = false)
    private Double act;

//    @Column(name = "actual", nullable = false)
//    private Boolean actual;

    public EcmoAct(){}

    public EcmoAct(EcmoActDto ecmoActDto, EcmoProcedure procedure, User createdBy){
        this.id = ecmoActDto.getId();
        this.procedureId = procedure;
        this.createdBy = createdBy;
        this.date = DateTimeUtils.getTimestampOrNullFromLong(ecmoActDto.getDate());
        this.act = ecmoActDto.getAct();
//        this.actual = ecmoActDto.getActual();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EcmoProcedure getProcedureId() {
        return procedureId;
    }

    public void setProcedureId(EcmoProcedure configurationId) {
        this.procedureId = configurationId;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Double getAct() {
        return act;
    }

    public void setAct(Double act) {
        this.act = act;
    }

//    public Boolean getActual() {
//        return actual;
//    }
//
//    public void setActual(Boolean actual) {
//        this.actual = actual;
//    }
}

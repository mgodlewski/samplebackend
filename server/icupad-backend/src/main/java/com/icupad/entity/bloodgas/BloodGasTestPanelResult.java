package com.icupad.entity.bloodgas;

import com.icupad.domain.BaseEntity;
import com.icupad.entity.enums.BloodSource;
import com.icupad.entity.submodel.TestResultExecutor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "blood_gas_test_panel_result")
public class BloodGasTestPanelResult extends BaseEntity {

    @Column(name = "request_date", nullable = false)
    @NotNull
    private Timestamp requestDate;

    @Embedded
    private TestResultExecutor testResultExecutor;

    @Column(name = "blood_source")
    @Enumerated(value = EnumType.STRING)
    @NotNull
    private BloodSource bloodSource;

    @OneToMany(mappedBy = "bloodGasTestPanelResult")
    private List<BloodGasTestRequest> bloodGasTestRequests = new ArrayList<>();

    public Timestamp getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Timestamp requestDate) {
        this.requestDate = requestDate;
    }

    public TestResultExecutor getTestResultExecutor() {
        return testResultExecutor;
    }

    public void setTestResultExecutor(TestResultExecutor testResultExecutor) {
        this.testResultExecutor = testResultExecutor;
    }

    public BloodSource getBloodSource() {
        return bloodSource;
    }

    public void setBloodSource(BloodSource bloodSource) {
        this.bloodSource = bloodSource;
    }

    public List<BloodGasTestRequest> getBloodGasTestRequests() {
        return bloodGasTestRequests;
    }

    public void setBloodGasTestRequests(List<BloodGasTestRequest> bloodGasTestRequests) {
        this.bloodGasTestRequests = bloodGasTestRequests;
    }

    @Override
    public String toString() {
        return "BloodGasTestPanelResult{" +
                "requestDate=" + requestDate +
                ", testResultExecutor=" + testResultExecutor +
                ", bloodSource=" + bloodSource +
                ", bloodGasTestRequests=" + bloodGasTestRequests +
                '}';
    }
}

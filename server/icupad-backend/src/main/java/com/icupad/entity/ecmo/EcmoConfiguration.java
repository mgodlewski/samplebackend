package com.icupad.entity.ecmo;

import com.icupad.domain.User;
import com.icupad.utils.DateTimeUtils;
import com.icupad.webmodel.ecmo.EcmoConfigurationDto;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

/**
 * Created by Marcin on 03.03.2017.
 */
@Entity
@Table(name = "ecmo_configuration")
public class EcmoConfiguration {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ecmo_procedure", referencedColumnName = "id")
    @NotNull
    private EcmoProcedure procedureId;

    @ManyToOne
    private User createdBy;

    @Column(name = "mode", length = 50, nullable = false)
    @Size(min = 1, max = 50)
    @NotNull
    private String mode;

    @Column(name = "caiulation_type", length = 100, nullable = false)
    @Size(min = 1, max = 100)
    @NotNull
    private String caniulationType;

    @Column(name = "wentowanie_left_verticle", length = 100, nullable = false)
    @NotNull
    private String wentowanieLeftVentricle;

    @Column(name = "artery_canual_type", length = 100)
    private String arteryCannulaType;

    @Column(name = "artery_canual_size")
    private Double arteryCannulaSize;

    @Column(name = "oxygenerator_type", length = 100, nullable = false)
    @Size(max = 100)
    @NotNull
    private String oxygeneratorType;

    @Column(name = "actual", nullable = false)
    private Boolean actual;

    @Column
    @NotNull
    private Timestamp date;

    public EcmoConfiguration(){}

    public EcmoConfiguration(EcmoConfigurationDto ecmoConfigurationDto, EcmoProcedure ecmoProcedure, User createdBy) {
        this.id = ecmoConfigurationDto.getId();
        this.procedureId = ecmoProcedure;
        this.createdBy = createdBy;
        this.mode = ecmoConfigurationDto.getMode();
        this.caniulationType = ecmoConfigurationDto.getCaniulationType();
        this.wentowanieLeftVentricle = ecmoConfigurationDto.getWentowanieLeftVentricle();
        this.arteryCannulaType = ecmoConfigurationDto.getArteryCannulaType();
        this.arteryCannulaSize = ecmoConfigurationDto.getArteryCannulaSize();
        this.oxygeneratorType = ecmoConfigurationDto.getOxygeneratorType();
        this.actual = ecmoConfigurationDto.getActual();
        this.date = DateTimeUtils.getTimestampOrNullFromLong(ecmoConfigurationDto.getDate());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EcmoProcedure getProcedureId() {
        return procedureId;
    }

    public void setProcedureId(EcmoProcedure procedureId) {
        this.procedureId = procedureId;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getCaniulationType() {
        return caniulationType;
    }

    public void setCaniulationType(String caniulationType) {
        this.caniulationType = caniulationType;
    }

    public String getWentowanieLeftVentricle() {
        return wentowanieLeftVentricle;
    }

    public void setWentowanieLeftVentricle(String wentowanieLeftVentricle) {
        this.wentowanieLeftVentricle = wentowanieLeftVentricle;
    }

    public String getArteryCannulaType() {
        return arteryCannulaType;
    }

    public void setArteryCannulaType(String arteryCannulaType) {
        this.arteryCannulaType = arteryCannulaType;
    }

    public Double getArteryCannulaSize() {
        return arteryCannulaSize;
    }

    public void setArteryCannulaSize(Double arteryCannulaSize) {
        this.arteryCannulaSize = arteryCannulaSize;
    }

    public String getOxygeneratorType() {
        return oxygeneratorType;
    }

    public void setOxygeneratorType(String oxygeneratorType) {
        this.oxygeneratorType = oxygeneratorType;
    }

    public Boolean getActual() {
        return actual;
    }

    public void setActual(Boolean actual) {
        this.actual = actual;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }
}

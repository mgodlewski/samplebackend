package com.icupad.entity.bloodgas;

import com.icupad.domain.BaseEntity;
import com.icupad.entity.enums.Sex;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "blood_gas_test_default_norms")
public class BloodGasTestDefaultNorms extends BaseEntity{

    @Column(name = "test_id", nullable = false)
    private Long testId;

    @Column(name = "sex", nullable = false)
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @Column(name = "bottom_months_age", nullable = false)
    private Double bottomMonthsAge;

    @Column(name = "top_months_age", nullable = false)
    private Double topMonthsAge;

    @Column(name = "bottom_default_norm", nullable = false)
    private Double bottomDefaultNorm;

    @Column(name = "top_default_norm", nullable = false)
    private Double topDefaultNorm;


    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Double getBottomMonthsAge() {
        return bottomMonthsAge;
    }

    public void setBottomMonthsAge(Double bottomMonthsAge) {
        this.bottomMonthsAge = bottomMonthsAge;
    }

    public Double getTopMonthsAge() {
        return topMonthsAge;
    }

    public void setTopMonthsAge(Double topMonthsAge) {
        this.topMonthsAge = topMonthsAge;
    }

    public Double getBottomDefaultNorm() {
        return bottomDefaultNorm;
    }

    public void setBottomDefaultNorm(Double bottomDefaultNorm) {
        this.bottomDefaultNorm = bottomDefaultNorm;
    }

    public Double getTopDefaultNorm() {
        return topDefaultNorm;
    }


//    @Override
//    public String toString() {
//        return "BloodGasTestDefaultNorms{" +
//                ", testId=" + testId +
//                ", sex='" + sex + '\'' +
//                ", bottomMonthsAge=" + bottomMonthsAge +
//                ", topMonthsAge=" + topMonthsAge +
//                ", bottomDefaultNorm=" + bottomDefaultNorm +
//                ", topDefaultNorm=" + topDefaultNorm +
//                '}';
//    }
}

package com.icupad.entity.defaulttest;

import com.icupad.entity.BaseEntity;
import com.icupad.entity.bloodgas.BloodGasTest;
import com.icupad.entity.bloodgas.BloodGasTestPanelResult;
import com.icupad.entity.bloodgas.BloodGasTestResult;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/**
 * Created by Marcin on 22.05.2017.
 */
@Entity
@Table(name = "default_test_request")
public class DefaultTestRequest {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "created_date", nullable = false)
    @NotNull
    private Timestamp createdDate;

    @Column(name = "last_modified_date")
    private Timestamp lastModifiedDate;

    @Column(name = "hl7id", nullable = false)
    @NotNull
    private String hl7Id;

    @Column(name = "request_date", nullable = false)
    @NotNull
    private Timestamp requestDate;

    @ManyToOne
    @JoinColumn(name = "test_id", referencedColumnName = "id")
    @NotNull
    private DefaultTest defaultTest;

    @ManyToOne
    @JoinColumn(name = "test_panel_result_id", referencedColumnName = "id")
    @NotNull
    private DefaultTestPanelResult defaultTestPanelResult;

    @OneToOne(mappedBy="defaultTestRequest")
    private DefaultTestResult defaultTestResult;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Timestamp getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Timestamp lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getHl7Id() {
        return hl7Id;
    }

    public void setHl7Id(String hl7Id) {
        this.hl7Id = hl7Id;
    }

    public Timestamp getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Timestamp requestDate) {
        this.requestDate = requestDate;
    }

    public DefaultTest getDefaultTest() {
        return defaultTest;
    }

    public void setDefaultTest(DefaultTest defaultTest) {
        this.defaultTest = defaultTest;
    }

    public DefaultTestPanelResult getDefaultTestPanelResult() {
        return defaultTestPanelResult;
    }

    public void setDefaultTestPanelResult(DefaultTestPanelResult defaultTestPanelResult) {
        this.defaultTestPanelResult = defaultTestPanelResult;
    }

    public DefaultTestResult getDefaultTestResult() {
        return defaultTestResult;
    }

    public void setDefaultTestResult(DefaultTestResult defaultTestResult) {
        this.defaultTestResult = defaultTestResult;
    }
}


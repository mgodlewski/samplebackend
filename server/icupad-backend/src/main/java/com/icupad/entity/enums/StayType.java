package com.icupad.entity.enums;

public enum StayType {
    INPATIENT, OBSTETRICS, EMERGENCY, PREADMIT, RECURRING_PATIENT, OUTPATIENT
}

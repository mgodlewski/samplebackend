package com.icupad.entity.patient;

import com.icupad.domain.BaseEntity;
import com.icupad.domain.validation.constraints.Past;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

@Entity
@Table(name = "operation")
public class Operation extends BaseEntity {
    @ManyToOne
    @JoinColumn(name="stay_id", referencedColumnName = "id")
    private Stay stay;
    @Column(name = "operating_date", nullable = false)
    @NotNull
    private Timestamp operatingDate;
    @Column(name = "description")
    @Size(max = 1000)
    @NotNull
    private String description;
    @Column(name = "archived", nullable = false)
    @NotNull
    private boolean archived;

    public Stay getStay() {
        return stay;
    }

    public void setStay(Stay stay) {
        this.stay = stay;
    }

    public Timestamp getOperatingDate() {
        return operatingDate;
    }

    public void setOperatingDate(Timestamp operatingDate) {
        this.operatingDate = operatingDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }
}

package com.icupad.entity.ecmo;


import com.icupad.webmodel.ecmo.VeinCannulaDto;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

/**
 * Created by Marcin on 03.03.2017.
 */
@Entity
@Table(name = "ecmo_vein_cannula")
public class VeinCannula {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ecmo_configuration", referencedColumnName = "id")
    @NotNull
    private EcmoConfiguration configurationId;

    @Column(name = "type", nullable = false)
    @Size(min = 1, max = 100)
    @NotNull
    private String type;

    @Column(name = "size", nullable = false)
    private Double size;

    @Column
    @NotNull
    private Timestamp date;

    public VeinCannula(){}

    public VeinCannula(VeinCannulaDto veinCannulaDto, EcmoConfiguration ecmoConfiguration, Timestamp date) {
        this.configurationId = ecmoConfiguration;
        this.type = veinCannulaDto.getType();
        this.size = veinCannulaDto.getSize();
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EcmoConfiguration getChangeVeinCannulaId() {
        return configurationId;
    }

    public void setChangeVeinCannulaId(EcmoConfiguration changeVeinCannulaId) {
        this.configurationId = changeVeinCannulaId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }
}

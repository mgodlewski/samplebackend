package com.icupad.entity.stethoscope;

import com.icupad.domain.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "auscultate_suite_schema")
public class AuscultateSuiteSchema extends BaseEntity{
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "isPublic", nullable = false)
    private Boolean isPublic;
    @OneToMany(mappedBy = "auscultateSuiteSchema")
    private Set<AuscultatePoint> auscultatePoints;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Boolean aPublic) {
        isPublic = aPublic;
    }

    public Set<AuscultatePoint> getAuscultatePoints() {
        return auscultatePoints;
    }

    public void setAuscultatePoints(Set<AuscultatePoint> auscultatePoints) {
        this.auscultatePoints = auscultatePoints;
    }
}

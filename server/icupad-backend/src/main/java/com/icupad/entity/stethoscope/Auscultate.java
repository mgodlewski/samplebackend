package com.icupad.entity.stethoscope;

import com.icupad.domain.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "auscultation")
public class Auscultate extends BaseEntity{
    @Column(name = "description")
    private String description;
    @Column(name = "wav_name", nullable = false)
    private String wavName;
    @ManyToOne
    @JoinColumn(name = "point_id", referencedColumnName = "id")
    @NotNull
    private AuscultatePoint auscultatePoint;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="suite_id")
    private AuscultateSuite auscultateSuite;
    @Column(name="poll_result")
    private Long pollResult;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWavName() {
        return wavName;
    }

    public void setWavName(String wavName) {
        this.wavName = wavName;
    }

    public AuscultatePoint getAuscultatePoint() {
        return auscultatePoint;
    }

    public void setAuscultatePoint(AuscultatePoint auscultatePoint) {
        this.auscultatePoint = auscultatePoint;
    }

    public AuscultateSuite getAuscultateSuite() {
        return auscultateSuite;
    }

    public void setAuscultateSuite(AuscultateSuite auscultateSuite) {
        this.auscultateSuite = auscultateSuite;
    }

    public Long getPollResult() {
        return pollResult;
    }

    public void setPollResult(Long pollResult) {
        this.pollResult = pollResult;
    }
}

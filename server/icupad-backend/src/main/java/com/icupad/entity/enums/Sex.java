package com.icupad.entity.enums;

public enum Sex {
    MALE, FEMALE, BOTH, UNKNOWN
}

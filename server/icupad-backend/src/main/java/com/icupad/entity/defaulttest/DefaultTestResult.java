package com.icupad.entity.defaulttest;

import com.icupad.entity.BaseEntity;
import com.icupad.entity.bloodgas.BloodGasTestRequest;
import com.icupad.entity.enums.Abnormality;
import com.icupad.entity.patient.Stay;
import com.icupad.entity.submodel.TestResultExecutor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/**
 * Created by Marcin on 22.05.2017.
 */

@Entity
@Table(name = "default_test_result")
public class DefaultTestResult {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "created_date", nullable = false)
    @NotNull
    private Timestamp createdDate;

    @Column(name = "last_modified_date")
    private Timestamp lastModifiedDate;

    @Column(name = "hl7id", nullable = false)
    private String hl7Id;

    @Column(name = "value", nullable = false)
    private Double value;

    @Column(name = "unit", nullable = false)
    private String unit;

    @Column(name = "norm")
    private String norm;

    @Column(name = "abnormality")
    @Enumerated(EnumType.STRING)
    private Abnormality abnormality;

    @OneToOne
    @JoinColumn(name = "test_request_id", referencedColumnName = "id")
    @NotNull
    private DefaultTestRequest defaultTestRequest;

    @ManyToOne
    @JoinColumn(name = "stay_id", referencedColumnName = "id")
    @NotNull
    private Stay stay;

    @Column(name = "result_date", nullable = false)
    private Timestamp resultDate;

    @Embedded
    private TestResultExecutor executor;




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Timestamp getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Timestamp lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getHl7Id() {
        return hl7Id;
    }

    public void setHl7Id(String hl7Id) {
        this.hl7Id = hl7Id;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getNorm() {
        return norm;
    }

    public void setNorm(String norm) {
        this.norm = norm;
    }

    public Abnormality getAbnormality() {
        return abnormality;
    }

    public void setAbnormality(Abnormality abnormality) {
        this.abnormality = abnormality;
    }

    public DefaultTestRequest getDefaultTestRequest() {
        return defaultTestRequest;
    }

    public void setDefaultTestRequest(DefaultTestRequest defaultTestRequest) {
        this.defaultTestRequest = defaultTestRequest;
    }

    public Stay getStay() {
        return stay;
    }

    public void setStay(Stay stay) {
        this.stay = stay;
    }

    public Timestamp getResultDate() {
        return resultDate;
    }

    public void setResultDate(Timestamp resultDate) {
        this.resultDate = resultDate;
    }

    public TestResultExecutor getExecutor() {
        return executor;
    }

    public void setExecutor(TestResultExecutor executor) {
        this.executor = executor;
    }
}

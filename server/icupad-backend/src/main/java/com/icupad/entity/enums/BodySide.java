package com.icupad.entity.enums;

public enum BodySide {
    FRONT, BACK
}

package com.icupad.entity.stethoscope;

import com.icupad.domain.BaseEntity;
import com.icupad.entity.enums.BodySide;

import javax.persistence.*;

@Entity
@Table(name = "auscultate_point")
public class AuscultatePoint extends BaseEntity{
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "queue_position", nullable = false)
    private int queuePosition;
    @Column(name = "x", nullable = false)
    private Double x;
    @Column(name = "y", nullable = false)
    private Double y;
    @Column(name = "is_front", nullable = false)
    private boolean isFront;
    @ManyToOne
    @JoinColumn(name="suite_schema_id", referencedColumnName = "id")
    private AuscultateSuiteSchema auscultateSuiteSchema;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQueuePosition() {
        return queuePosition;
    }

    public void setQueuePosition(int queuePosition) {
        this.queuePosition = queuePosition;
    }

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public boolean getIsFront() {
        return isFront;
    }

    public void setIsFront(boolean isFront) {
        this.isFront = isFront;
    }

    public AuscultateSuiteSchema getAuscultateSuiteSchema() {
        return auscultateSuiteSchema;
    }

    public void setAuscultateSuiteSchema(AuscultateSuiteSchema auscultateSuiteSchema) {
        this.auscultateSuiteSchema = auscultateSuiteSchema;
    }
}

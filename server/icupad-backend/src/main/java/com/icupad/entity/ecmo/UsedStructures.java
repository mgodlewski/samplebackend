package com.icupad.entity.ecmo;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

/**
 * Created by Marcin on 03.03.2017.
 */
@Entity
@Table(name = "ecmo_used_structures")
public class UsedStructures {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ecmo_configuration", referencedColumnName = "id")
    @NotNull
    private EcmoConfiguration configurationId;

    @Column
    @NotNull
    private Timestamp date;

    @Column(name = "name", nullable = false)
    @Size(min = 1, max = 40)
    @NotNull
    private String name;

    public UsedStructures(){}

    public UsedStructures(String cannulationStructure, EcmoConfiguration ecmoConfiguration, Timestamp date) {
        this.configurationId = ecmoConfiguration;
        this.date = date;
        this.name = cannulationStructure;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EcmoConfiguration getConfigurationId() {
        return configurationId;
    }

    public void setConfigurationId(EcmoConfiguration configurationId) {
        this.configurationId = configurationId;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name ) {
        this.name = name;
    }
}

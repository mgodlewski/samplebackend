package com.icupad.entity.enums;

public enum Abnormality {
    BELOW_LOW_NORM,
    ABOVE_HIGH_NORM
}

package com.icupad.entity.completebloodcount;

import com.icupad.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Marcin on 22.05.2017.
 */

@Entity
@Table(name = "complete_blood_count_test")
public class CompleteBloodCountTest extends BaseEntity{

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "unit", nullable = false)
    private String unit;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}

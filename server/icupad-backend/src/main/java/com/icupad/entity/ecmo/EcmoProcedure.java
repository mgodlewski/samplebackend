package com.icupad.entity.ecmo;

import com.icupad.domain.User;
import com.icupad.entity.patient.Stay;
import com.icupad.utils.DateTimeUtils;
import com.icupad.webmodel.ecmo.EcmoProcedureDto;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

/**
 * Created by Marcin on 03.03.2017.
 */
@Entity
@Table(name = "ecmo_procedure")
public class EcmoProcedure {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "stay_id", referencedColumnName = "id")
    @NotNull
    private Stay stay;

    @ManyToOne
    private User createdBy;

    @ManyToOne
    private User endBy;

    @Column(name = "start_cause", length = 100, nullable = false)
    @Size(min = 1, max = 100)
    @NotNull
    private String startCause;

    @Column
    @NotNull
    private Timestamp startDate;

    @Column(name = "end_cause", length = 100)
    @Size(max = 100)
    private String endCause;

    @Column
    private Timestamp endDate;

    public EcmoProcedure(){}

    public EcmoProcedure(EcmoProcedureDto ecmoProcedureDto, User createdByUser, Stay stay, User endByUser) {
        this.id = ecmoProcedureDto.getId();
        this.stay = stay;
        this.createdBy = createdByUser;
        this.endBy = endByUser;
        this.startCause = ecmoProcedureDto.getStartCause();
        this.startDate = DateTimeUtils.getTimestampOrNullFromLong(ecmoProcedureDto.getStartDate());
        this.endCause = ecmoProcedureDto.getEndCause();
        this.endDate = DateTimeUtils.getTimestampOrNullFromLong(ecmoProcedureDto.getEndDate());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Stay getStay() {
        return stay;
    }

    public void setStay(Stay stay) {
        this.stay = stay;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public String getStartCause() {
        return startCause;
    }

    public void setStartCause(String startCause) {
        this.startCause = startCause;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public String getEndCause() {
        return endCause;
    }

    public void setEndCause(String endCause) {
        this.endCause = endCause;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public User getEndBy() {
        return endBy;
    }

    public void setEndBy(User endBy) {
        this.endBy = endBy;
    }
}

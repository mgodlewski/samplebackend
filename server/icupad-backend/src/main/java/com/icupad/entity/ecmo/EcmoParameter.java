package com.icupad.entity.ecmo;

import com.icupad.domain.User;
import com.icupad.utils.DateTimeUtils;
import com.icupad.webmodel.ecmo.EcmoParameterDto;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

/**
 * Created by Marcin on 03.03.2017.
 */
@Entity
@Table(name = "ecmo_parameter")
public class EcmoParameter {
    @Id
    @GeneratedValue
    private java.lang.Long id;

    @ManyToOne
    @JoinColumn(name = "ecmo_procedure", referencedColumnName = "id")
    @NotNull
    private EcmoProcedure procedureId;

    @ManyToOne
    private User createdBy;

    @Column
    @NotNull
    private Timestamp date;

    @Column(name = "rpm", nullable = true)
    private Double rpm;

    @Column(name = "blood_flow", nullable = true)
    private Double bloodFlow;

    @Column(name = "FiO2", nullable = true)
    private Double fiO2;

    @Column(name = "gas_flow", nullable = true)
    private Double gasFlow;

    @Column(name = "p1", nullable = true)
    private Double p1;

    @Column(name = "p2", nullable = true)
    private Double p2;

    @Column(name = "p3", nullable = true)
    private Double p3;

    @Column(name = "delta", nullable = true)
    private Double delta;

    @Column(name = "heparin_suply", nullable = true)
    private Double heparinSuply;

    @Column(name = "description", nullable = true)
    @Size(max = 250)
    @NotNull
    private String description;

//    @Column(name = "actual", nullable = false)
//    private Boolean actual;

    public EcmoParameter(){}

    public EcmoParameter(EcmoParameterDto ecmoParameterDto, EcmoProcedure ecmoProcedure, User createdBy) {
        this.id = ecmoParameterDto.getId();
        this.procedureId = ecmoProcedure;
        this.createdBy = createdBy;
        this.date = DateTimeUtils.getTimestampOrNullFromLong(ecmoParameterDto.getDate());
        this.rpm = ecmoParameterDto.getRpm();
        this.bloodFlow = ecmoParameterDto.getBloodFlow();
        this.fiO2 = ecmoParameterDto.getFiO2();
        this.gasFlow = ecmoParameterDto.getGasFlow();
        this.p1 = ecmoParameterDto.getP1();
        this.p2 = ecmoParameterDto.getP2();
        this.p3 = ecmoParameterDto.getP3();
        this.delta = ecmoParameterDto.getDelta();
        this.heparinSuply = ecmoParameterDto.getHeparinSuply();
        this.description = ecmoParameterDto.getDescription();
//        this.actual = ecmoParameterDto.getActual();
    }

    public java.lang.Long getId() {
        return id;
    }

    public void setId(java.lang.Long id) {
        this.id = id;
    }

    public EcmoProcedure getProcedureId() {
        return procedureId;
    }

    public void setProcedureId(EcmoProcedure procedureId) {
        this.procedureId = procedureId;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Double getRpm() {
        return rpm;
    }

    public void setRpm(Double rpm) {
        this.rpm = rpm;
    }

    public Double getBloodFlow() {
        return bloodFlow;
    }

    public void setBloodFlow(Double bloodFlow) {
        this.bloodFlow = bloodFlow;
    }

    public Double getFiO2() {
        return fiO2;
    }

    public void setFiO2(Double fiO2) {
        this.fiO2 = fiO2;
    }

    public Double getGasFlow() {
        return gasFlow;
    }

    public void setGasFlow(Double gasFlow) {
        this.gasFlow = gasFlow;
    }

    public Double getP1() {
        return p1;
    }

    public void setP1(Double p1) {
        this.p1 = p1;
    }

    public Double getP2() {
        return p2;
    }

    public void setP2(Double p2) {
        this.p2 = p2;
    }

    public Double getP3() {
        return p3;
    }

    public void setP3(Double p3) {
        this.p3 = p3;
    }

    public Double getDelta() {
        return delta;
    }

    public void setDelta(Double delta) {
        this.delta = delta;
    }

    public Double getHeparinSuply() {
        return heparinSuply;
    }

    public void setHeparinSuply(Double heparinSuply) {
        this.heparinSuply = heparinSuply;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

//    public Boolean getActual() {
//        return actual;
//    }
//
//    public void setActual(Boolean actual) {
//        this.actual = actual;
//    }
}

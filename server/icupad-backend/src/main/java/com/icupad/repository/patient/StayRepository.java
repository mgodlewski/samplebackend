package com.icupad.repository.patient;

import com.icupad.entity.patient.Stay;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface StayRepository extends CrudRepository<Stay, Long> {
    List<Stay> findAll();

    @Query(value = "select s from Stay s where s.createdDate > ?1 " +
            "or (s.lastModifiedDate is not null and s.lastModifiedDate > ?1)")
//    @Query(value = "select s from Stay s where (s.createdDate > ?1 and s.dischargeDate is null) " +
//            "or (s.lastModifiedDate is not null and s.lastModifiedDate > ?1)")
    List<Stay> findAllAfter(Timestamp after);

    @Query(value = "select max(s.lastModifiedDate) from Stay s")
    Timestamp getMaxLastModifiedDate();

    @Query(value = "select max(s.createdDate) from Stay s")
    Timestamp getMaxCreatedDate();

    @Query(value = "select s from Stay s where s.dischargeDate is null and s.createdDate > ?1 ")
    List<Stay> findAllActiveAfter(Timestamp after);
}
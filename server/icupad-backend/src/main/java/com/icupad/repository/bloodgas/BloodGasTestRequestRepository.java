package com.icupad.repository.bloodgas;

import com.icupad.entity.bloodgas.BloodGasTestRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

public interface BloodGasTestRequestRepository extends CrudRepository<BloodGasTestRequest, Long> {

    @Query(value = "select max(b.lastModifiedDate) from BloodGasTestRequest b")
    Timestamp getMaxLastModifiedDate();

    @Query(value = "select max(b.createdDate) from BloodGasTestRequest b")
    Timestamp getMaxCreatedDate();
}

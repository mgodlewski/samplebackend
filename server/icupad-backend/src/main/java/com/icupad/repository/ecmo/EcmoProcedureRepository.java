package com.icupad.repository.ecmo;

import com.icupad.entity.ecmo.EcmoProcedure;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Marcin on 05.03.2017.
 */
public interface EcmoProcedureRepository extends CrudRepository<EcmoProcedure, java.lang.Long> {

    @Query(value = "select p from EcmoProcedure p where p.startDate > ?1 or p.endDate > ?1" +
            "       and p.stay.patient.id in ?2")
    List<EcmoProcedure> findAllAfterAndPatientIdIn(Timestamp after, List<Long> patientIds, Pageable pageable);

    @Query(value = "select max(p.startDate) from EcmoProcedure p")
    Timestamp getMaxCreatedDate();

    @Query(value = "select max(p.endDate) from EcmoProcedure p")
    Timestamp getMaxEndDate();
}

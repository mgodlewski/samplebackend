package com.icupad.repository.ecmo;

import com.icupad.entity.ecmo.EcmoConfiguration;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Marcin on 04.03.2017.
 */
public interface EcmoConfigurationRepository extends CrudRepository<EcmoConfiguration, Long> {

    @Query(value = "select p from EcmoConfiguration p where p.date > ?1" +
            "       and p.procedureId.stay.patient.id in ?2")
    List<EcmoConfiguration> findAllAfterAndPatientIdIn(Timestamp after, List<Long> patientIds, Pageable pageable);

    @Query(value = "select max(p.date) from EcmoConfiguration p")
    Timestamp getMaxCreatedDate();
}

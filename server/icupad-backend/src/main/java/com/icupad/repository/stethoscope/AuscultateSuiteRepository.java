package com.icupad.repository.stethoscope;

import com.icupad.entity.stethoscope.AuscultateSuite;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface AuscultateSuiteRepository extends CrudRepository<AuscultateSuite, Long> {

    //TODO CHECK IF IT'S WORKING
    @Query(value = "select a from AuscultateSuite a " +
            "where a.createdDate > ?1 or (a.lastModifiedDate is not null and a.lastModifiedDate > ?1)")
    List<AuscultateSuite> findAllAfter(Timestamp after);

    @Query(value = "select a from AuscultateSuite a " +
            "where a.createdDate > ?1 or (a.lastModifiedDate is not null and a.lastModifiedDate > ?1)" +
            "       and a.patient.id in ?2")
    List<AuscultateSuite> findAllAfterAndPatientIdIn(Timestamp after, List<Long> patientIds);

    @Query(value = "select max(a.lastModifiedDate) from AuscultateSuite a")
    Timestamp getMaxLastModifiedDate();

    @Query(value = "select max(a.createdDate) from AuscultateSuite a")
    Timestamp getMaxCreatedDate();
}

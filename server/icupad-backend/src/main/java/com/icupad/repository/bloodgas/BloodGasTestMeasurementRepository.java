package com.icupad.repository.bloodgas;

import com.icupad.entity.bloodgas.BloodGasTestPanelResult;
import com.icupad.webmodel.bloodgas.BloodGasTestMeasurementDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Marcin on 09.06.2017.
 */
public interface BloodGasTestMeasurementRepository extends CrudRepository<BloodGasTestPanelResult, Long> {

    @Query(value = "select new com.icupad.webmodel.bloodgas.BloodGasTestMeasurementDto(request.bloodGasTest.name, request.bloodGasTest.unit, " +
            "b.bloodSource, request.bloodGasTestResult.value, request.bloodGasTestResult.abnormality, " +
            "request.bloodGasTestResult.resultDate, request.bloodGasTestResult.stay.id, request.bloodGasTest.id, " +
            "request.bloodGasTestResult.stay.patient.birthDate)" +
            " from BloodGasTestPanelResult b join b.bloodGasTestRequests request " +
            "where (b.createdDate > ?1 or (b.lastModifiedDate is not null and b.lastModifiedDate > ?1)) " +
            "       and request.bloodGasTestResult.stay.patient.id in ?2 ")

    List<BloodGasTestMeasurementDto> findAllAfterAndPatientIdIn(Timestamp after, List<Long> patientIds, Pageable pageable);
}
package com.icupad.repository.completebloodcount;

import com.icupad.entity.bloodgas.BloodGasTestDefaultNorms;
import com.icupad.entity.bloodgas.BloodGasTestResult;
import com.icupad.entity.completebloodcount.CompleteBloodCountTestRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Marcin on 22.05.2017.
 */
public interface CompleteBloodCountTestRequestRepository extends CrudRepository<CompleteBloodCountTestRequest, Long> {

    @Query(value = "select max(b.lastModifiedDate) from CompleteBloodCountTestRequest b")
    Timestamp getMaxLastModifiedDate();

    @Query(value = "select max(b.createdDate) from CompleteBloodCountTestRequest b")
    Timestamp getMaxCreatedDate();
}

package com.icupad.repository.completebloodcount;

import com.icupad.entity.bloodgas.BloodGasTestDefaultNorms;
import com.icupad.entity.bloodgas.BloodGasTestPanelResult;
import com.icupad.entity.completebloodcount.CompleteBloodCountTestPanelResult;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Marcin on 22.05.2017.
 */
public interface CompleteBloodCountTestPanelResultRepository extends CrudRepository<CompleteBloodCountTestPanelResult, Long> {

    @Query(value = "select max(b.lastModifiedDate) from CompleteBloodCountTestPanelResult b")
    Timestamp getMaxLastModifiedDate();

    @Query(value = "select max(b.createdDate) from CompleteBloodCountTestPanelResult b")
    Timestamp getMaxCreatedDate();
}

package com.icupad.repository.bloodgas;

import com.icupad.entity.bloodgas.BloodGasTest;
import com.icupad.entity.bloodgas.BloodGasTestDefaultNorms;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

public interface BloodGasTestDefaultNormsRepository extends CrudRepository<BloodGasTestDefaultNorms, Long> {

    @Query(value = "select max(b.lastModifiedDate) from BloodGasTestDefaultNorms b")
    Timestamp getMaxLastModifiedDate();

    @Query(value = "select max(b.createdDate) from BloodGasTestDefaultNorms b")
    Timestamp getMaxCreatedDate();
}

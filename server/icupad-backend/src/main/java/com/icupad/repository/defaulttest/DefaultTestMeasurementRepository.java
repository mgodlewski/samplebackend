package com.icupad.repository.defaulttest;

import com.icupad.entity.defaulttest.DefaultTestPanelResult;
import com.icupad.webmodel.defaulttest.DefaultTestMeasurementDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Marcin on 10.06.2017.
 */
public interface DefaultTestMeasurementRepository extends CrudRepository<DefaultTestPanelResult, Long> {

    @Query(value = "select new com.icupad.webmodel.defaulttest.DefaultTestMeasurementDto(request.defaultTest.name, request.defaultTest.unit, " +
            "request.defaultTestResult.value, request.defaultTestResult.abnormality, " +
            "request.defaultTestResult.resultDate, request.defaultTestResult.stay.id, request.defaultTest.id, " +
            "request.defaultTestResult.stay.patient.birthDate)" +
            " from DefaultTestPanelResult b join b.defaultTestRequests request " +
            "where (b.createdDate > ?1 or (b.lastModifiedDate is not null and b.lastModifiedDate > ?1)) " +
            "       and request.defaultTestResult.stay.patient.id in ?2 ")

    List<DefaultTestMeasurementDto> findAllAfterAndPatientIdIn(Timestamp after, List<Long> patientIds, Pageable pageable);
}

package com.icupad.repository.stethoscope;

import com.icupad.entity.stethoscope.AuscultateSuiteSchema;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface AuscultateSuiteSchemaRepository extends CrudRepository<AuscultateSuiteSchema, Long> {
    List<AuscultateSuiteSchema> findAll();

    @Query(value = "select a from AuscultateSuiteSchema a where a.createdDate > ?1 " +
            "or (a.lastModifiedDate is not null and a.lastModifiedDate > ?1)")
    List<AuscultateSuiteSchema> findAllAfter(Timestamp after);

    @Query(value = "select max(a.lastModifiedDate) from AuscultateSuiteSchema a")
    Timestamp getMaxLastModifiedDate();

    @Query(value = "select max(a.createdDate) from AuscultateSuiteSchema a")
    Timestamp getMaxCreatedDate();
}

package com.icupad.repository.defaulttest;

import com.icupad.entity.completebloodcount.CompleteBloodCountTest;
import com.icupad.entity.completebloodcount.CompleteBloodCountTestPanelResult;
import com.icupad.entity.defaulttest.DefaultTest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Marcin on 22.05.2017.
 */
public interface DefaultTestRepository extends CrudRepository<DefaultTest, Long> {

    @Query(value = "select max(b.lastModifiedDate) from DefaultTest b")
    Timestamp getMaxLastModifiedDate();

    @Query(value = "select max(b.createdDate) from DefaultTest b")
    Timestamp getMaxCreatedDate();
}

package com.icupad.repository.bloodgas;

import com.icupad.entity.bloodgas.BloodGasTest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

public interface BloodGasTestRepository extends CrudRepository<BloodGasTest, Long> {
    @Query(value = "select max(b.lastModifiedDate) from BloodGasTest b")
    Timestamp getMaxLastModifiedDate();

    @Query(value = "select max(b.createdDate) from BloodGasTest b")
    Timestamp getMaxCreatedDate();
}

package com.icupad.repository.completebloodcount;

import com.icupad.entity.completebloodcount.CompleteBloodCountTestPanelResult;
import com.icupad.webmodel.completebloodcount.CompleteBloodCountTestMeasurementDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Marcin on 10.06.2017.
 */
public interface CompleteBloodCountTestMeasurementRepository extends CrudRepository<CompleteBloodCountTestPanelResult, Long> {

    @Query(value = "select new com.icupad.webmodel.completebloodcount.CompleteBloodCountTestMeasurementDto(request.completeBloodCountTest.name, request.completeBloodCountTest.unit, " +
            "b.bloodSource, request.completeBloodCountTestResult.value, request.completeBloodCountTestResult.abnormality, " +
            "request.completeBloodCountTestResult.resultDate, request.completeBloodCountTestResult.stay.id, request.completeBloodCountTest.id, " +
            "request.completeBloodCountTestResult.stay.patient.birthDate)" +
            " from CompleteBloodCountTestPanelResult b join b.completeBloodCountTestRequests request " +
            "where (b.createdDate > ?1 or (b.lastModifiedDate is not null and b.lastModifiedDate > ?1)) " +
            "       and request.completeBloodCountTestResult.stay.patient.id in ?2 ")

    List<CompleteBloodCountTestMeasurementDto> findAllAfterAndPatientIdIn(Timestamp after, List<Long> patientIds, Pageable pageable);
}

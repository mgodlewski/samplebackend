package com.icupad.repository.stethoscope;

import com.icupad.entity.stethoscope.Auscultate;
import com.icupad.entity.stethoscope.AuscultatePoint;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface AuscultateRepository extends CrudRepository<Auscultate, Long> {

    @Query(value = "select a from Auscultate a " +
            "where a.createdDate > ?1 or (a.lastModifiedDate is not null and a.lastModifiedDate > ?1)")
    List<Auscultate> findAllAfter(Timestamp after);

    @Query(value = "select a from Auscultate a " +
            "where a.createdDate > ?1 or (a.lastModifiedDate is not null and a.lastModifiedDate > ?1)" +
            "       and a.auscultateSuite.patient.id in ?2")
    List<Auscultate> findAllAfterAndPatientIdIn(Timestamp after, List<Long> patientIds);

    @Query(value = "select max(a.lastModifiedDate) from Auscultate a")
    Timestamp getMaxLastModifiedDate();

    @Query(value = "select max(a.createdDate) from Auscultate a")
    Timestamp getMaxCreatedDate();
}

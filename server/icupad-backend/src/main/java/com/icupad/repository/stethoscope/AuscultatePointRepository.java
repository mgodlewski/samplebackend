package com.icupad.repository.stethoscope;

import com.icupad.entity.stethoscope.AuscultatePoint;
import com.icupad.entity.stethoscope.AuscultateSuiteSchema;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Repository
public interface AuscultatePointRepository extends CrudRepository<AuscultatePoint, Long> {

    @Query(value = "select a from AuscultatePoint a " +
            "where a.createdDate > ?1 or (a.lastModifiedDate is not null and a.lastModifiedDate > ?1)")
    List<AuscultatePoint> findAllAfter(Timestamp after);

    //SuiteSchemaId/auscultateSuiteSchema
    Set<AuscultatePoint> findByAuscultateSuiteSchema(AuscultateSuiteSchema AuscultateSuiteSchema);

    @Query(value = "select max(a.lastModifiedDate) from AuscultatePoint a")
    Timestamp getMaxLastModifiedDate();

    @Query(value = "select max(a.createdDate) from AuscultatePoint a")
    Timestamp getMaxCreatedDate();
}

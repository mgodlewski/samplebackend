package com.icupad.repository.defaulttest;

import com.icupad.entity.completebloodcount.CompleteBloodCountTestPanelResult;
import com.icupad.entity.completebloodcount.CompleteBloodCountTestRequest;
import com.icupad.entity.defaulttest.DefaultTestRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Marcin on 22.05.2017.
 */
public interface DefaultTestRequestRepository extends CrudRepository<DefaultTestRequest, Long> {

    @Query(value = "select max(b.lastModifiedDate) from DefaultTestRequest b")
    Timestamp getMaxLastModifiedDate();

    @Query(value = "select max(b.createdDate) from DefaultTestRequest b")
    Timestamp getMaxCreatedDate();
}

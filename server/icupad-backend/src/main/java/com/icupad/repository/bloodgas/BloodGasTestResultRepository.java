package com.icupad.repository.bloodgas;

import com.icupad.entity.bloodgas.BloodGasTest;
import com.icupad.entity.bloodgas.BloodGasTestResult;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

public interface BloodGasTestResultRepository extends CrudRepository<BloodGasTestResult, Long> {

    @Query(value = "select max(b.lastModifiedDate) from BloodGasTestResult b")
    Timestamp getMaxLastModifiedDate();

    @Query(value = "select max(b.createdDate) from BloodGasTestResult b")
    Timestamp getMaxCreatedDate();
}

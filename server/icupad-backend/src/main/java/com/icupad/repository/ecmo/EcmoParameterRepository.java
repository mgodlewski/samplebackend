package com.icupad.repository.ecmo;

import com.icupad.entity.ecmo.EcmoAct;
import com.icupad.entity.ecmo.EcmoParameter;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Marcin on 06.03.2017.
 */
public interface EcmoParameterRepository extends CrudRepository<EcmoParameter, Long> {

    @Query(value = "select p from EcmoParameter p where p.date > ?1" +
            "       and p.procedureId.stay.patient.id in ?2")
    List<EcmoParameter> findAllAfterAndPatientIdIn(Timestamp after, List<Long> patientIds, Pageable pageable);

    @Query(value = "select max(p.date) from EcmoParameter p")
    Timestamp getMaxCreatedDate();
}


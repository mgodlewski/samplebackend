package com.icupad.repository.patient;

import com.icupad.entity.patient.Operation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface OperationRepository extends CrudRepository<Operation, Long> {

    @Query(value = "select o from Operation o " +
            "where (o.createdDate > ?1 or (o.lastModifiedDate is not null and o.lastModifiedDate > ?1)) " +
            "       and o.stay.patient.id in ?2")
    List<Operation> findAllAfter(Timestamp after, List<Long> patientIds);

    @Query(value = "select max(o.lastModifiedDate) from Operation o")
    Timestamp getMaxLastModifiedDate();

    @Query(value = "select max(o.createdDate) from Operation o")
    Timestamp getMaxCreatedDate();
}

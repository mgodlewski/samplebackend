package com.icupad.repository.bloodgas;

import com.icupad.entity.bloodgas.BloodGasTestPanelResult;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

public interface BloodGasTestPanelResultRepository extends CrudRepository<BloodGasTestPanelResult, Long> {

    @Query(value = "select max(b.lastModifiedDate) from BloodGasTestPanelResult b")
    Timestamp getMaxLastModifiedDate();

    @Query(value = "select max(b.createdDate) from BloodGasTestPanelResult b")
    Timestamp getMaxCreatedDate();
}

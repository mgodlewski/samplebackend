package com.icupad.repository.completebloodcount;

import com.icupad.entity.completebloodcount.CompleteBloodCountTest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Marcin on 22.05.2017.
 */
public interface CompleteBloodCountTestRepository extends CrudRepository<CompleteBloodCountTest, Long> {

    @Query(value = "select max(b.lastModifiedDate) from CompleteBloodCountTest b")
    Timestamp getMaxLastModifiedDate();

    @Query(value = "select max(b.createdDate) from CompleteBloodCountTest b")
    Timestamp getMaxCreatedDate();
}

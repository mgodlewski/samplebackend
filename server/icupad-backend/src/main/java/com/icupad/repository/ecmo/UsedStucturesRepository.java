package com.icupad.repository.ecmo;

import com.icupad.entity.ecmo.EcmoConfiguration;
import com.icupad.entity.ecmo.UsedStructures;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Marcin on 04.03.2017.
 */
public interface UsedStucturesRepository extends CrudRepository<UsedStructures, Long> {

    List<UsedStructures> findByConfigurationIdAndDate(EcmoConfiguration configuration, Timestamp date);

    @Query(value = "select max(p.date) from UsedStructures p")
    Timestamp getMaxCreatedDate();
}

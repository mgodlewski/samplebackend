package com.icupad.repository.patient;

import com.icupad.entity.patient.Patient;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface PatientRepository extends CrudRepository<Patient, Long> {
    List<Patient> findAll();

    @Query(value = "select p from Patient p where p.createdDate > ?1 " +
            "or (p.lastModifiedDate is not null and p.lastModifiedDate > ?1)")
//    @Query(value = "select p from Patient p join Stay s " +
//            "on s.patient = p " +
//            "where (p.createdDate > ?1 and s.dischargeDate is null)" +
//            "or (p.lastModifiedDate is not null and p.lastModifiedDate > ?1 and s.dischargeDate is null)" +
//            "or (s.dischargeDate is not null and s.dischargeDate > ?1)")
    List<Patient> findAllAfter(Timestamp after);

    @Query(value = "select max(p.lastModifiedDate) from Patient p where p.lastModifiedByIcupad = ?1")
    Timestamp getMaxLastModifiedDateWithLastModifiedByIcupadEqual(boolean lastModifiedByIcupad);

    @Query(value = "select max(p.createdDate) from Patient p where p.lastModifiedByIcupad = ?1")
    Timestamp getMaxCreatedDateWithLastModifiedByIcupadEqual(boolean lastModifiedByIcupad);

    @Query(value = "select p from Stay s join s.patient p where p.id = s.patient  and p.createdDate > ?1 and s.active = true")
    List<Patient> findAllActiveAfter(Timestamp after);
}